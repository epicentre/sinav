<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $step = isset($this->step) ? $this->step : 1;
        $rules = array();
        switch($step){
            case 1:
                $rules = array_add($rules, 'name', 'required|max:255');
                if($this->time_manual == "0"){
                    $rules = array_add($rules, 'start_time', 'required|date_format:d.m.Y H:i|before:end_time');
                    $rules = array_add($rules,'end_time', 'required|date_format:d.m.Y H:i|after:start_time');
                }

                return $rules;
                break;
            case 2:
                $rules = array_add($rules, 'questions', 'required');
                return $rules;
                break;
            case 3:
                $rules = array_add($rules, 'theme', 'required');
                return $rules;
                break;
            default:
                abort(403);
                break;
        }

    }
}

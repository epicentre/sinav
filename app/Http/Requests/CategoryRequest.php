<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $page= $this->segment(3);
        $rules = [];
        switch($page){
            case "yeni":
                $rules = array_add($rules, 'name', 'required|unique:categories,name,NULL,id,user_id,'.$this->user()->id);
                break;
            case "duzenle":
                $rules = array_add($rules, 'name', 'required|unique:categories,name,'.$this->segment(4).',id,user_id,'.$this->user()->id);
                break;
        }
        return $rules;
    }
}

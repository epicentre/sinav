<?php

namespace App\Http\Controllers\Panel;

use Gate;
use Auth;

use Illuminate\Http\Request;
use App\Http\Requests\ExamRequest;
use App\Http\Controllers\Controller;
use App\Repositories\ExamRepository;
use App\Repositories\QuestionRepository;

class ExamController extends Controller
{

	protected $exam_repo;

    protected $question_repo;

    public function __construct(ExamRepository $exam_repo)
    {
    	$this->exam_repo = $exam_repo;
        $this->middleware('ajax', ['only' => ['destroy', 'kodGetir', 'sinavGirisKontrol', 'cevapla', 'sinavManuelBaslat', 'sinavManuelBitir', 'senkronVeriAl', 'senkronCevapla', 'senkronSinavBitir']]);
    }

    public function index(){
        $exams = $this->exam_repo->tumSinavlariGetir(Auth::user()->id);
    	return view('panel.exams.index', compact('exams'));
    }

    public function kodGetir(){

        $code = $this->exam_repo->createCode();

        return response()->json(['code' => $code]);
    }

    public function getStep1(){
        return view('panel.exams.step1');
    }

    public function getStep2(QuestionRepository $question_repo){
        if(session()->has('step1')){
            $this->question_repo = $question_repo;

            if(session()->has('step2.questions')){
                $firstGetQuestions = array_map('intval', explode(",", session('step2.questions')));
                $questions = $this->question_repo->panelSinavSoruGetir(Auth::user()->id, $firstGetQuestions);
            }
            else
                $questions = $this->question_repo->panelSinavSoruGetir(Auth::user()->id);

            return view('panel.exams.step2', compact('questions'));
        }
        else
            abort(403);
    }

    public function getStep3(){
        if(session()->has('step2'))
            return view('panel.exams.step3');
        else
            abort(403);
    }

    public function postCreate(ExamRequest $request, $step = 1){
    	switch($step){
    		case 1:
    			session()->put('step1', $request->all());

                return redirect('panel/sinavlar/yeni/2');
    		break;
    		case 2:
                session()->put('step2', $request->all());

    			//return redirect('panel/sinavlar/yeni/3');

                session()->put('step3', ["theme" => "default"]);
                $this->exam_repo->kaydet($request->user()->id);

                session()->forget('step1');
                session()->forget('step2');
                session()->forget('step3');

                alert()->success('Sınavınız başarıyla oluşturuldu')->persistent(false, true);

                return redirect('panel/sinavlar');
    		break;
            /*
            case 3:
                session()->put('step3', $request->all());
    			$this->exam_repo->kaydet($request->user()->id);

                session()->forget('step1');
                session()->forget('step2');
                session()->forget('step3');

                alert()->success('Sınavınız başarıyla oluşturuldu')->persistent(false, true);

                return redirect('panel/sinavlar');
    		break;
            */
            default:
                abort(403);
            break;
    	}
    }

    public function getEditStep1($exam_id){
        $exam = $this->exam_repo->iddenSinavGetir($exam_id);

        if (Gate::denies('update-delete-exam', $exam)) {
            abort(403);
        }

        if(session()->has('editStep1.exam_id') && session('editStep1.exam_id')<>$exam_id){
            session()->forget('editStep1');
            session()->forget('editStep2');
            session()->forget('editStep3');
        }

        return view('panel.exams.step1', compact("exam"));
    }

    public function getEditStep2(QuestionRepository $question_repo, $exam_id){
        $exam = $this->exam_repo->iddenSinavGetir($exam_id);

        if (Gate::denies('update-delete-exam', $exam)) {
            abort(403);
        }

        if(session()->has('editStep1')){

            $this->question_repo = $question_repo;

            $exam_questions = $exam->exam_questions()->orderBy('id', 'asc')->pluck('question_id')->implode(',');

            $firstGetQuestions = array_map('intval', explode(",", session()->has('editStep2.questions') ? session('editStep2.questions') : $exam_questions));

            $questions = $this->question_repo->panelSinavSoruGetir(Auth::user()->id, $firstGetQuestions);

            return view('panel.exams.step2', compact("exam", "questions", "exam_questions"));
        }
        else
            abort(403);
    }

    public function getEditStep3($exam_id){
        $exam = $this->exam_repo->iddenSinavGetir($exam_id);

        if (Gate::denies('update-delete-exam', $exam)) {
            abort(403);
        }

        if(session()->has('editStep2'))
            return view('panel.exams.step3', compact("exam"));
        else
            abort(403);
    }

    public function postEdit(ExamRequest $request, $exam_id, $step = 1){
        $exam = $this->exam_repo->iddenSinavGetir($exam_id);

        if (Gate::denies('update-delete-exam', $exam)) {
            abort(403);
        }
        switch($step){
            case 1:
                session()->put('editStep1', $request->all());

                session()->put('editStep1.exam_id', $exam_id);

                return redirect('panel/sinavlar/duzenle/'.$exam_id.'/2');
            break;
            case 2:
                session()->put('editStep2', $request->all());

                //return redirect('panel/sinavlar/duzenle/'.$exam_id.'/3');

                session()->put('editStep3', ["theme" => "default"]);

                $this->exam_repo->guncelle($exam_id);

                session()->forget('editStep1');
                session()->forget('editStep2');
                session()->forget('editStep3');

                alert()->success('Sınavınız başarıyla düzenlendi')->persistent(false, true);

                return redirect('panel/sinavlar');
            break;
            /*
            case 3:
                session()->put('editStep3', $request->all());

                $this->exam_repo->guncelle($exam_id);

                session()->forget('editStep1');
                session()->forget('editStep2');
                session()->forget('editStep3');

                alert()->success('Sınavınız başarıyla düzenlendi')->persistent(false, true);

                return redirect('panel/sinavlar');
            break;
            */
            default:
                abort(403);
            break;
        }
    }

    public function kodIleGir(){
        return view('front.kodilegir');
    }

    public function sinavGirisKontrol($code){
        $message = "";
        $exam = $this->exam_repo->koddanSinavGetir($code);
        if($exam){
            if($exam->time_manual == 1 && $exam->type == "senkron")
            {
                if(!is_null($exam->start_time) && is_null($exam->end_time))
                    $message = "senkrongiris";
                else
                    $message = "sinavyok";
            }
            else{
                if(session()->has('exam.exam_code'))
                    $message = "yarim";
                else{
                    $message = "ok";
                    if($this->exam_repo->sinavCozulmusmu($code))
                        $message = "cozulmus";
                    else
                        session()->put('exam.exam_code', $code);
                }
            }

        }
        else
            $message = "sinavyok";

        return response()->json(['message' => $message]);
    }

    public function exam(QuestionRepository $question_repo, $code){
        if(session()->has('exam.exam_code')){
            if(session('exam.exam_code')==$code){

                if(!session()->has('exam.exam_questions'))
                    $this->exam_repo->sinavGiris($code);

                $this->question_repo = $question_repo;
                $question = $this->question_repo->iddenSoruGetir(session('exam.exam_questions')[0]);
                $category_name = $question->category_id > 0 ? $this->question_repo->iddenSoruKategoriAdiGetir($question->category_id) : "";

                //Soru sessiona atılıyor.
                session()->put('exam.question_id', $question->id);
                session()->put('exam.question_time', $question->time);

                //Soru raporu kontrol ediliyor.
                if(!session()->has('exam.question_report_id')){
                    $this->question_repo->soruRaporGir($question->id);
                    session()->put('exam.question_elapsed_time', $question->time);
                }
                else{

                    $fark = $this->question_repo->saniyeFarkiGetir(session('exam.question_report_start_time'));

                    if(session('exam.question_time') - $fark > 0 && session('exam.question_time') - $fark < session('exam.question_time')){

                        session()->put('exam.question_elapsed_time', session('exam.question_time') - $fark);
                    }
                    else{
                        $message = $this->question_repo->cevapRaporGir();

                        if($message=="son")
                            return redirect('/');
                        else
                            return redirect('sinav/'.session('exam.exam_code'));
                    }
                    //
                }

                //Sınav esnasında sınav süresini kontrol ediyor. Eğer sınav süresi geçmişse sınavı bitiriyo.
                if($this->exam_repo->sinavSureKontrol()){
                    session()->forget('exam');
                    alert()->warning('Sınav süresi bittiği için sınavınız sonlandırıldı')->persistent(false, true);
                    return redirect('/');
                }

                return view('front.exam.asenkron', compact('question', 'category_name'));
            }
            else
                return redirect('/');
        }
        else
            return redirect('/');
    }

    public function cevapla(Request $request, QuestionRepository $question_repo){
        $this->question_repo = $question_repo;
        $questionId = $request->questionId;
        $cevapId = $request->cevapId;
        $answer_string = $request->answer_string;

        $fark = $this->question_repo->saniyeFarkiGetir(session('exam.question_report_start_time'));
        if(session('exam.question_id') == $questionId && session('exam.question_time') - $fark > 0 && session('exam.question_time') - $fark < session('exam.question_time'))
            $message = $this->question_repo->cevapRaporGir($cevapId, $answer_string);
        elseif($cevapId==0)
            $message = $this->question_repo->cevapRaporGir();

        return response()->json(['message' => $message]);
    }

    public function senkronSinav($code){
        $exam = $this->exam_repo->koddanSinavGetir($code);

        if($exam->time_manual==1 && $exam->type=="senkron" && !is_null($exam->start_time) && is_null($exam->end_time)){

            if($exam->user_id == auth()->user()->id){
                if($exam->socket_start == 1){
                    $exam = $this->exam_repo->senkronYenidenBaslat($exam);
                    return redirect('senkron/'.$exam->code);
                }
                else{
                    $exam->socket_start = 1;
                    $exam->save();
                }
            }

            if (auth()->user()->id === $exam->user_id)
                return view('front.exam.senkron_admin', compact('exam'));
            else
                return view('front.exam.senkron', compact('exam'));
        }
        else
            return redirect('/');

    }

    public function senkronVeriAl(Request $request, QuestionRepository $question_repo){
        $code = $request->code;
        $nicknames = $request->nicknames;

        $exam = $this->exam_repo->koddanSinavGetir($code);

        $sorular = "";
        if($exam->time_manual==1 && $exam->type=="senkron" && is_null($exam->end_time)){
            //Her kullanıcıya exam reports tablosunda kayıt açılıyor.
            foreach($nicknames as $key=>$value){
                $user_id = $value["userId"];
                $this->exam_repo->senkronSinavGiris($user_id, $exam->id);
            }

            $exam_questions = $exam->exam_questions;
            $this->question_repo = $question_repo;

            $questions = array();
            $category_names = array();
            foreach($exam_questions as $exam_question){
                $question = $this->question_repo->iddenSoruGetir($exam_question->question_id);
                $question->answers;
                array_push($category_names, ($question->category_id > 0 ? $this->question_repo->iddenSoruKategoriAdiGetir($question->category_id) : "bos21012013"));
                $question->toArray();
                //array_push($question, $answer);

                array_push($questions, $question);
            }

            $sorular = $questions;
        }
        else
            return response('Hata.', 401);

        return response()->json(['sorular'=>$sorular, 'category_names' => $category_names]);
    }

    public function senkronCevapla(Request $request, QuestionRepository $question_repo){
        $this->question_repo = $question_repo;
        $examId = $request->examId;
        $questionId = $request->questionId;
        $cevapId = $request->cevapId;
        $answer_string = $request->answer_string;
        $start_time = date('Y-m-d h:i:s', $request->start_time / 1000);


        $end_time = $this->question_repo->senkronCevapRaporGir($cevapId, $answer_string, $examId, $questionId, $start_time);

        return response()->json(['end_time', $end_time]);
    }

    public function senkronSinavBitir(Request $request){
        $exam_id = $request->examId;
        $this->exam_repo->senkronSinavBitir($exam_id);

        return response()->json();
    }

    public function sinavManuelBaslat($exam_id){
        $exam = $this->exam_repo->iddenSinavGetir($exam_id);

        if (Gate::denies('exam-start-end', $exam)) {
            abort(403);
        }

        $gidilcek_yer = "";
        if(is_null($exam->start_time) && is_null($exam->end_time)){
            $exam = $this->exam_repo->sinavManuelBaslat($exam);

            $gidilcek_yer = "self";
            if($exam->time_manual==1 && $exam->type=="senkron")
                $gidilcek_yer = "socket";
            else
                alert()->success('Sınav başlatıldı')->persistent(false, true);
        }

        return response()->json(['gidilcek_yer' => $gidilcek_yer]);
    }

    public function sinavManuelBitir(Request $request, $exam_id){
        $exam = $this->exam_repo->iddenSinavGetir($exam_id);

        if (Gate::denies('exam-start-end', $exam)) {
            abort(403);
        }

        if($exam->time_manual==0 || is_null($exam->end_time)){
            $this->exam_repo->sinavManuelBitir($exam);

            if(!isset($request->sayfa))
                alert()->success('Sınav bitirildi')->persistent(false, true);
        }

        return response()->json();
    }

    public function destroy($exam_id){
        $exam = $this->exam_repo->iddenSinavGetir($exam_id);

        if (Gate::denies('update-delete-exam', $exam)) {
            abort(403);
        }

        $this->exam_repo->sinavSil($exam);

        alert()->success('Sınav başarıyla silindi')->persistent(false, true);
    }

}

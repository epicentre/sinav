<?php

namespace App\Http\Controllers\Panel;

use Auth;
use Gate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\QuestionRepository;
use App\Http\Requests\QuestionRequest;

class QuestionController extends Controller
{
    protected $question_repo;

    public function __construct(QuestionRepository $repo)
	{
        $this->question_repo = $repo;
        $this->middleware('ajax', ['only' => ['destroy']]);
	}

    public function index(){

        $questions = $this->question_repo->tumSorulariGetir(Auth::user()->id);

    	return view('panel.questions.index', compact("questions"));
    }

    public function getCreate(){
        $categories = $this->question_repo->kategorileriGetir();

    	return view('panel.questions.create-edit', ['categories' => $categories]);
    }

    public function postCreate(QuestionRequest $request){
        $this->question_repo->kaydet($request->all(), $request->user()->id);

        alert()->success('Sorunuz başarıyla oluşturuldu')->persistent(false, true);

        return redirect('panel/sorular');
    }

    public function getEdit($question_id){
        $question = $this->question_repo->iddenSoruGetir($question_id);

        if (Gate::denies('update-delete-question', $question)) {
            abort(403);
        }

        $categories = $this->question_repo->kategorileriGetir();

        return view('panel.questions.create-edit', compact("question", "categories"));
    }

    public function postEdit(QuestionRequest $request, $question_id){
        $question = $this->question_repo->iddenSoruGetir($question_id);

        if (Gate::denies('update-delete-question', $question)) {
            abort(403);
        }

        $this->question_repo->guncelle($request->all(), $question_id);

        alert()->success('Soru başarıyla düzenlendi')->persistent(false, true);

        return redirect('panel/sorular');
    }

    public function destroy($question_id){
        $question = $this->question_repo->iddenSoruGetir($question_id);

        if (Gate::denies('update-delete-question', $question)) {
            abort(403);
        }

        $this->question_repo->soruSil($question);

        alert()->success('Soru başarıyla silindi')->persistent(false, true);
    }
}

<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use Validator;
use Gate;
use App\Repositories\CategoryRepository;
use App\Http\Requests\CategoryRequest;

class CategoryController extends Controller
{
    protected $category_repo;

    public function __construct(CategoryRepository $category_repo)
    {
        $this->category_repo = $category_repo;
        $this->middleware('ajax', ['only' => ['destroy']]);
    }

    public function index(){

        $categories = $this->category_repo->tumKategorileriGetir(auth()->user()->id);
    	return view('panel.categories.index', compact('categories'));
    }

    public function getCreate(){
    	return view('panel.categories.create-edit');
    }

    public function postCreate(CategoryRequest $request){

        $inputs = $request->all();

        $this->category_repo->kaydet($inputs, auth()->user()->id);

        alert()->success('Kategori başarıyla oluşturuldu')->persistent(false, true);

        return redirect('panel/kategoriler');

    }

    public function getEdit($category_id){

        $category = $this->category_repo->iddenKategoriGetir($category_id);

        if (Gate::denies('update-delete-category', $category)) {
            abort(403);
        }

        return view('panel.categories.create-edit', compact('category'));
    }

    public function postEdit(CategoryRequest $request, $category_id){

        $category = $this->category_repo->iddenKategoriGetir($category_id);

        if (Gate::denies('update-delete-category', $category)) {
            abort(403);
        }

        $inputs = $request->all();

        $this->category_repo->guncelle($inputs, $category_id);

        alert()->success('Kategori başarıyla düzenlendi')->persistent(false, true);

        return redirect('panel/kategoriler');
    }

    public function destroy($category_id)
    {
        $category = $this->category_repo->iddenKategoriGetir($category_id);

        if (Gate::denies('update-delete-category', $category)) {
            abort(403);
        }

        if($category->questions->count() > 0){
            alert()->info('Kategoriye ait '.$category->questions->count().' soru bulunduğundan pasife alındı')->persistent(false, true);
            $category->delete();
        }
        else{
            alert()->success('Kategori başarıyla silindi')->persistent(false, true);
            $category->forceDelete();
        }
    }
}

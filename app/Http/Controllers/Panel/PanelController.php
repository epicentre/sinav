<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use App\Repositories\UserRepository;
use App\Models\City, App\Models\County;

class PanelController extends Controller
{

	public function __construct()
	{
        $this->middleware('ajax')->only(['ilceGetir', 'profilResimYukle']);
	}

    public function index(){
    	//return view('panel.index');

    	return redirect('panel/sonuclarim');
    }

    public function profilim(){
        $user = auth()->user();
        $cities = City::all();
    	return view('panel.profilim', ['user' => $user, 'cities' => $cities]);
    }

    public function profilResimYukle(Request $request, UserRepository $user_repo){
        $validator = Validator::make($request->all(), [
            'picture' => 'required|image'
        ]);

        $hata = "";
        $mesaj = "";
        $resim = "";
        if ($validator->fails()) {
            $hata = "Bu alana resim dosyası yüklenmesi gerekir";
        } else {
            $user = auth()->user();
            $destination_path = 'uploads/user'.$user->id;
            $image_name = time().".".$request->file('picture')->getClientOriginalExtension();

            $request->file('picture')->move($destination_path, $image_name);
            $user = $user_repo->resimGuncelle($user, $destination_path."/".$image_name);
            $mesaj="Resim başarıyla yüklendi";
            $resim = asset($user->resim);
        }

        return response()->json(['mesaj' => $mesaj, 'hata' => $hata, 'resim' => $resim]);
    }

    public function profilBilgiGuncelle(Request $request, UserRepository $user_repo){
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $user = auth()->user();
        $data = $request->all();
        $user_repo->profilBilgiGuncelle($user, $data);

        alert()->success('Bilgileriniz kaydedildi')->persistent(false, true);

        return redirect('panel/profilim');

    }

    public function ilceGetir(Request $request){
        $city_id = $request->city_id;
        $data = "";
        if($city_id > 0){
            $data = County::where('city_id', $city_id)->get();
        }

        return response()->json(['data' => $data]);
    }

    public function profilSifreGuncelle(Request $request, UserRepository $user_repo){
        $validator = Validator::make($request->all(), [
            'password_old' => 'required',
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->with('sayfa', 'sifre')
                        ->withErrors($validator)
                        ->withInput();
        }

        $user = auth()->user();
        $data = $request->all();

        if($user_repo->profilSifreGuncelle($user, $data) == "Şifre yanlış"){
            alert()->warning('Şimdiki şifreniz yanlış')->persistent(false, true);
            return redirect()->back()
                        ->with('sayfa', 'sifre');
        }

        alert()->success('Şifreniz güncellendi')->persistent(false, true);

        return redirect('panel/profilim');
    }
}

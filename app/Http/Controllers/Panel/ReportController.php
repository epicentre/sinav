<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use Validator;
use Gate;
use App\Repositories\QuestionRepository;
use App\Repositories\ExamRepository;

class ReportController extends Controller
{
    protected $exam_repo;

    protected $question_repo;

    public function __construct(ExamRepository $exam_repo){
        $this->exam_repo = $exam_repo;
    }

    public function sinavSonuclarim(){

        $exam_reports = $this->exam_repo->sinavSonuclarimiGetir();

    	return view('panel.reports.sinav_sonuclarim', compact('exam_reports'));
    }

    public function sinavSonucumuGetir(QuestionRepository $question_repo, $exam_report_id){

        $exam_report = $this->exam_repo->sinavSonucumuGetir($exam_report_id);

        if (Gate::denies('select-exam_report', $exam_report)) {
            abort(403);
        }

        $siralama_verisi = $this->exam_repo->sinavSiralamaGetir($exam_report->exam_id);

        $this->question_repo = $question_repo;
        $user_question_reports = $this->question_repo->kullaniciSinavSoruRaporuGetir($exam_report->user_id, $exam_report->exam_id);

        return view('panel.reports.sinav_sonucum', compact('exam_report', 'user_question_reports', 'siralama_verisi'));
    }

    public function sinavRapor($exam_id){
        $exam = $this->exam_repo->iddenSinavGetir($exam_id);

        if (Gate::denies('exam-start-end', $exam)) {
            abort(403);
        }

        list($questions, $siralama_verisi, $cevap_veri, $grafik_veri) = $this->exam_repo->sinavRaporuGetir($exam);

        return view('panel.reports.sinav_rapor', compact('questions', 'siralama_verisi', 'cevap_veri', 'grafik_veri'));

    }
}

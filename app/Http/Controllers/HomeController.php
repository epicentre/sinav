<?php

namespace App\Http\Controllers;

use App\Repositories\ExamRepository;

class HomeController extends Controller
{
    protected $exam_repo;

    public function index(ExamRepository $exam_repo){
        $this->exam_repo = $exam_repo;

        $exams = $this->exam_repo->anasayfaSinavGetir();
        return view('front.index', compact('exams'));
    }
}

<?php namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ExamReport extends Model  {

	protected $table = 'exam_reports';

	public $timestamps = false;

	public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

	public function exam()
    {
        return $this->belongsTo('App\Models\Exam');
    }
}

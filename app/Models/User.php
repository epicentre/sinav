<?php 

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\User
 *
 * @property-read \App\Models\Role $role
 */
class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

    use Authenticatable, CanResetPassword, SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */

    protected $dates = ['deleted_at'];
    
    protected $table = 'users';

    protected $fillable = ['name', 'email', 'password','role_id', 'confirmed', 'confirmation_code'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * One to Many relation
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role() 
    {
        return $this->belongsTo('App\Models\Role');
    }

    /**
     * Check media all access
     *
     * @return bool
     */
    public function accessMediasAll()
    {
        return $this->role->slug === 'admin';
    }

    /**
     * Check media access one folder
     *
     * @return bool
     */
    public function accessMediasFolder()
    {
        return $this->role->slug === 'user';
    }

    public function questions(){
        return $this->hasMany('App\Models\Question');
    }

    public function exams(){
        return $this->hasMany('App\Models\Exam');
    }

    public function exam_reports(){
        return $this->hasMany('App\Models\ExamReport');
    }

    public function question_reports(){
        return $this->hasMany('App\Models\QuestionReport');
    }

}

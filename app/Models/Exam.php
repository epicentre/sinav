<?php namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Exam extends Model  {

	use SoftDeletes;

	protected $dates = ['deleted_at'];

	protected $table = 'exams';

	public function user(){
		return $this->belongsTo('App\Models\User');
	}

	public function exam_questions(){
		return $this->hasMany('App\Models\ExamQuestion');
	}

	public function exam_reports(){
		return $this->hasMany('App\Models\ExamReport');
	}

}

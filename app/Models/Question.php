<?php namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model  {

	use SoftDeletes;

	protected $dates = ['deleted_at'];

	protected $table = 'questions';

	public function answers(){
		return $this->hasMany('App\Models\Answer');
	}

	public function category(){
		return $this->hasOne('App\Models\Category', 'id', 'category_id');
	}

	public function question_reports(){
		return $this->hasMany('App\Models\QuestionReport');
	}

}

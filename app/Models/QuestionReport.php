<?php namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class QuestionReport extends Model  {

	protected $table = 'question_reports';

	public $timestamps = false;
}

<?php namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ExamQuestion extends Model  {

	protected $table = 'exam_questions';

	public function question(){
		return $this->belongsTo('App\Models\Question');
	}

}

<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('update-delete-category', function($user, $category){
            return $user->id === $category->user_id;
        });

        Gate::define('update-delete-question', function($user, $question){
            return $user->id === $question->user_id && $question->question_reports->count() < 1;
        });

        Gate::define('update-delete-exam', function($user, $exam){
            return $user->id === $exam->user_id && $exam->exam_reports->count() < 1;
        });

        Gate::define('select-exam_report', function($user, $exam_report){
            return $user->id === $exam_report->user_id;
        });

        Gate::define('exam-start-end', function($user, $exam){
            return $user->id === $exam->user_id;
        });
    }
}

<?php 

namespace App\Repositories;

use App\Models\User;
use Hash;

class UserRepository
{
	protected $user;

	public function __construct(User $user)
	{
		$this->user = $user;
	}

	public function confirm($confirmation_code)
	{
		$user = $this->user->where('confirmation_code', $confirmation_code)->firstOrFail();

		$user->confirmed = true;
		$user->confirmation_code = null;
		$user->save();
	}

	public function resimGuncelle($user, $resim){
		$user->resim = $resim;

		$user->save();

		return $user;
	}

	public function profilBilgiGuncelle($user, $data){
		$user->name = $data['name'];
		$user->city_id = $data['il'];
		$user->county_id = $data['ilce'];
		$user->cinsiyet = $data['cinsiyet'];
		$user->tel = $data['tel'];
		$user->adres = $data['adres'];

		$user->save();
	}

	public function profilSifreGuncelle($user, $data){
		$mesaj = "";
		if(Hash::check($data['password_old'], $user->password)){
			$user->password = bcrypt($data['password']);
			$user->save();
		}
		else{
			$mesaj = "Şifre yanlış";
		}

		return $mesaj;
		
	}

}

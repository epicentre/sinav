<?php

namespace App\Repositories;

use App\Models\Exam, App\Models\ExamReport, App\Models\ExamQuestion;
use App\Models\Question, App\Models\Answer, App\Models\QuestionReport;
use Carbon\Carbon;
use DB;

class ExamRepository{

	protected $exams;

	public function __construct(Exam $exams){
		$this->exams = $exams;
	}

	public function tumSinavlariGetir($user_id){
		$exams = $this->exams->where('user_id', $user_id)->get();

		return $exams;
	}

	public function createCode(){
		$don = true;
		while($don){
			$code = strtolower(str_random(7));
			if($this->exams->where('code', $code)->count() < 1)
				$don = false;
		}

		return $code;
	}

	public function iddenSinavGetir($exam_id){
		$exam = $this->exams->findOrFail($exam_id);

		return $exam;
	}

	public function koddanSinavGetir($code){
		$exam = $this->exams->where('code', $code)->firstOrFail();

		return $exam;
	}

	public function kaydet($user_id){
		$exam = $this->veritabaniKaydet(new $this->exams, $user_id);

		foreach(explode(",", session('step2.questions')) as $key=>$id){
			$exam_question = new ExamQuestion;
			$exam_question->exam_id = $exam->id;
			$exam_question->question_id = $id;

			$exam_question->save();
		}
	}

	public function guncelle($exam_id){
		$exam = $this->veritabaniKaydet($this->exams->findOrFail($exam_id));

		$ssnQuestions = explode(",", session('editStep2.questions'));
		foreach($ssnQuestions as $key=>$value){
			if(($key + 1) > $exam->exam_questions()->count())
				$exam_question = new ExamQuestion;
			else
				$exam_question = $exam->exam_questions[$key];

			$exam_question->exam_id = $exam->id;
			$exam_question->question_id = $value;

			$exam_question->save();

			//Eğer seçeneklerde veritabanında olupta kullanıcı panelden silmişse bu seçenekler veritabanından silinir.
			if(count($ssnQuestions) == ($key + 1) && $exam->exam_questions()->count() > count($ssnQuestions)){

				for($i=count($ssnQuestions) - 1; $i<$exam->exam_questions()->count(); $i++){
					$exam->exam_questions[$i]->delete();
				}
			}
		}
	}

	private function veritabaniKaydet($exam, $user_id = null){

		if(strpos(request()->url(), 'duzenle')!==false)
			$sessionName = "editStep1";
		else
			$sessionName = "step1";

		if($user_id)
			$exam->user_id = $user_id;
		$exam->name = session($sessionName.'.name');
		$exam->time_manual = session($sessionName.'.time_manual');
		$exam->start_time = session($sessionName.'.time_manual') == "0" ? Carbon::createFromFormat('d.m.Y H:i', session($sessionName.'.start_time')) : null;
		$exam->end_time = session($sessionName.'.time_manual') == "0" ? Carbon::createFromFormat('d.m.Y H:i', session($sessionName.'.end_time')) : null;
		$exam->question_shuffle = session()->has($sessionName.'.question_shuffle') ? 1 : 0;
		$exam->answer_shuffle = session()->has($sessionName.'.answer_shuffle') ? 1 : 0;
		$exam->code = session($sessionName.'.code');
		$exam->type = session($sessionName.'.type');
		$exam->public = session($sessionName.'.r1') == "public" ? 1 : 0;
		$exam->theme = session('step3.theme');

		$exam->save();

		return $exam;
	}

	public function anasayfaSinavGetir(){
		$exams = $this->exams
			->where([
				['public', 1],
				['time_manual', 0],
				['start_time', '<', Carbon::now()],
				['end_time', '>', Carbon::now()]
			])
			->orWhere(function($query){
				$query->where('public', 1)
					  ->where('time_manual', 1)
					  ->where('type', 'asenkron')
					  ->whereNull('end_time');
			})
			->get();

		return $exams;
	}

	public function sinavGiris($code){
		$exam = $this->exams->where('code', $code)->with('exam_questions')->first();

		$exam_report = new ExamReport;

		$exam_report->user_id = auth()->user()->id;
		$exam_report->exam_id = $exam->id;
		$exam_report->start_time = Carbon::now();

		$exam_report->save();

		session()->put('exam.exam_report_id', $exam_report->id);
		session()->put('exam.answer_shuffle', $exam->answer_shuffle);
		session()->put('exam.exam_name', $exam->name);
		session()->put('exam.exam_id', $exam->id);


		$exam_questions = $exam->question_shuffle == 1 ? $exam->exam_questions->shuffle() : $exam->exam_questions;

		foreach($exam_questions as $exam_question){
			session()->push('exam.exam_questions', $exam_question->question_id);
		}

		session()->put('exam.exam_questions_count', $exam_questions->count());

		//return $exam;
	}

	public function senkronSinavGiris($user_id, $exam_id){
		$exam_report = new ExamReport;

		$exam_report->user_id = $user_id;
		$exam_report->exam_id = $exam_id;
		$exam_report->start_time = Carbon::now();

		$exam_report->save();

	}

	public function senkronYenidenBaslat($exam){
		if($exam->time_manual==1 && $exam->type=="senkron" && !is_null($exam->start_time) && is_null($exam->end_time)){
			ExamReport::where('exam_id', $exam->id)->delete();
			QuestionReport::where('exam_id', $exam->id)->delete();
		}

		$exam->start_time = Carbon::now();
		$exam->code = strtolower(str_random(7));
		$exam->socket_start = 0;
		$exam->save();

		return $exam;
	}

	public function sinavCozulmusmu($code){
		$exam_id = $this->exams->where('code', $code)->value('id');

		$rapor_sayi = ExamReport::where([
				['user_id', auth()->user()->id],
				['exam_id', $exam_id],
			])->count();

		if($rapor_sayi > 0)
			return true;
		else
			return false;
	}

	public function sinavSonuclarimiGetir(){
		$exam_reports = ExamReport::where('user_id', auth()->user()->id)
			->orderBy('id', 'desc')
			->with('exam')
			->get();

		return $exam_reports;
	}

	public function sinavSonucumuGetir($exam_report_id){
		$exam_report = ExamReport::findOrFail($exam_report_id);

		return $exam_report;
	}

	public function sinavSiralamaGetir($exam_id){
		$exam_reports = ExamReport::where('exam_id', $exam_id)->whereNotNull('end_time')->get();

		$exam_questions = null;
		$veriler = array();
		foreach($exam_reports as $key=>$exam_report){

			if($key == 0)
				$exam_questions = ExamQuestion::where('exam_id', $exam_id)->get();

			$geciciVeri = array(
								'user_id'=> $exam_report->user->id,
								'name'	 =>	$exam_report->user->name,
								'dogru'  =>	0,
								'yanlis' => 0,
								'bos'	 => 0,
								'puan'	 => 0
								);

			foreach($exam_questions as $exam_question){
				$question_report = QuestionReport::where([
					['user_id', $exam_report->user->id],
					['exam_id', $exam_id],
					['question_id', $exam_question->question_id],
					])->first();

				if($question_report->answer_id == 0)
					$geciciVeri['bos']++;
				elseif(
					(!is_null($question_report->answer_string) && in_array($question_report->answer_string, explode(",", Answer::find($question_report->answer_id)->cevap)))
					||
					(is_null($question_report->answer_string) && Answer::find($question_report->answer_id)->dogru == 1)
					)
					$geciciVeri['dogru']++;
				else
					$geciciVeri['yanlis']++;

				$geciciVeri['puan'] = $geciciVeri['dogru']<>0 ? number_format(((100 / $exam_questions->count()) * $geciciVeri['dogru']), 2) : 0;

			}

			array_push($veriler, $geciciVeri);
		}

		$veriler = collect($veriler);

		$siralanmis = $veriler->sortByDesc('puan');

		$veriler = $siralanmis->values()->all();

		return $veriler;
	}

	public function sinavRaporuGetir($exam){
		$exam_reports = $exam->exam_reports;


		$exam_questions = $exam->exam_questions;
		$id = array();
		foreach($exam_questions as $exam_question)
			array_push($id, $exam_question->question_id);

		$stringId = implode(",", $id);
		$questions = Question::whereIn('id', $id)->orderByRaw(\DB::raw("FIELD(id, ".$stringId.")"))->get();

		$siralama_veri = array();
		$cevap_veri = array();
		$grafik_veri = array();
		foreach($exam_reports as $key=>$exam_report){

			if(!is_null($exam_report->start_time) && !is_null($exam_report->end_time)){
				$siralama_tmp = array(
					'user_id'=> $exam_report->user->id,
					'name'	 =>	$exam_report->user->name,
					'dogru'  =>	0,
					'yanlis' => 0,
					'bos'	 => 0,
					'puan'	 => 0
				);

				$question_reports = QuestionReport::where([
						['user_id', $exam_report->user->id],
						['exam_id', $exam->id]
						])->get();

				foreach($questions as $question){
					$question_report = $question_reports->where('question_id', $question->id)->first();

					if(!isset($grafik_veri[$question->id]))
						$grafik_veri[$question->id] = array(
							'dogru' 	=> 0,
							'yanlis'	=> 0,
							'bos' 		=> 0
						);

					foreach($question->answers as $answer){
						if($question->type == "coktan"){
							if(!isset($cevap_veri[$question->id][$answer->id]))
								$cevap_veri[$question->id][$answer->id] = 0;
						}
					}

					$cevap_key = ($question->type=="coktan" || is_null($question_report->answer_string)) ? $question_report->answer_id : $question_report->answer_string;
					if(isset($cevap_veri[$question->id][$cevap_key]))
						$cevap_veri[$question->id][$cevap_key]+=1;
					else
						$cevap_veri[$question->id][$cevap_key]=1;

					/* Sıralama verileri */
					if($question_report->answer_id == 0){
						$siralama_tmp['bos']++;
						$grafik_veri[$question->id]['bos']+=1;
					}
					elseif(
						(!is_null($question_report->answer_string) && in_array($question_report->answer_string, explode(",", Answer::find($question_report->answer_id)->cevap)))
						||
						(is_null($question_report->answer_string) && Answer::find($question_report->answer_id)->dogru == 1)
						){
							$siralama_tmp['dogru']++;
							$grafik_veri[$question->id]['dogru']+=1;
					}
					else{
						$siralama_tmp['yanlis']++;
						$grafik_veri[$question->id]['yanlis']+=1;
					}

					$siralama_tmp['puan'] = $siralama_tmp['dogru']<>0 ? number_format(((100 / $questions->count()) * $siralama_tmp['dogru']), 2) : 0;

				}

				array_push($siralama_veri, $siralama_tmp);

			}
		}
		/*
		$cevap_veri_siralanmis = array();
		foreach($cevap_veri as $key=>$veri){
			ksort($veri);
			$cevap_veri_siralanmis[$key] = $veri;
		}
		*/

		$siralama_veri = collect($siralama_veri);

		$siralanmis = $siralama_veri->sortByDesc('puan');

		$siralama_veri = $siralanmis->values()->all();

		return array($questions, $siralama_veri, $cevap_veri, $grafik_veri);
	}

	public function senkronSinavBitir($exam_id){
		$exam_report = ExamReport::where([
			['user_id', auth()->user()->id],
			['exam_id', $exam_id]
			])->first();

		$exam_report->end_time = Carbon::now();

		$exam_report->save();
	}

	public function sinavManuelBitir($exam){
		$exam->end_time = Carbon::now();

		$exam->save();
	}

	public function sinavManuelBaslat($exam){
		$exam->start_time = Carbon::now();
		$exam->end_time = null;

		$exam->save();

		return $exam;
	}

	public function sinavSureKontrol(){
		$exam_end_time = $this->exams->where('id', session('exam.exam_id'))->value('end_time');

		if(!is_null($exam_end_time) && ($exam_end_time < Carbon::now())){

			foreach(session('exam.exam_questions') as $key=>$exam_question){
				if($key==0){
					$question_report = QuestionReport::where([
						['user_id', auth()->user()->id],
						['exam_id', session('exam.exam_id')],
						['question_id', $exam_question]
					])->first();

					$question_report->end_time = Carbon::now();
				}
				else{
					$question_report = new QuestionReport;

					$question_report->user_id = auth()->user()->id;
					$question_report->exam_id = session('exam.exam_id');
					$question_report->question_id = $exam_question;
					$question_report->answer_id = 0;
					$question_report->answer_string = null;
					$question_report->start_time = Carbon::now();
					$question_report->end_time = Carbon::now();
				}

				$question_report->save();

			}

			$exam_report = ExamReport::find(session('exam.exam_report_id'));
			$exam_report->end_time = Carbon::now();
			$exam_report->save();

			return true;
		}

		return false;
	}

	public function sinavSil($exam){
		$exam->forceDelete();
	}


}

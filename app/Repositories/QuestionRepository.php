<?php

namespace App\Repositories;

use App\Models\Question, App\Models\ExamReport, App\Models\QuestionReport, App\Models\Category, App\Models\Answer;
use DB;
use Carbon\Carbon;

class QuestionRepository{

	protected $questions;

	public function __construct(Question $questions)
	{
		$this->questions = $questions;
	}

	public function tumSorulariGetir($user_id){
		$questions = $this->questions->where('user_id', $user_id)->get();

		return $questions;

	}

	public function panelSinavSoruGetir($user_id, $firstGetQuestions = null){
		if(is_null($firstGetQuestions))
			$questions = $this->questions->where('user_id', $user_id)->get();
		else{
			$stringFirstGetQuestions = implode(",", $firstGetQuestions);
			$questions = $this->questions
				->where('user_id', $user_id)
				->orderByRaw(\DB::raw("FIELD(id, ".$stringFirstGetQuestions." )"))
				->get();
			//->whereIn('id', $firstGetQuestions)
		}

		return $questions;
	}

	public function iddenSoruGetir($question_id){
		$question = $this->questions->findOrFail($question_id);

		return $question;
	}

	public function iddenSoruKategoriAdiGetir($category_id){
		$category = Category::withTrashed()->where('id' ,  $category_id)->first();

		return $category->name;
	}

	public function kategorileriGetir(){
		$categories = Category::where('user_id', auth()->user()->id)->get();

		return $categories;
	}

	public function kaydet($inputs, $user_id){
		$question = $this->veritabaniKaydet(new $this->questions, $inputs, $user_id);

		if($inputs["type"]=="coktan"){

			foreach($inputs["cevap"] as $key=>$value){
				$answer = new Answer;
				$answer->question_id = $question->id;

				$arancak = "cevap".($key + 1);
				$dogrumu = array_search($arancak, is_array(@$inputs["chkDogru"]) ? $inputs["chkDogru"] : array(""));

				$answer->dogru = $dogrumu === false ? 0 : 1;
				$answer->cevap = $value;

				$answer->save();
			}
		}
		else{
			$answer = new Answer;
			$answer->question_id = $question->id;
			$answer->dogru = 1;
			$answer->cevap = mb_strtolower($inputs["boslukdoldurma"]);

			$answer->save();

		}
	}

	public function guncelle($inputs, $question_id){
		$question = $this->veritabaniKaydet($this->questions->findOrFail($question_id), $inputs);

		if($inputs["type"]=="coktan"){
			foreach($inputs["cevap"] as $key=>$value){
				if(($key + 1) > $question->answers()->count())
					$answer = new Answer;
				else
					$answer = $question->answers[$key];
				$answer->question_id = $question->id;

				$arancak = "cevap".($key + 1);

				$dogrumu = array_search($arancak, is_array(@$inputs["chkDogru"]) ? $inputs["chkDogru"] : array(""));

				$answer->dogru = $dogrumu === false ? 0 : 1;
				$answer->cevap = $value;

				$answer->save();
				//Eğer seçeneklerde veritabanında olupta kullanıcı panelden silmişse bu seçenekler veritabanından silinir.
				if(count($inputs["cevap"]) == ($key + 1) && $question->answers()->count() > count($inputs["cevap"])){

					for($i=count($inputs["cevap"]) - 1; $i<$question->answers()->count(); $i++){
						$question->answers[$i]->delete();
					}
				}
			}
		}
		else{
			$answer = $question->answers()->first();
			$answer->question_id = $question->id;
			$answer->dogru = 1;
			$answer->cevap = mb_strtolower($inputs["boslukdoldurma"]);

			$answer->save();

			//Eğer seçeneklerde veritabanında olupta kullanıcı panelden silmişse bu seçenekler veritabanından silinir.
			if($question->answers()->count() > 1){

				for($i=1; $i<=$question->answers()->count(); $i++){
					$question->answers[$i]->delete();
				}
			}
		}
	}

	private function veritabaniKaydet($question, $inputs, $user_id = null){
		if($user_id)
			$question->user_id = $user_id;
		$question->category_id = $inputs['category'];
		$question->content = $inputs['content'];
		$question->description = $inputs['description'];
		$question->time = $inputs['time'];
		$question->type = $inputs['type'];

		$question->save();

		return $question;
	}

	public function soruSil($question){
		$question->forceDelete();
	}

	public function soruRaporGir($soruId){
		$question_report = new QuestionReport;

		$question_report->user_id = auth()->user()->id;
		$question_report->exam_id = session('exam.exam_id');
		$question_report->question_id = $soruId;
		$question_report->answer_id = 0;
		$question_report->start_time = Carbon::now();

		$question_report->save();

		session()->put('exam.question_report_id', $question_report->id);
		session()->put('exam.question_report_start_time', $question_report->start_time);
	}

	public function senkronCevapRaporGir($cevapId, $answer_string, $exam_id, $question_id, $start_time){

		$end_time = Carbon::now();

		$question_report = new QuestionReport;

		$question_report->user_id = auth()->user()->id;
		$question_report->exam_id = $exam_id;
		$question_report->question_id = $question_id;
		$question_report->answer_id = $cevapId;
		$question_report->answer_string = empty($answer_string) ? null : mb_strtolower($answer_string);
		$question_report->start_time = $start_time;
		$question_report->end_time = $end_time;

		$question_report->save();

		return $end_time;

	}

	public function cevapRaporGir($cevapId = 0, $answer_string = ""){
		$question_report = QuestionReport::find(session('exam.question_report_id'));

		$question_report->answer_id = $cevapId;
		$question_report->answer_string = empty($answer_string) ? null : mb_strtolower($answer_string);
		$question_report->end_time = Carbon::now();

		$question_report->save();

		$arr= session('exam.exam_questions');
		session()->put('exam.exam_questions', array_splice($arr, 1));

		$message = "";
		if(count(session('exam.exam_questions')) > 0){
			$istisnalar = array('exam_report_id', 'answer_shuffle', 'exam_name', 'exam_id', 'exam_code', 'exam_questions', 'exam_questions_count');
			foreach(session('exam') as $key=>$value){
				if(array_search($key, $istisnalar)===false)
					session()->forget('exam.'.$key);
			}
		}
		else
		{
			$this->sinavBitir();
			session()->forget('exam');
			alert()->success('Sınavınız bitti, sınav süresi bittiğinde sonuçlarınızı görebilirsiniz')->persistent(false, true);
			$message = "son";
		}

		return $message;
	}

	public function saniyeFarkiGetir($tarih){
		$dt1 = new Carbon($tarih);
        $dt2 = Carbon::now();

        return $dt2->diffInSeconds($dt1);
	}

	public function sinavBitir(){
		$exam_report = ExamReport::find(session('exam.exam_report_id'));

		$exam_report->end_time = Carbon::now();

		$exam_report->save();
	}

	public function kullaniciSinavSoruRaporuGetir($user_id, $exam_id){
		$user_question_reports = QuestionReport::where([
			['user_id', $user_id],
			['exam_id', $exam_id]
			])->get();

		return $user_question_reports;
	}
}

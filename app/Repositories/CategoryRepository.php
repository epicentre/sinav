<?php 

namespace App\Repositories;

use App\Models\Category;

class CategoryRepository{

	protected $categories;

	public function __construct(Category $categories){
		$this->categories = $categories;
	}

	public function tumKategorileriGetir($user_id){
		$categories = $this->categories->where('user_id', $user_id)->get();
		
		return $categories;
	}

	public function iddenKategoriGetir($category_id){
		$category = $this->categories->findOrFail($category_id);

		return $category;
	}

	public function kaydet($inputs, $user_id){
		$this->veritabaniKaydet(new $this->categories, $inputs, $user_id);
	}

	public function guncelle($inputs, $category_id){
		$this->veritabaniKaydet($this->categories->findOrFail($category_id), $inputs);
	}

	private function veritabaniKaydet($category, $inputs, $user_id = null){
		if($user_id)
			$category->user_id = $user_id;
		$category->name = $inputs['name'];
		$category->description = $inputs['description'];

		$category->save();
	}

	public function kategorininSorusuVarmi($category){
		return $category->questions()->count();
	}
}
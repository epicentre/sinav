<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>{{ $title }}</h2>

		<div>
			{{ $intro }}<br>
			Üyeliğinizi doğrulamak için: <a href="{{ url('confirm', $confirmation_code) }}" target="_blank" title="Üyelik Doğrulama">burayı tıklayın</a>
		</div>
	</body>
</html>

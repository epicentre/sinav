<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Sınav Merkezi</title>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Animate Css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css">
    <!-- Superslide css file-->
    <link rel="stylesheet" href="{{ asset('assets/front/css/superslides.css') }}">
    <!-- Slick slider css file -->
    <link href="{{ asset('assets/front/css/slick.css') }}" rel="stylesheet">
    <!-- Default Theme css file -->
    <link href="{{ asset('assets/front/css/default-theme.css') }}" rel="stylesheet">

    <!-- Main structure css file -->
    <link href="{{ asset('assets/front/css/front_style.css') }}" rel="stylesheet">

    <!-- Google fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Varela' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>

    <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>

    <header id="header">
        <div class="menu_area">
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <!-- LOGO -->

                        <!-- TEXT BASED LOGO -->
                        <a class="navbar-brand" href="#">Sınav <span>Merkezi</span></a>

                        <!-- IMG BASED LOGO  -->
                        <!--  <a class="navbar-brand" href="#"><img src="img/logo.png" alt="logo"></a> -->
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul id="top-menu" class="nav navbar-nav navbar-right main_nav">
                            <li class="active"><a href="#">Anasayfa</a></li>
                            <li><a href="#sinavlar">Sınavlar</a></li>
                            <li><a href="#ozellikler">Özellikler</a></li>
                            <li><a href="#kullanim">Kullanım</a></li>
                            <li><a href="{{ url('kodilegir') }}" id="except">Kod ile Sınava Gir</a></li>
                            @if (Auth::guest())
                                <li><a href="{{ url('/login') }}" id="except">Giriş</a></li>
                                <li><a href="{{ url('/register') }}" id="except">Kayıt Ol</a></li>
                            @else
                                <li><a href="{{ url('panel') }}" id="except">Panelim</a></li>
                                <li class="dropdown">
                                    <a href="#" id="except" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{ url('/logout') }}" id="except"><i class="fa fa-btn fa-sign-out"></i>Çıkış</a></li>
                                    </ul>
                                </li>
                            @endif

                        </ul>
                    </div><!--/.nav-collapse -->
                </div>
            </nav>
        </div>

        <div class="slider_area">
            <div id="slides">
                <ul class="slides-container">
                    <li>
                        <div class="slider_overlay"></div>
                        <img src="{{ asset('assets/front/img/slide1.jpg') }}" alt="img">
                        <!--
                        <div class="slider_caption">
                            <h2>Hi,We are Ultramodern Single pro</h2>
                            <p>we are a group of experienced designers and developers</p>
                            <a href="#" class="slider_btn">Who We are</a>
                        </div>
                        -->
                    </li>

                    <li>
                        <div class="slider_overlay">
                        <img src="{{ asset('assets/front/img/slide2.jpg') }}" alt="img">
                        <!--
                        <div class="slider_caption">
                            <h2>Hi,We are Ultramodern Single pro</h2>
                            <p>we are a group of experienced designers and developers</p>
                            <a href="#" class="slider_btn">Who We are</a>
                        </div>
                        -->
                    </li>
                </ul>

                <nav class="slides-navigation">
                <!-- PREV IN THE SLIDE -->
                    <a class="prev" href="/item1">
                        <span class="icon-wrap"></span>
                        <h3><strong>Önceki</strong></h3>
                    </a>
                    <a class="next" href="/item3">
                        <span class="icon-wrap"></span>
                        <h3><strong>Sonraki</strong></h3>
                    </a>
                </nav>
            </div>
        </div>
    </header>

    <section id="sinavlar">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="heading">
                        <h2 class="wow fadeInLeftBig">Sınavlar</h2>
                    </div>
                    <div class="team_area">
                        <?php
                            setlocale(LC_TIME, 'tr_TR@TL', 'tr_TR', 'tr', 'Turkish');
                        ?>
                        <div class="team_slider">
                            @foreach($exams as $exam)
                            <div class="col-lg-4 col-sm-6">
                                <div class="single_team wow fadeInUp">
                                    <h3>
                                        {{ $exam->name }}
                                        <span>{{ $exam->user->name }}</span>
                                    </h5>
                                    <ul>
                                        <li>
                                            <strong>Başlangıç Tarihi:</strong>
                                            {{ \Carbon\Carbon::parse($exam->start_time)->formatLocalized('%d %B %Y %H:%M') }}
                                        </li>
                                        <li>
                                            <strong>Tahmini Bitiş tarihi:</strong>
                                            {{ is_null($exam->end_time) ? 'Belli Değil' : \Carbon\Carbon::parse($exam->end_time)->formatLocalized('%d %B %Y %H:%M') }}
                                        </li>
                                        <li>
                                            <strong>Sınav Kodu:</strong>
                                            <span class="label label-success" style="color:#FFF; font-size: 120%;">{{ $exam->code }}</span>
                                        </li>
                                        <li>
                                            <strong>Soru Sayısı:</strong>
                                            {{ $exam->exam_questions()->count() }}
                                        </li>
                                        <li>
                                            <strong>Çözen Kişi Sayısı:</strong>
                                            {{ $exam-> exam_reports()->whereNotNull('end_time')->count() }}
                                        </li>
                                        <li style="text-align:center;">
                                            <a href="{{ url('sinav/'.$exam->code) }}" id="sinavBaslat" data-adi="{{ $exam->name }}" data-kod="{{ $exam->code }}" class="btn btn-lg btn-warning"><span class="glyphicon glyphicon-hourglass" style="color:#FFF;"></span>Sınavı Başlat</a>
                                            <div class="clearfix"></div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="ozellikler">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="heading">
                        <h2 class="wow fadeInLeftBig">Özellikler</h2>
                        <p>Sınav Merkezi projemiz son teknolojiler kullanılarak hazırlanmıştır. </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="service_area">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="single_service wow fadeInLeft">
                                    <div class="service_iconarea">
                                        <span class="fa fa-lightbulb-o service_icon"></span>
                                    </div>
                                    <h3 class="service_title">Yenilikçi Teknoloji</h3>
                                    <p>Sınav Merkezi programlaması son teknolojilerle yazılmıştır. HTML5, CSS3, Laravel, Jquery, NodeJs + Socket.IO gibi güçlü teknolojiler kullanılmıştır.</p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="single_service wow fadeInRight">
                                    <div class="service_iconarea">
                                        <span class="fa fa-tablet service_icon"></span>
                                    </div>
                                    <h3 class="service_title">Modern Web</h3>
                                    <p>Sınav Merkezi %99 responsive olarak tasarlanmıştır. Yani tablet, telefon, bilgisayarla sisteme girdiğinizde hepsi için ayrı ve düzenli görüntü göreceksiniz</p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="single_service wow fadeInRight">
                                    <div class="service_iconarea">
                                        <span class="fa fa-refresh service_icon"></span>
                                    </div>
                                    <h3 class="service_title">Eş Zamanlı Sınav</h3>
                                    <p>Sistemde normal sınava ek olarak eş zamanlı sınav(senkron) seçeneği bulunmaktadır. Bu sınav türüyle sınav yapmak istediğiniz kişilere canlı olarak sınavını uygulayabilirsiniz</p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="single_service wow fadeInLeft">
                                    <div class="service_iconarea">
                                        <span class="fa fa-bar-chart service_icon"></span>
                                    </div>
                                    <h3 class="service_title">Raporlandırma</h3>
                                    <p>Sınav sonrasında sonuçlarınızı görebileceğiniz detaylı grafiklendirmelerin olduğu raporlandırma sistemiyle nerede eksik, yanlış olduğunu çok iyi anlayacaksınız</p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="single_service wow fadeInRight">
                                    <div class="service_iconarea">
                                        <span class="fa fa-users service_icon"></span>
                                    </div>
                                    <h3 class="service_title">Sınırsız Yetki</h3>
                                    <p>Sistemimize kayıtlı olan herkes aynı yetkiye sahiptir. Dilersen sınav olabilir veya kendin sınav oluşturabilirsin.</p>
                                    </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="single_service wow fadeInLeft">
                                    <div class="service_iconarea">
                                        <span class="fa fa-lock service_icon"></span>
                                    </div>
                                    <h3 class="service_title">Güvenli</h3>
                                    <p>Sistemdeki veriler güvenlik için çeşitli optimizasyonların yapıldığı sunucumuzda saklanmaktadır. Ayrıca sınav anında ve diğer sayfalarda sizin güvenliğiniz için çeşitli ayarlamalar yapılmıştır</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="kullanim">

        <section class="adim-siyah">
            <div class="container wow bounceInRight">
                <div class="row">
                    <div class="col-md-4 step-img">
                        <span class="fa fa-user-plus"></span>
                    </div>
                    <div class="col-md-8 step-desc">
                        <div class="col-md-2 text-center">
                            <div class="step-no">
                                <span class="no-inner">1</span>
                            </div>
                        </div>

                        <div class="col-md-10 step-details">
                            <h3 class="step-title">Üye Ol - Giriş Yap</h3>
                            <p class="step-description">Sistemi kullanmak için öncelikle üye olmalısın. Üyelik ekranında gerekli bilgileri doldurduktan sonra giriş yapıp sistemi kullanmaya başlayabilirsin. Sisteme giriş yaptıktan sonra <a href="{{ url('panel') }}" title="">Panelim</a> sayfasına girip Soru, Kategori, Sınav oluşturabilirsin. Sınav oluşturmak istemiyorsan Adım 5'ten devam edebilirsin.</p>
                        </div><!-- End step-details -->
                    </div>
                </div>
            </div>
        </section>

        <section class="adim-normal">
            <div class="container wow bounceInRight">
                <div class="row">
                    <div class="col-md-4 step-img">
                        <span class="fa fa-list" style="color: #7F8C8D;"></span>
                    </div>
                    <div class="col-md-8 step-desc">
                        <div class="col-md-2 text-center">
                            <div class="step-no">
                                <span class="no-inner">2</span>
                            </div>
                        </div>

                        <div class="col-md-10 step-details">
                            <h3 class="step-title">Kategori Oluştur</h3>
                            <p class="step-description">Kategori İşlemleri menüsünden oluşturacağınız sorular için kategoriler oluşturup sorularınızı kategorilendirebilirsiniz.</p>
                        </div><!-- End step-details -->
                    </div>
                </div>
            </div>
        </section>

        <section class="adim-siyah">
            <div class="container wow bounceInLeft">
                <div class="row">
                    <div class="col-md-8 step-desc">
                        <div class="col-md-2 text-center">
                            <div class="step-no">
                                <span class="no-inner">3</span>
                            </div>
                        </div>

                        <div class="col-md-10 step-details">
                            <h3 class="step-title">Soru Oluştur</h3>
                            <p class="step-description">Soru İşlemleri menüsünde Soru Bankam ve Soru Ekle alt menüsü bulunmaktadır.</p>
                            <p class="step-description">Sorularım sayfasında oluşturmuş olduğunuz soruları görüp üzerinde değişiklikler yapabilirsiniz.</p>
                            <p class="step-description">
                                Soru Ekleme sayfasında yeni soru oluşturabilirsiniz.
                            </p>
                            <h3 class="step-title">Özellikler</h3>
                            <ul>
                                <li><b>Soru İçeriği :</b> Soru eklerken soru içeriğini editör aracılığıyla düzenleyip istediğin şekilde yapabilirsin. Soru içeriğinde resim, tablo gibi bileşenler kullanabilirsin</li>
                                <li><b>Soru Açıklaması :</b> Soru Açıklama bölümünde sınav esnasında soru gösterildiğinde kullanıcılara ipucu olarak buraya girdiğiniz açıklama gösterilecektir</li>
                                <li><b>Soru Süresi</b></li>
                                <li><b>Soru Kategorisi</b></li>
                                <li><b>Soru Tipi : </b> Soru tipi olarak Çoktan Seçmeli ve Boşluk Doldurma olarak iki tür vardır. Çoktan Seçmeli türüyle sorunuza istediğiniz kadar seçenek ekleyip bir veya birden fazla seçeneği doğru olarak belirtebilirsiniz. Boşluk Doldurma tipini ise açık uçlu sorularınızda kullanabilirsiniz. Birden fazla cevap belirleyip kullanıcının bu cevaplardan birisini girmesini bekleyin</li>
                            </ul>
                        </div> <!-- End step-details -->
                    </div>
                    <div class="col-md-4">
                        <img src="{{ asset('assets/front/img/soru.jpg') }}" class="img-responsive" alt="">
                    </div>
                </div>
            </div>
        </section>

        <section class="adim-normal">
            <div class="container wow bounceInLeft">
                <div class="row">
                    <div class="col-md-8 step-desc">
                        <div class="col-md-2 text-center">
                            <div class="step-no">
                                <span class="no-inner">4</span>
                            </div>
                        </div>

                        <div class="col-md-10 step-details">
                            <h3 class="step-title">Sınav Oluştur</h3>
                            <p class="step-description">Sınav İşlemleri menüsünde Sınavlarım ve Sınav Ekle alt menüsü bulunmaktadır.</p>
                            <p class="step-description">Sınavlarım sayfasında oluşturmuş olduğunuz sınavları görüp üzerinde işlemler yapabilirsiniz. Örneğin; bir sınavı bitirebilir veya başlatabilirsiniz. Ayrıca biten sınavlar için Sınav Raporunu bu sayfadan görebilirsiniz.</p>
                            <p class="step-description">
                                Sınav Ekle sayfasında yeni sınavlar oluşturabilirsin. Sınav oluşturma işlemi 2 adımda gerçekleşmektedir. İlk adımda sınavla ilgili temel bilgiler girilir, ikinci adımda ise sınavda olmasını istediğiniz sorular seçilir.
                            </p>
                            <h3 class="step-title">Özellikler</h3>
                            <ul>
                                <li><b>Tarih :</b> Tarih bölümünde Belirttiğim Aralık seçilirse hemen altında bulunan başlangıç ve bitiş tarihi olacak şekilde sınav olacaktır. Tarih türü Manuel seçilirse alt menü açılıp Sınav Türü açılacaktır. Burda sınav türü olarak eş zamanlı sınav veya eş zamansız seçilip sonra dilediğiniz zaman Sınavlarım sayfasında bu sınavı başlatabilirsiniz.</li>
                                <li><b>Sınav sorularının kullanıcıya karışık sırayla gelmesi</b></li>
                                <li><b>Soru cevaplarının kullanıcıya karışık sırayla gelmesi</b></li>
                                <li><b>Giriş İzni : </b> Dilerseniz sınavı herkese açık yapıp sisteme üye olan herkesin bu sınava girmesini sağlayabilirsiniz. Veya kodla giriş seçilip sistemin bu sınav için ürettiği kodu sınava girmesini istediğiniz kişilerle paylaşıp belirli kişilere sınavı uygulayabilirsin.</li>
                            </ul>
                        </div> <!-- End step-details -->
                    </div>
                    <div class="col-md-4">
                        <img src="{{ asset('assets/front/img/sinav.jpg') }}" class="img-responsive" alt="">
                    </div>
                </div>
            </div>
        </section>

        <section class="adim-siyah">
            <div class="container wow bounceInRight">
                <div class="row">
                    <div class="col-md-4 step-img">
                        <img src="{{ asset('assets/front/img/giris.jpg') }}" class="img-responsive" alt="">
                    </div>
                    <div class="col-md-8 step-desc">
                        <div class="col-md-2 text-center">
                            <div class="step-no">
                                <span class="no-inner">5</span>
                            </div>
                        </div>

                        <div class="col-md-10 step-details">
                            <h3 class="step-title">Sınava Gir</h3>
                            <p class="step-description">Dilersen anasayfada sınavlar bölümünde gözüken sınavlara girebilir veya menüde bulunan kod ile giriş sayfasına girip edindiğin kodla sınava girebilirsin</p>
                        </div><!-- End step-details -->
                    </div>
                </div>
            </div>
        </section>

        <section class="adim-normal">
            <div class="container wow bounceInLeft">
                <div class="row">
                    <div class="col-md-8 step-desc">
                        <div class="col-md-2 text-center">
                            <div class="step-no">
                                <span class="no-inner">6</span>
                            </div>
                        </div>

                        <div class="col-md-10 step-details">
                            <h3 class="step-title">Sınav Sonucuna Bak</h3>
                            <p class="step-description">Panelim sayfasına girip Sınav Sonuçlarım sayfasından girmiş olduğunuz sınavların sonucunu görebilirsin. Sınav sonuçların 100'lük sisteme göre hesaplanacaktır.</p>
                        </div> <!-- End step-details -->
                    </div>
                    <div class="col-md-4">
                        <img src="{{ asset('assets/front/img/rapor.jpg') }}" class="img-responsive" alt="">
                    </div>
                </div>
            </div>
        </section>

    </section>

    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="footer_left">
                        <p>Copyright &copy; 2016 <a href="http://www.arfdmr.me">Arif DEMİR</a>. Tüm Hakları Saklıdır</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="footer_right">
                        <ul class="social_nav">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <!-- For smooth animatin  -->
    <script src="{{ asset('assets/front/js/wow.min.js') }}"></script>
    <!-- superslides slider -->
    <script src="{{ asset('assets/front/js/jquery.superslides.min.js') }}" type="text/javascript"></script>
    <!-- slick slider -->
    <script src="{{ asset('assets/front/js/slick.min.js') }}"></script>
    <!-- Custom js-->
    <script src="{{ asset('assets/front/js/custom.js') }}"></script>
    <!-- Sweet Alert -->
    @include('sweetalert::alert')

    <script src="{{ asset('assets/js/jquery.progressTimer.js') }}"></script>
    <script>
        $(function(){

          $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });

        $(document).on('click', '#sinavBaslat', function(e){
          var th = $(this);
          e.preventDefault();
          Swal.fire({
            title: $(th).data('adi') + ' adlı sınava girmek istediğine emin misin?',
            type: "info",
            showCancelButton: true,
            cancelButtonText: 'İptal',
            confirmButtonText: "Evet, eminim",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
          },
          function(){
            $.ajax({
                url: "{{ url('sinavgiriskontrol') }}" + '/' + $(th).data('kod'),
                type: 'POST',
                dataType : 'json'
              })
              .done(function(data) {
                if(data.message=="ok"){
                  window.location.href = $(th).attr('href');
                }
                else if(data.message == "cozulmus")
                  swal.fire('Uyarı', 'Bu sınavı çözmüşsün', 'info');
                else if(data.message=="yarim")
                  swal.fire({
                    title: "Uyarı",
                    text: "Yarıda kalan sınavınız bulunmaktadır, eğer diğer sekme veya pencerede sınav açık değilse devam et'e tıklayıp sınava kaldığın yerden devam edebilirsin",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Devam Et",
                    cancelButtonText: "İptal",
                    closeOnConfirm: false },
                    function(){
                      window.location.href = "{{ url('sinav/'.session('exam.exam_code')) }}"
                  });
              })
              .fail(function(jqXHR, textStatus) {
                if(jqXHR.status == 401)
                  Swal.fire('Hata', 'Sınava girmek için giriş yapmalısın', 'error');
                else
                  Swal.fire('Hata', 'Beklenmedik bir hata oluştu!', 'error');
              });
          });
        });

      });
    </script>
</body>
</html>

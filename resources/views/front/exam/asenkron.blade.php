@extends('front.layouts.minimal')

@section('title', session('exam.exam_name'))

@section('content')

@include('front.exam.question')
@endsection

@section('scripts')
<script src="{{ asset('assets/js/jquery.progressTimer.js') }}"></script>
<script>
    $(function(){

        $('body').css("background" , "#D3D3D3");

        //$('.container-fluid').css("margin-top" , function(){ return ($(window).outerHeight() - $(this).outerHeight()) / 2 } );
        
        var sayac = $("#progressTimer").progressTimer({
            timeLimit: {{ session('exam.question_elapsed_time') }},
            warningThreshold: {{ round(session('exam.question_elapsed_time') / 4) }},
            baseStyle: 'progress-bar-info',
            warningStyle: 'progress-bar-danger',
            onFinish: function() {
                cevapla(0, "", $('#questionId').val());
            }
        });

        $(document).on('click', '#cevap', function(e){
            e.preventDefault();
            sayac.stop();
            if($('#questionType').val()=="coktan")
                cevapla($(this).data('id'), "", $('#questionId').val());
            else{
                if($.trim($('#cevapText').val())=="")
                    alert('Cevap girmelisin');
                else{
                    cevapla($(this).data('id'), $('#cevapText').val(), $('#questionId').val());
                    $(document).off("click", "#cevap");
                    }
                }
            
          });

        function cevapla(answer_id, answer_string, question_id){
            $.ajax({
                url: "{{ url('cevapla') }}",
                type: 'POST',
                data:{"cevapId" : answer_id, "questionId" : question_id, "answer_string" : answer_string },
                dataType : 'json'
            })
            .done(function(data) {
                if(data.message=="son")
                    window.location.href = "{{ url('/') }}";
                else
                    window.location.reload();
            })
            .fail(function(jqXHR, textStatus) {
                alert('Beklenmedik bir hata oluştu');
            });
        }

        String.prototype.turkishLowerCase = function () {
            return this.replace(/Ğ/g, 'ğ')
                .replace(/Ü/g, 'ü')
                .replace(/Ş/g, 'ş')
                .replace(/I/g, 'ı')
                .replace(/İ/g, 'i')
                .replace(/Ö/g, 'ö')
                .replace(/Ç/g, 'ç')
                .toLowerCase();
        };//String.turkishLowerCase

        $(document).on('keyup', '#cevapText', function (e) {
            if (e.which >= 97 && e.which <= 122) {
              var newKey = e.which - 32;
              // I have tried setting those
              e.keyCode = newKey;
              e.charCode = newKey;
            }

            $(this).val(($(this).val()).turkishLowerCase());
        });

    });
</script>
@endsection
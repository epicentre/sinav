@extends('front.layouts.minimal')

@section('title', $exam->name)

@section('content')
<div id="loader-wrapper">
    <div id="loader">
    </div>
    <div id="content">
      <h2>Sınavın başlamasını bekle...</h2>
    </div>
</div>

<div id="soruAlani" style="display:none; ">
    @include('front.exam.question')
</div>

<div id="raporAlani" style="display:none;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <canvas id="myChart" width="400" height="400"></canvas>
            </div>
            <div class="col-md-12">
                <div id="secenekRapor">

                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
    <script src="{{ asset('assets/js/jquery.progressTimer.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>

    <script>
        $(function() {

            $('#loader-wrapper').show();
            $('#soruAlani').hide();
            $('#raporAlani').hide();

            $('body').css("background" , "#50514F");

            var socket_client_url = "{{ config('app.socket_client_url') }}";
            var socket = io(socket_client_url);

            socket.on('connect', function(data){
            	socket.emit('baglan', {
            		room : '{{ $exam->code }}',
            		user : { id : '{{ auth()->user()->id }}', name : '{{ auth()->user()->name }}', email : '{{ auth()->user()->email }}' }
            	});
            });

            var sayac;
            var start_time;
            var type;
            socket.on('soruGetir', function(data){

                start_time = data.start_time;
                type = data.soru.type;

                //Kaçıncı soruda olduğunu belirten progress
                $('#soruBar .progress-bar').css("width", Math.round((100 / data.soru_sayisi) * data.kacinci_soru) + "%");
                $('#soruBar .progress-bar span').html("Soru" + data.kacinci_soru + ' / ' + data.soru_sayisi);

                //input hiddenlar
                $('#questionId').val(data.soru.id);
                $('#questionType').val(data.soru.type);
                //Kalan süre progress barı.
                sayac = $("#progressTimer").progressTimer({
                    timeLimit: data.soru.time,
                    warningThreshold: Math.round(data.soru.time / 4),
                    baseStyle: 'progress-bar-info',
                    warningStyle: 'progress-bar-danger',
                    onFinish: function() {
                        cevapla(0, "", $('#questionId').val());
                    }
                });
                //Soru İçeriği
                $('#icerikDiv').html(data.soru.content);
                if(data.soru.description != ""){
                    $('#icerikDiv').prepend('<div class="ipucu"><button type="button" class="buton" data-toggle="modal" data-target="#ipucuModal">&#63;</button></div>');
                    $('#ipucuModal .modal-body').html(data.soru.description);
                }
                if(data.kategori != "bos21012013")
                    $('#icerikDiv').prepend('<div class="kategori">' + data.kategori + '</div>');

                //Seçenekler
                var secenek_html = "";
                if(data.soru.type == "coktan"){
                    for (var prop in data.soru.answers) {

                      secenek_html+="<div><button type='button' id='cevap' class='buton' data-id='"+ data.soru.answers[prop].id +"'>" + data.soru.answers[prop].cevap + "</button></div>";
                    }
                }
                else{
                    secenek_html+= "<div class='soru-bosluk'><input type='text' id='cevapText' placeholder='cevabınızı yazın' value=''><button type='button' id='cevap' class='buton' data-id='" + data.soru.answers[0].id + "'>Cevapla</button><div class='clearfix'></div></div>";
                }
                $('#secenekDiv').html(secenek_html);

                $('#loader-wrapper').hide();
                $('#raporAlani').hide();
                $('#soruAlani').show();

            });

            $(document).on('click', '#cevap', function(event){
                $(this).attr("disabled", "disabled");

                $('#loader-wrapper').show();
                $('#raporAlani').hide();
                $('#soruAlani').hide();
                $('#loader-wrapper #content h2').html("Soru süresinin bitmesini bekle.");

                sayac.stop();

                if($('#cevapText').length < 1){
                    cevapla($(this).data('id'), "", $('#questionId').val());
                }
                else{
                    if($.trim($('#cevapText').val())==""){
                        alert('Cevap girmelisin');
                    }
                    else{
                        cevapla($(this).data('id'), $('#cevapText').val(), $('#questionId').val());
                    }
                }
            });

            String.prototype.turkishLowerCase = function () {
                return this.replace(/Ğ/g, 'ğ')
                    .replace(/Ü/g, 'ü')
                    .replace(/Ş/g, 'ş')
                    .replace(/I/g, 'ı')
                    .replace(/İ/g, 'i')
                    .replace(/Ö/g, 'ö')
                    .replace(/Ç/g, 'ç')
                    .toLowerCase();
            };//String.turkishLowerCase

            $(document).on('keyup', '#cevapText', function (e) {
                if (e.which >= 97 && e.which <= 122) {
                  var newKey = e.which - 32;
                  // I have tried setting those
                  e.keyCode = newKey;
                  e.charCode = newKey;
                }

                $(this).val(($(this).val()).turkishLowerCase());
            });

            var x = 0;
            function cevapla(answer_id, answer_string, question_id){

                //alert('Answer id:' + answer_id + 'Answer_string:' + answer_string + 'Question id:' + question_id);
                x+=1;
                console.log('Cevap verildi ' + x);
                $.ajax({
                    url: "{{ url('senkroncevapla') }}",
                    type: 'POST',
                    data:{"examId" : {{ $exam->id }}, "cevapId" : answer_id, "questionId" : question_id, "answer_string" : answer_string, "start_time" : start_time },
                    dataType : 'json'
                })
                .done(function(data) {

                    var answer_string2 = (answer_string == "" ? "bos21012013" : answer_string);

                    socket.emit('cevapRaporGir', { "cevapId" : answer_id, "answer_string" : answer_string2, "start_time" : start_time, "end_time" : data.end_time });

                })
                .fail(function(jqXHR, textStatus) {
                    alert('Beklenmedik bir hata oluştu');
                });
            }

            // Get context with jQuery - using jQuery's .get() method.
            var ctx = $("#myChart").get(0).getContext("2d");
            var myBarChart;
            socket.on('raporGetir', function(data){
                if(myBarChart!=undefined)
                    myBarChart.destroy();

                $('#loader-wrapper').hide();
                $('#soruAlani').hide();
                $('#raporAlani').show();

                var label_text = [];
                var cevap_data = [];
                if(type=="coktan"){

                    //Boş cevap varsa onu ekletiyor.
                    if(data.cevap_veri.hasOwnProperty("0")){
                        label_text.push("Boş");

                        cevap_data.push(data.cevap_veri["0"]);
                    }


                    for(i = 0; i < data.secenekler.length; i++) {
                        label_text.push((i + 1) + ".Seçenek");

                        if(data.cevap_veri.hasOwnProperty(data.secenekler[i].id))
                            cevap_data.push(data.cevap_veri[data.secenekler[i].id]);
                        else
                            cevap_data.push("0");
                    }
                }
                else{
                    for(var k in data.cevap_veri) {
                        k == "bos21012013" ? label_text.push("Boş"): label_text.push(k);

                        cevap_data.push(data.cevap_veri[k]);
                    }
                }

                var graphic_data = {
                labels: label_text,
                datasets: [
                    {
                        label: "My first dataset",
                        fillColor: "rgba(151,187,205,0.5)",
                        strokeColor: "rgba(151,187,205,0.8)",
                        highlightFill: "rgba(151,187,205,0.75)",
                        highlightStroke: "rgba(151,187,205,1)",
                        data: cevap_data
                    }
                ]};

                myBarChart = new Chart(ctx).Bar(graphic_data, {});

                //Seçenekler
                var secenek_html = "";
                if(type == "coktan"){
                    for (var prop in data.secenekler) {

                      secenek_html+="<div><button type='button' disabled class='buton'>" + data.secenekler[prop].cevap + "</button></div>";
                    }
                }

                $('#secenekRapor').html(secenek_html);

            });

            socket.on('sinavBitir', function(){
                $.ajax({
                    url: "{{ url('senkronsinavbitir') }}",
                    type: 'POST',
                    data:{"examId" : {{ $exam->id }} },
                    dataType : 'json'
                })
                .done(function(){

                    alert('Sınavınız bitti');
                    window.location.href = "{{ url('/') }}";

                })
                .fail(function(jqXHR, textStatus) {
                    alert('Beklenmedik bir hata oluştu');
                });
            });

            socket.on('ban', function(){
                console.log('banlandım');
                window.location.href = "{{ url('/') }}";
            });


        });
    </script>
@endsection

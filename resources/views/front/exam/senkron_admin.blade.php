@extends('front.layouts.minimal')

@section('title', $exam->name)

@section('content')
<div id="giris">
    <div class="container-fluid">
    	<div class="row senkron-giris-ust">
    		<div class="col-md-4">
                <div class="senkron-kullanici-sayi text-center">
                    <span>0</span>
                    <p>Kullanıcı</p>
                </div>
            </div>
            <div class="col-md-4 senkron-orta">
    			<p class="bilgi">Sınava girmesini istediğiniz kişilere bu kodu paylaşın</p>
    			<h3>{{ $exam->code }}</h3>
    		</div>
            <div class="col-md-4">
                <div class="senkron-baslat text-center">
                    <a href="#" id="baslat" class="buton">Başlat</a>
                    <a href="#" id="ileri" class="buton" style="display:none;">İleri</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul id="kullaniciListesi">

                </ul>
            </div>
        </div>
    	</div>
    </div>
</div>

<div id="soruAlani" style="display:none;">
    @include('front.exam.question')
</div>

<div id="raporAlani" style="display:none;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <canvas id="myChart" width="400" height="400"></canvas>
            </div>
            <div class="col-md-12">
                <div id="secenekRapor">

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
    <script src="{{ asset('assets/js/jquery.progressTimer.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>

    <script>
        $(function() {
            $('#giris').show();
            $('#soruAlani').hide();
            $('#raporAlani').hide();

            $('body').css("background" , "#50514F");

            $('.senkron-kullanici-sayi').css("margin-top" , function(){ return ($(this).parents('.row').outerHeight() / 2 - $(this).outerHeight() / 2) });
            $('.senkron-baslat').css("margin-top" , function(){ return ($(this).parents('.row').outerHeight() / 2 - $(this).outerHeight() / 2) });


            var socket_client_url = "{{ config('app.socket_client_url') }}";
            var socket = io(socket_client_url);
            var nicknames2 = {};

            socket.on('connect', function(data){
            	socket.emit('baglan', {
            		room : '{{ $exam->code }}',
            		user : { id : '{{ auth()->user()->id }}', name : '{{ auth()->user()->name }}', email : '{{ auth()->user()->email }}' }
            	});
            });

            socket.on('kullaniciListele', function(kullanicilar){
            	var html = '';
            	nicknames2 = kullanicilar;
                var y = 0;
            	$.each(kullanicilar, function(index, value){
                    y+=1;
            		html += "<li><a href='#' data-socket='" + value.socketId + "' class='kullanici'>" + value.name + "</a></li>";
            	});

                $('.senkron-kullanici-sayi span').html(y);
            	$('#kullaniciListesi').html(html);
            });

            $(document).on('click', '.kullanici', function(e){
                e.preventDefault();
                socket.emit('banla', $(this).data('socket'));
            });

            $(document).on('click', '#baslat', function(e){
                e.preventDefault();
                if(Object.keys(nicknames2).length > 0){
                    var _that = $(this);
                    $.ajax({
                        url: "{{ url('senkronverial') }}",
                        type: 'POST',
                        data:{"code" : "{{ $exam->code }}", "nicknames" : nicknames2 },
                        dataType : 'json'
                    })
                    .done(function(data) {
                        $(_that).hide();
                        socket.emit('baslat', { "sorular" : data.sorular, "kategoriler" : data.category_names});
                    })
                    .fail(function(jqXHR, textStatus) {
                        Swal.fire('Beklenmedik bir hata oluştu');
                    });
                }
                else
                    Swal.fire('Uyarı', 'Sınava kimse gelmeden başlanamaz.', 'error');

            });

            var sayac;
            var start_time;
            var type;
            socket.on('soruGetir', function(data){

                start_time = data.start_time;
                type = data.soru.type;


                //$('#giris').hide();
                $('.senkron-orta .bilgi').html("Cevaplayan Kişi Sayısı");
                $('.senkron-orta h3').html("0");
                $('#ileri').hide();
                $('#kullaniciListesi').parents('.row').hide();
                $('#raporAlani').hide();
                $('#soruAlani').show();

                //Kaçıncı soruda olduğunu belirten progress
                $('#soruBar .progress-bar').css("width", Math.round((100 / data.soru_sayisi) * data.kacinci_soru) + "%");
                $('#soruBar .progress-bar span').html("Soru" + data.kacinci_soru + ' / ' + data.soru_sayisi);

                //input hiddenlar
                $('#questionId').val(data.soru.id);
                $('#questionType').val(data.soru.type);
                //Kalan süre progress barı.
                sayac = $("#progressTimer").progressTimer({
                    timeLimit: data.soru.time,
                    warningThreshold: Math.round(data.soru.time / 4),
                    baseStyle: 'progress-bar-info',
                    warningStyle: 'progress-bar-danger',
                    onFinish: function() {
                        setTimeout(function(){
                            socket.emit('cevapRapor');
                        }, 3000);

                    }
                });
                //Soru İçeriği
                $('#icerikDiv').html(data.soru.content);
                if(data.soru.description != ""){
                    $('#icerikDiv').prepend('<div class="ipucu"><button type="button" class="buton" data-toggle="modal" data-target="#ipucuModal">&#63;</button></div>');
                    $('#ipucuModal .modal-body').html(data.soru.description);
                }
                if(data.kategori != "bos21012013")
                    $('#icerikDiv').prepend('<div class="kategori">' + data.kategori + '</div>');

                //Seçenekler
                var secenek_html = "";
                if(data.soru.type == "coktan"){
                    for (var prop in data.soru.answers) {

                      secenek_html+="<div><button type='button' disabled class='buton'>" + data.soru.answers[prop].cevap + "</button></div>";
                    }
                }
                else{
                    secenek_html+= "<div class='soru-bosluk'><input type='text' id='cevapText' placeholder='cevabınızı yazın' value=''><button type='button' disabled class='buton'>Cevapla</button><div class='clearfix'></div></div>";
                }

                $('#secenekDiv').html(secenek_html);

            });

            socket.on("cevapGeldi", function(data){
                $('.senkron-orta h3').html(data.sayi);
            });

            // Get context with jQuery - using jQuery's .get() method.
            var ctx = $("#myChart").get(0).getContext("2d");
            var myBarChart;
            socket.on('raporGetir', function(data){
                if(myBarChart!=undefined)
                    myBarChart.destroy();

                //$('#giris').hide();
                $('#soruAlani').hide();
                $('#raporAlani').show();
                $('#ileri').show();

                var label_text = [];
                var cevap_data = [];
                if(type=="coktan"){

                    //Boş cevap varsa onu ekletiyor.
                    if(data.cevap_veri.hasOwnProperty("0")){
                        label_text.push("Boş");

                        cevap_data.push(data.cevap_veri["0"]);
                    }

                    for(i = 0; i < data.secenekler.length; i++) {
                        label_text.push((i + 1) + ".Seçenek");

                        if(data.cevap_veri.hasOwnProperty(data.secenekler[i].id))
                            cevap_data.push(data.cevap_veri[data.secenekler[i].id]);
                        else
                            cevap_data.push("0");
                    }
                }
                else{
                    for(var k in data.cevap_veri) {
                        k == "bos21012013" ? label_text.push("Boş"): label_text.push(k);

                        cevap_data.push(data.cevap_veri[k]);
                    }
                }

                var graphic_data = {
                labels: label_text,
                datasets: [
                    {
                        label: "My first dataset",
                        fillColor: "rgba(151,187,205,0.5)",
                        strokeColor: "rgba(151,187,205,0.8)",
                        highlightFill: "rgba(151,187,205,0.75)",
                        highlightStroke: "rgba(151,187,205,1)",
                        data: cevap_data
                    }
                ]};

                myBarChart = new Chart(ctx).Bar(graphic_data, {});

                //Seçenekler
                var secenek_html = "";
                if(type == "coktan"){
                    for (var prop in data.secenekler) {

                      secenek_html+="<div><button type='button' disabled class='buton'>" + data.secenekler[prop].cevap + "</button></div>";
                    }
                }

                $('#secenekRapor').html(secenek_html);

            });

            $(document).on('click', '#ileri', function(e){
                e.preventDefault();
                socket.emit('ileri');
            });

            socket.on('sinavBitir', function(){
                $.ajax({
                  url: "{{ url('panel/sinavlar/bitir') }}" + '/' + {{ $exam->id }},
                  type: 'POST',
                  data: { "sayfa" : "socket" }
                })
                .done(function() {
                    alert('Sınav bitirildi');
                    window.location.href = "{{ url('/') }}";

                })
                .fail(function(jqXHR, textStatus) {
                  Swal.fire('Beklenmedik bir hata oluştu!' + textStatus);
                });
            });

        });
    </script>
@endsection

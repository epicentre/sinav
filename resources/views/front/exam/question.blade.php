@if(isset($question))
<div class="container-fluid soru-container">
    <div class="row">
        <div class="col-md-6 col-center">
            <div class="progress">
              <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {{  round(((100 / session('exam.exam_questions_count')) * (1 + session('exam.exam_questions_count') - count(session('exam.exam_questions'))))) }}%">
                <span>Soru {{ (1 + session('exam.exam_questions_count') - count(session('exam.exam_questions'))).' / '.session('exam.exam_questions_count') }}</span>
              </div>
            </div>
            <div id="progressTimer"></div>
            <input type="hidden" id="questionId" value="{{ $question->id }}">
            <input type="hidden" id="questionType" value="{{ $question->type }}">
            <div id="icerikDiv" class="kagit">
                @if($category_name<>"")
                    <div class="kategori">{{ $category_name }}</div>
                @endif
                @if($question->description<>"")
                <div class="ipucu">
                    <button type="button" class="buton" data-toggle="modal" data-target="#ipucuModal">&#63;</button>
                </div>
                @endif
                {!! $question->content !!}
            </div>
            <div id="secenekDiv" class="secenek-div">
                <?php
                    if($question->type=="coktan")
                        $question_answers = session('exam.answer_shuffle')==1 ? $question->answers->shuffle() : $question->answers;
                ?>
                
                @if($question->type=="coktan")
                    @foreach($question_answers as $answer)
                        <a href="#" id="cevap" data-id="{{ $answer->id }}">{{ $answer->cevap }}</a>
                    @endforeach
                @else
                    <div class="soru-bosluk">                            
                        <input type="text" id="cevapText" placeholder="cevabınızı yazın" value="">
                        <a href="#" id="cevap" data-id="{{ $question->answers->first()->id }}">Cevapla</a>
                        <div class="clearfix"></div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@else
<div class="container-fluid soru-container">
    <div class="row">
        <div class="col-md-6 col-center vertical-align">
            <div class="progress" id="soruBar">
              <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">
                <span></span>
              </div>
            </div>
            <div id="progressTimer"></div>
            <input type="hidden" id="questionId" value="">
            <input type="hidden" id="questionType" value="">
            <div id="icerikDiv" class="kagit">

            </div>
            <div id="secenekDiv" class="secenek-div">


            
            </div>
        </div>
    </div>
</div>
@endif

<div class="modal fade" id="ipucuModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="ipucuModalLabel">İpucu</h4>
      </div>
      <div class="modal-body">
            @if(isset($question) && $question->description<>"")
                {{ $question->description }}
            @endif
      </div>
    </div>
  </div>
</div>
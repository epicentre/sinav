<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Sınav Merkezi</title>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Animate Css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css">
    <!-- Superslide css file-->
    <link rel="stylesheet" href="{{ asset('assets/front/css/superslides.css') }}">
    <!-- Slick slider css file -->
    <link href="{{ asset('assets/front/css/slick.css') }}" rel="stylesheet">
    <!-- Default Theme css file -->
    <link href="{{ asset('assets/front/css/default-theme.css') }}" rel="stylesheet">

    <!-- Main structure css file -->
    <link href="{{ asset('assets/front/css/front_style.css') }}" rel="stylesheet">

    <!-- Google fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Varela' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>

    <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>

    <header id="header">
        <div class="menu_area">
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <!-- LOGO -->

                        <!-- TEXT BASED LOGO -->
                        <a class="navbar-brand" href="#">Sınav <span>Merkezi</span></a>

                        <!-- IMG BASED LOGO  -->
                        <!--  <a class="navbar-brand" href="#"><img src="img/logo.png" alt="logo"></a> -->
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul id="top-menu" class="nav navbar-nav navbar-right main_nav">
                            <li class="active"><a href="#">Anasayfa</a></li>
                            <li><a href="#sinavlar">Sınavlar</a></li>
                            <li><a href="#ozellikler">Özellikler</a></li>
                            <li><a href="#kullanim">Kullanım</a></li>
                            <li><a href="{{ url('kodilegir') }}" id="except">Kod ile Sınava Gir</a></li>
                            @if (Auth::guest())
                                <li><a href="{{ url('/login') }}" id="except">Giriş</a></li>
                                <li><a href="{{ url('/register') }}" id="except">Kayıt Ol</a></li>
                            @else
                                <li><a href="{{ url('panel') }}" id="except">Panelim</a></li>
                                <li class="dropdown">
                                    <a href="#" id="except" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{ url('/logout') }}" id="except"><i class="fa fa-btn fa-sign-out"></i>Çıkış</a></li>
                                    </ul>
                                </li>
                            @endif

                        </ul>
                    </div><!--/.nav-collapse -->
                </div>
            </nav>
        </div>

        <div class="slider_area">
            <div id="slides">
                <ul class="slides-container">
                    <li>
                        <div class="slider_overlay"></div>
                        <img src="{{ asset('assets/front/img/full-slider/full-slide7.jpg') }}" alt="img">
                        <div class="slider_caption">
                            <h2>Hi,We are Ultramodern Single pro</h2>
                            <p>we are a group of experienced designers and developers</p>
                            <a href="#" class="slider_btn">Who We are</a>
                        </div>
                    </li>

                    <li>
                        <div class="slider_overlay"></div>
                        <img src="{{ asset('assets/front/img/full-slider/full-slide2.jpg') }}" alt="img">
                        <div class="slider_caption">
                            <h2>Hi,We are Ultramodern Single pro</h2>
                            <p>we are a group of experienced designers and developers</p>
                            <a href="#" class="slider_btn">Who We are</a>
                        </div>
                    </li>

                    <li>
                        <div class="slider_overlay"></div>
                        <img src="{{ asset('assets/front/img/full-slider/full-slide1.jpg') }}" alt="img">
                        <div class="slider_caption">
                            <h2>we'll change your Idea of Design</h2>
                            <p>we are a group of experienced designers and developers</p>
                            <a href="#" class="slider_btn">Who We are</a>
                        </div>
                    </li>
                </ul>

                <nav class="slides-navigation">
                <!-- PREV IN THE SLIDE -->
                    <a class="prev" href="/item1">
                        <span class="icon-wrap"></span>
                        <h3><strong>Önceki</strong></h3>
                    </a>
                    <a class="next" href="/item3">
                        <span class="icon-wrap"></span>
                        <h3><strong>Sonraki</strong></h3>
                    </a>
                </nav>
            </div>
        </div>
    </header>

    <section id="sinavlar">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="heading">
                        <h2 class="wow fadeInLeftBig">Sınavlar</h2>
                    </div>
                    <div class="team_area">
                        <?php
                            setlocale(LC_TIME, 'tr_TR@TL', 'tr_TR', 'tr', 'Turkish');
                        ?>
                        <div class="team_slider">
                            @foreach($exams as $exam)
                            <div class="col-lg-4 col-sm-6">
                                <div class="single_team wow fadeInUp">
                                    <h3>
                                        {{ $exam->name }}
                                        <span>{{ $exam->user->name }}</span>
                                    </h5>
                                    <ul>
                                        <li>
                                            <strong>Başlangıç Tarihi:</strong>
                                            {{ \Carbon\Carbon::parse($exam->start_time)->formatLocalized('%d %B %Y %H:%M') }}
                                        </li>
                                        <li>
                                            <strong>Tahmini Bitiş tarihi:</strong>
                                            {{ is_null($exam->end_time) ? 'Belli Değil' : \Carbon\Carbon::parse($exam->end_time)->formatLocalized('%d %B %Y %H:%M') }}
                                        </li>
                                        <li>
                                            <strong>Sınav Kodu:</strong>
                                            <span class="label label-success" style="color:#FFF; font-size: 120%;">{{ $exam->code }}</span>
                                        </li>
                                        <li>
                                            <strong>Soru Sayısı:</strong>
                                            {{ $exam->exam_questions()->count() }}
                                        </li>
                                        <li>
                                            <strong>Çözen Kişi Sayısı:</strong>
                                            {{ $exam-> exam_reports()->whereNotNull('end_time')->count() }}
                                        </li>
                                        <li style="text-align:center;">
                                            <a href="{{ url('sinav/'.$exam->code) }}" id="sinavBaslat" data-adi="{{ $exam->name }}" data-kod="{{ $exam->code }}" class="btn btn-lg btn-warning"><span class="glyphicon glyphicon-hourglass" style="color:#FFF;"></span>Sınavı Başlat</a>
                                            <div class="clearfix"></div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="ozellikler">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="heading">
                        <h2 class="wow fadeInLeftBig">Our Services</h2>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="service_area">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="single_service wow fadeInLeft">
                                    <div class="service_iconarea">
                                        <span class="fa fa-line-chart service_icon"></span>
                                    </div>
                                    <h3 class="service_title">Planning & Strategy</h3>
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text</p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="single_service wow fadeInRight">
                                    <div class="service_iconarea">
                                        <span class="fa fa-suitcase service_icon"></span>
                                    </div>
                                    <h3 class="service_title">Corporate Branding</h3>
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text</p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="single_service wow fadeInLeft">
                                    <div class="service_iconarea">
                                        <span class="fa fa-eraser service_icon"></span>
                                    </div>
                                    <h3 class="service_title">Web Desing & Development</h3>
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text</p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="single_service wow fadeInRight">
                                    <div class="service_iconarea">
                                        <span class="fa fa-paper-plane service_icon"></span>
                                    </div>
                                    <h3 class="service_title">SEO,SMM and Internet marketing</h3>
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text</p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="single_service wow fadeInLeft">
                                    <div class="service_iconarea">
                                        <span class="fa fa-envelope-o service_icon"></span>
                                    </div>
                                    <h3 class="service_title">Email Marketing</h3>
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text</p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="single_service wow fadeInRight">
                                    <div class="service_iconarea">
                                        <span class="fa fa-support service_icon"></span>
                                    </div>
                                    <h3 class="service_title">Premium Customer SUpport</h3>
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text</p>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="kullanim">

        <section class="adim-normal">
            <div class="container wow bounceInLeft">
                <div class="row">
                    <div class="col-md-8 step-desc">
                        <div class="col-md-2 text-center">
                            <div class="step-no">
                                <span class="no-inner">1</span>
                            </div>
                        </div>

                        <div class="col-md-10 step-details">
                            <h3 class="step-title">Work flow title here</h3>
                            <p class="step-description">Cillum laboris <strong>consequat</strong>, qui elit retro next level skateboard freegan hella. Cillum laboris consequat qui elit retro next level skateboard freegan hella. Cillum laboris consequat skateboard freegan hella</p>
                        </div> <!-- End step-details -->
                    </div>
                    <div class="col-md-4">
                        <img src="{{ asset('assets/front/img/note.png') }}" class="img-responsive" alt="">
                    </div>
                </div>
            </div>
        </section>

        <section class="adim-siyah">
            <div class="container wow bounceInRight">
                <div class="row">
                    <div class="col-md-4 step-img">
                        <img src="{{ asset('assets/front/img/desk.png') }}" class="img-responsive" alt="">
                    </div>
                    <div class="col-md-8 step-desc">
                        <div class="col-md-2 text-center">
                            <div class="step-no">
                                <span class="no-inner">2</span>
                            </div>
                        </div>

                        <div class="col-md-10 step-details">
                            <h3 class="step-title">Work flow title here</h3>
                            <p class="step-description">Cillum laboris <strong>consequat</strong>, qui elit retro next level skateboard freegan hella. Cillum laboris consequat qui elit retro next level skateboard freegan hella. Cillum laboris consequat skateboard freegan hella</p>
                        </div><!-- End step-details -->
                    </div>
                </div>
            </div>
        </section>
    </section>

    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="footer_left">
                        <p>Copyright &copy; 2016 <a href="http://www.arfdmr.me">Arif DEMİR</a>. Tüm Hakları Saklıdır</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="footer_right">
                        <ul class="social_nav">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <!-- For smooth animatin  -->
    <script src="{{ asset('assets/front/js/wow.min.js') }}"></script>
    <!-- superslides slider -->
    <script src="{{ asset('assets/front/js/jquery.superslides.min.js') }}" type="text/javascript"></script>
    <!-- slick slider -->
    <script src="{{ asset('assets/front/js/slick.min.js') }}"></script>
    <!-- Custom js-->
    <script src="{{ asset('assets/front/js/custom.js') }}"></script>
    <!-- Sweet Alert -->
    @include('sweetalert::alert')

    <script src="{{ asset('assets/js/jquery.progressTimer.js') }}"></script>
    <script>
        $(function(){

        $(document).on('click', '#sinavBaslat', function(e){
          var th = $(this);
          e.preventDefault();
          Swal.fire({
            title: $(th).data('adi') + ' adlı sınava girmek istediğine emin misin?',
            type: "info",
            showCancelButton: true,
            cancelButtonText: 'İptal',
            confirmButtonText: "Evet, eminim",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
          },
          function(){
            $.ajax({
                url: "{{ url('sinavgiriskontrol') }}" + '/' + $(th).data('kod'),
                type: 'POST',
                dataType : 'json'
              })
              .done(function(data) {
                if(data.message=="ok")
                  window.location.href = $(th).attr('href');
                else if(data.message == "cozulmus")
                  Swal.fire('Uyarı', 'Bu sınavı çözmüşsün', 'info');
                else if(data.message=="yarim")
                  Swal.fire({
                    title: "Uyarı",
                    text: "Yarıda kalan sınavınız bulunmaktadır, eğer diğer sekme veya pencerede sınav açık değilse devam et'e tıklayıp sınava kaldığın yerden devam edebilirsin",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Devam Et",
                    cancelButtonText: "İptal",
                    closeOnConfirm: false },
                    function(){
                      window.location.href = "{{ url('sinav/'.session('exam.exam_code')) }}"
                  });
              })
              .fail(function(jqXHR, textStatus) {
                if(jqXHR.status == 401)
                  Swal.fire('Hata', 'Sınava girmek için giriş yapmalısın', 'error');
                else
                  Swal.fire('Hata', 'Beklenmedik bir hata oluştu!', 'error');
              });
          });
        });

      });
    </script>
</body>
</html>

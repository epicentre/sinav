@extends('front.layouts.minimal')

@section('title', 'Sınava Gir')

@section('content')
<div class="kod-giris-container">
    <div class="kod-giris-logo">
        <a href="{{ url('/') }}"><b>Sınav</b> Merkezi</a>
    </div>
    <div class="kod-giris-icerik">
        <input type="text" id="code" class="animated bounce" placeholder="kodu girin" value="">
        <a href="#" title="" id="kodGiris">Giriş</a>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Canlı Sınav Kuralları</h4>
      </div>
      <div class="modal-body">
        <p>
          <ul>
            <li>Sınavının raporunu görmek ve sınavın tamamlanmış olması için tüm soruları cevaplamalısın</li>
            <li>Sınav anında internet bağlantın koparsa veya sınav penceresini kapatırsan sınavın tamamlanmış sayılmayacak</li>
            <li><b>Bu yüzden sınav anında sayfa yenileme, sayfayı kapatma gibi işlemler yapılmamalıdır</b></li>
            <li>Sınav sonucunu panelinde Sınav Sonuçlarım sayfasından görebilirsin</li>
            <li>Sınav sonucu 100'lük sisteme göre puanlandırılacaktır</li>
          </ul>
        </p>
      </div>
      <div class="modal-footer">
        <a href="#" class="btn btn-primary">Anladım ve Sınava Girmek İstiyorum</a>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@section('scripts')
<script>
    $(function(){
        $('body').css({"animation": "colorchange 50s infinite", "-webkit-animation": "colorchange 50s infinite"});

        $('#kodGiris').on('click', ajaxKontrol);

        function ajaxKontrol(e){
            e.preventDefault();
            if($('#code').val() != ""){
                var that = $(this);
                that.off('click'); // remove handler
                $.ajax({
                    url: "{{ url('sinavgiriskontrol') }}" + '/' + $('#code').val(),
                    type: 'POST',
                    dataType : 'json'
                })
                .done(function(data) {
                    if(data.message=="ok")
                        window.location.href = "{{ url('sinav') }}" + '/' + $('#code').val();
                    else if(data.message == "senkrongiris"){
                        $('.modal .modal-footer a').attr("href", "{{ url('senkron') }}" + "/" + $('#code').val());
                        swal.close();
                        $('.modal').modal({"backdrop" : 'static'});
                    }
                    else if(data.message == "cozulmus")
                        Swal.fire('Uyarı', 'Bu sınavı çözmüşsün', 'info');
                    else if(data.message=="yarim")
                        Swal.fire({
                        title: "Uyarı",
                        text: "Yarıda kalan sınavınız bulunmaktadır, eğer diğer sekme veya pencerede sınav açık değilse devam et'e tıklayıp sınava kaldığın yerden devam edebilirsin",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Devam Et",
                        cancelButtonText: "İptal",
                        closeOnConfirm: false },
                        function(){
                          window.location.href = "{{ url('sinav/'.session('exam.exam_code')) }}"
                        });
                    else if(data.message == "sinavyok")
                        Swal.fire('Uyarı', 'Girdiğin kodla uyuşan sınav bulunamadı', 'warning');
                })
                .fail(function(jqXHR, textStatus) {
                    if(jqXHR.status == 401)
                        Swal.fire({
                        title: "Uyarı",
                        text: "Sınava girmek için giriş yapmalısın",
                        type: "info",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Giriş Yap",
                        closeOnConfirm: false},
                        function(){
                          window.location.href = "{{ url('login') }}";
                        });
                    else if(jqXHR.status == 404)
                        Swal.fire('Uyarı', 'Girdiğin kodla uyuşan sınav bulunamadı', 'warning');
                    else
                        Swal.fire('Hata', 'Beklenmedik bir hata oluştu!', 'error');
                })
                .always(function() {
                    that.on('click', ajaxKontrol); // add handler back after ajax
                });
            }
        }
    });
</script>
@endsection

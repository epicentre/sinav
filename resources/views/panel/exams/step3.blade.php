@extends('panel.layouts.template')

@section('title', 'Sınav Ekleme')

@section('breadcrumbs')
  @if(request()->segment(3)=="yeni")
    <section class="content-header">
      <h1>
        Sınav Ekle
        <small>Adım 3 - Sınav ekleme sayfası</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('panel') }}"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
        <li><i class="fa fa-trophy"></i> Sınav İşlemleri</li>
        <li class="active"><i class="fa fa-plus"></i> Sınav Ekle</li>
      </ol>
    </section>
  @elseif(request()->segment(3)=="duzenle")
    <section class="content-header">
      <h1>
        Sınav Düzenle
        <small>Adım 3 - Sınav düzenleme sayfası</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('panel') }}"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
        <li><i class="fa fa-trophy"></i> Sınav İşlemleri</li>
        <li class="active"><i class="fa fa-edit"></i> Sınav Düzenle</li>
      </ol>
    </section>
  @endif
@endsection

@section('content')

<?php
	if(strpos(request()->url(), 'duzenle')!==false){
		$sessionName = "editStep3";
		$backUrl = url('panel/sinavlar/duzenle/'.request()->segment(4).'/2');
	}
	else{
		$sessionName = "step3";
		$backUrl = url('panel/sinavlar/yeni/2');
	}
?>

<div class="row">
	<div class="col-xs-12">
		<div class="box box-default">
			<form class="form-horizontal" role="form" method="POST" action="">
			{{  csrf_field() }}
			<div class="box-body">
				<div class="progress active">
					<div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="3" style="width: 100%;">
					Adım 3 / 3
					</div>
				</div>

				<div class="row">
					<div class="col-md-3 col-sm-6">
						<input type="radio" class="sinavTR" name="theme" value="default" checked>
						<label>Seç</label>
						<div class="small-box bg-aqua">
			                <div class="inner">
			                  <h3>&nbsp;</h3>
			                  <p>Varsayılan Tema</p>
			                </div>
			                <div class="icon">
			                  &nbsp;
			                </div>
		                </div>
					</div>

					<div class="col-md-3 col-sm-6">
						<input type="radio" class="sinavTR" name="theme" value="yilbasi">
						<label>Seç</label>
						<div class="small-box bg-green">
			                <div class="inner">
			                  <h3>&nbsp;</h3>
			                  <p>Yılbaşı Teması</p>
			                </div>
			                <div class="icon">
			                  &nbsp;
			                </div>
		                </div>
					</div>

					<div class="col-md-3 col-sm-6">
						<input type="radio" class="sinavTR" name="theme" value="cocuk">
						<label>Seç</label>
						<div class="small-box bg-yellow">
			                <div class="inner">
			                  <h3>&nbsp;</h3>
			                  <p>Çocuk Teması</p>
			                </div>
			                <div class="icon">
			                  &nbsp;
			                </div>
		                </div>
					</div>

					<div class="col-md-3 col-sm-6">
						<input type="radio" class="sinavTR" name="theme" value="modern">
						<label>Seç</label>
						<div class="small-box bg-red">
			                <div class="inner">
			                  <h3>&nbsp;</h3>
			                  <p>Modern Tema</p>
			                </div>
			                <div class="icon">
			                  &nbsp;
			                </div>
		                </div>
					</div>
				</div>

				<div class="row">

					<div class="col-sm-12">
						<a href="{{ $backUrl }}" class="btn btn-info pull-left" style="margin-top:20px;">Geri</a>

			 			<input type="submit" class="btn btn-info pull-right" style="margin-top:20px;" value="Kaydet">
		 		 	</div>

					<div class="clearfix"></div>
				</div>

			</div>
			</form>

		</div><!-- box -->
	</div><!-- col -->
</div><!-- row -->
@endsection

@section('scripts')

<script>
	$(function(){
		$('input[type="radio"].sinavTR').each(function(){
		    var self = $(this),
		      label = self.next(),
		      label_text = label.text();

		    label.remove();
		    self.iCheck({
		      radioClass: 'icheckbox_line-grey',
		      insert: '<div class="icheck_line-icon"></div>' + label_text
		    });
	  	});
	});
</script>
@endsection
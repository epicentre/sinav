@extends('panel.layouts.template')

@section('title', 'Sınav Ekleme')

@section('head')
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('assets/css/bootstrap-datetimepicker.min.css') }}">
@endsection

@section('breadcrumbs')
  @if(request()->segment(3)=="yeni")
    <section class="content-header">
      <h1>
        Sınav Ekle
        <small>Adım 1 - Sınav ekleme sayfası</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('panel') }}"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
        <li><i class="fa fa-trophy"></i> Sınav İşlemleri</li>
        <li class="active"><i class="fa fa-plus"></i> Sınav Ekle</li>
      </ol>
    </section>
  @elseif(request()->segment(3)=="duzenle")
    <section class="content-header">
      <h1>
        Sınav Düzenle
        <small>Adım 1 - Sınav düzenleme sayfası</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('panel') }}"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
        <li><i class="fa fa-trophy"></i> Sınav İşlemleri</li>
        <li class="active"><i class="fa fa-edit"></i> Sınav Düzenle</li>
      </ol>
    </section>
  @endif
@endsection

@section('content')

<?php
	if(strpos(request()->url(), 'duzenle')!==false){
		$sessionName = "editStep1";
	}
	else{
		$sessionName = "step1";
	}
?>
<div class="row">
	<div class="col-xs-12">
		<div class="box box-default">
			<form class="form-horizontal" role="form" method="POST" action="">
			{{  csrf_field() }}
			<div class="box-body">
				<div class="progress active">
					<div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="3" style="width: 50%;">
					Adım 1 / 2
					</div>
				</div>

				<div class="form-group @if($errors->has('name')) has-error @endif">
		            <label for="name" class="col-sm-offset-2 col-sm-2 control-label">Sınav Adı</label>
		            <div class="col-sm-6">
		              <input type="text" class="form-control" name="name" id="name" value="{{ old('name', session()->has($sessionName.'.name') ? session($sessionName.'.name') : (isset($exam->name) ? $exam->name : '')) }}">
		              @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
	            	</div>
            	</div>

            	<div class="form-group">
		            <label for="time_manual" class="col-sm-offset-2 col-sm-2 control-label">Tarih</label>
		            <div class="col-sm-6">
		              <select class="form-control" name="time_manual" id="time_manual">
		              	<option value="0" @if(old('time_manual', session()->has($sessionName.'.time_manual') ? session($sessionName.'.time_manual') : (isset($exam->time_manual) ? $exam->time_manual : ''))=="0") selected @endif>Belirttiğim Aralık</option>
		              	<option value="1" @if(old('time_manual', session()->has($sessionName.'.time_manual') ? session($sessionName.'.time_manual') : (isset($exam->time_manual) ? $exam->time_manual : ''))=="1") selected @endif>Manuel</option>
		              </select>
	            	</div>
            	</div>

				<div id="tarihAraligi">
            	<div class="form-group @if($errors->has('start_time')) has-error @endif">
                    <label class="col-sm-offset-2 col-sm-2 control-label">Başlangıç Tarihi</label>
                    <div class="col-sm-6">
                    	<div class="input-group">
	                      <input type="text" class="form-control" name="start_time" id="start_time" value="{{ old('start_time', session()->has($sessionName.'.start_time') ? session($sessionName.'.start_time') : (isset($exam->start_time) ? \Carbon\Carbon::parse($exam->start_time)->format('d.m.Y H:i') : date('d.m.Y H:i'))) }}">
	                      <div class="input-group-addon">
	                        <i class="fa fa-clock-o"></i>
	                      </div>
	                    </div><!-- /.input group -->
	                    @if ($errors->has('start_time')) <p class="help-block">{{ $errors->first('start_time') }}</p> @endif
                    </div>
                </div>

                <div class="form-group @if($errors->has('end_time')) has-error @endif">
                    <label class="col-sm-offset-2 col-sm-2 control-label">Bitiş Tarihi</label>
                    <div class="col-sm-6">
                    	<div class="input-group">
	                      <input type="text" class="form-control" name="end_time" id="end_time" value="{{ old('end_time', session()->has($sessionName.'.end_time') ? session($sessionName.'.end_time') : (isset($exam->end_time) ? \Carbon\Carbon::parse($exam->end_time)->format('d.m.Y H:i') : \Carbon\Carbon::now()->addDay(7)->format('d.m.Y H:i'))) }}">
	                      <div class="input-group-addon">
	                        <i class="fa fa-clock-o"></i>
	                      </div>
	                    </div><!-- /.input group -->
	                    @if ($errors->has('end_time')) <p class="help-block">{{ $errors->first('end_time') }}</p> @endif
                    </div>
                </div>
				</div>

				<div id="examType">
                <div class="form-group">
                    <label for="type" class="col-sm-offset-2 col-sm-2 control-label">Sınav Türü</label>
                    <div class="col-sm-6">
                    	<select class="form-control" name="type" id="type">
			              	<option value="senkron" @if(old('type', session()->has($sessionName.'.type') ? session($sessionName.'.type') : (isset($exam->type) ? $exam->type : ''))=="senkron") selected @endif>Eş zamanlı(senkron)</option>
			              	<option value="asenkron" @if(old('type', session()->has($sessionName.'.type') ? session($sessionName.'.type') : (isset($exam->type) ? $exam->type : ''))=="asenkron") selected @endif>Eş zamansız(asenkron)</option>
		              	</select>
                    </div>
                </div>
            	</div>

				<div id="shuffleCheckbox">
                <div class="form-group">
                  <div class="col-sm-6 col-sm-offset-4">
                  	<label>
                      <input name="question_shuffle" id="question_shuffle" type="checkbox" value="1" class="minimal" @if(old('question_shuffle', session()->has($sessionName) ? session($sessionName.'.question_shuffle') : (isset($exam->question_shuffle) ? $exam->question_shuffle : '' ))=="1") checked @endif>
                      Soruları karıştır
                    </label>
                    <br><br>
                    <label>
                      <input name="answer_shuffle" id="answer_shuffle" type="checkbox" value="1" class="minimal" @if(old('answer_shuffle', session()->has($sessionName) ? session($sessionName.'.answer_shuffle') : (isset($exam->answer_shuffle) ? $exam->answer_shuffle : '' ))=="1") checked @endif>
                      Cevapları karıştır
                    </label>
                  </div>

				</div>
				</div>
				<div class="form-group giris-izni">
                  <label class="col-sm-offset-2 col-sm-2 control-label">Giriş İzni</label>
                  <div class="col-sm-6" style="margin-top: 5px;">
                  	<label style="margin-right:15px;">
                      <input type="radio" name="r1" value="public" class="minimal" @if(old('r1', session($sessionName.'.r1'))=="public") checked @elseif(((isset($exam) && $exam->public==0) ? $exam->code : '') == '') checked @endif>
                      Herkese Açık
                    </label>
                    <label>
                      <input type="radio" name="r1" value="private" class="minimal" @if(old('r1', session($sessionName.'.r1'))=="private") checked @elseif(!session()->has($sessionName) && ((isset($exam) && $exam->public==0) ? $exam->code : '') <> '') checked @endif>
                      Kodla giriş
                    </label>
                  </div>
				</div>
				<div class="form-group kod" style="display:block;">
					<label class="col-sm-offset-2 col-sm-2 control-label">Sınav Kodu</label>
                  <div class="col-sm-6">
                  	<input type="hidden" name="code" id="code" value="<?php $kod = old('code', session()->has($sessionName.'.code') ? session($sessionName.'.code') : (isset($exam->code) ? $exam->code : strtolower(str_random(7)))); echo $kod; ?>">
                  	<span class="label label-success" style="font-size:20px;">{{ $kod }}</span>
                  </div>
				</div>
				<div class="clearfix"></div>
				<input type="submit" class="btn btn-info pull-right next" value="Kaydet ve İlerle">

			</div><!-- box-body -->
			</form>

		</div><!-- box -->
	</div><!-- col -->
</div><!-- row -->
@endsection

@section('scripts')
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment-with-locales.min.js"></script>
<script src="{{ asset('assets/js/bootstrap-datetimepicker.min.js') }}"></script>

<script>
	$(function(){
		moment.locale('tr');

		$('#start_time').datetimepicker({
			"locale" : "tr",
			minDate: moment($('#start_time').val(), "DD.MM.YYYY HH:mm"),
			showClose : true

		});

		$('#end_time').datetimepicker({
			"locale" : "tr",
			useCurrent:false,
			minDate : moment($('#start_time').val(), "DD.MM.YYYY HH:mm"),
			showClose : true
		});

		$("#start_time").on("dp.change", function (e) {
            $('#end_time').data("DateTimePicker").minDate(e.date);
        });
        $("#end_time").on("dp.change", function (e) {
            $('#start_time').data("DateTimePicker").maxDate(e.date);
        });

        if($('#time_manual').val()=="0")
	    {
	      $('#examType').hide();
	      $('#shuffleCheckbox').show();
	      $('#tarihAraligi').show();
	      $('.giris-izni').show();
	    }
	    else{
	    	$('#tarihAraligi').hide();
	      	$('#examType').show();
	    	if($('#type').val()=="senkron"){
				$('#shuffleCheckbox').hide();
				$('.giris-izni').hide();
			}
			else{
				$('#shuffleCheckbox').show();
				$('.giris-izni').show();
			}

	    }

		$('#time_manual').change(function(){
	    if($(this).val()=="0")
	    {
	      $('#examType').hide(1000);
	      $('#shuffleCheckbox').show(1000);
	      $('#tarihAraligi').show(1000);
	      $('.giris-izni').show(1000);
	    }
	    else{
	      $('#tarihAraligi').hide(1000);
	      $('#examType').show(1000);
	      if($('#type').val()=="senkron"){
	      	$('#shuffleCheckbox').hide(1000);
	      	$('.giris-izni').hide(1000);
	      }
	      else{
	      	$('#shuffleCheckbox').show(1000);
	      	$('.giris-izni').show(1000);
	      }
	    }
	  });

		if($('#type').val()=="senkron" && $('#time_manual').val()=="1"){
	      	$('#shuffleCheckbox').hide();
	      	$('.giris-izni').hide();
		}
	    else{
	    	$('#shuffleCheckbox').show();
	    	$('.giris-izni').show();
	    }

	  $('#type').change(function(){
	    if($(this).val()=="senkron"){
	      	$('#shuffleCheckbox').hide(1000);
	      	$('.giris-izni').hide(1000);
	  	}
	    else{
	    	$('#shuffleCheckbox').show(1000);
	    	$('.giris-izni').show(1000);
	    }
	  });

	  if($('input:radio[name=r1]:checked').val()=="private"){
		$('.kod').show();
	  }

	  $('input:radio[name=r1]').on('ifClicked', function(){
	    if($(this).val()=="private"){
	    	$('.kod').find('.label').html('<i class="fa fa-spin fa-refresh"></i>');
	    	$('.kod').show();
	    	$.ajax({
	          url: "{{ url('panel/sinavlar/kodgetir') }}",
	          type: 'POST',
	          dataType : 'json'
	        })
	        .done(function(data) {
	        	$('#code').val(data.code);
	        	$('.kod').find('.label').html(data.code);
	        })
	        .fail(function(jqXHR, textStatus) {
	          Swal.fire('Hata', 'Beklenmedik bir hata oluştu!' + textStatus, 'error');
	        });
	    }
	    //else
	      	//$('.kod').hide(1000);
	  });
	});
</script>
@endsection

@extends('panel.layouts.template')

@section('title', 'Sınav Ekleme')

@section('breadcrumbs')
  @if(request()->segment(3)=="yeni")
    <section class="content-header">
      <h1>
        Sınav Ekle
        <small>Adım 2 - Sınav ekleme sayfası</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('panel') }}"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
        <li><i class="fa fa-trophy"></i> Sınav İşlemleri</li>
        <li class="active"><i class="fa fa-plus"></i> Sınav Ekle</li>
      </ol>
    </section>
  @elseif(request()->segment(3)=="duzenle")
    <section class="content-header">
      <h1>
        Sınav Düzenle
        <small>Adım 2 - Sınav düzenleme sayfası</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('panel') }}"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
        <li><i class="fa fa-trophy"></i> Sınav İşlemleri</li>
        <li class="active"><i class="fa fa-edit"></i> Sınav Düzenle</li>
      </ol>
    </section>
  @endif
@endsection

@section('content')
<?php
	if(strpos(request()->url(), 'duzenle')!==false){
		$sessionName = "editStep2";
		$backUrl = url('panel/sinavlar/duzenle/'.request()->segment(4));
	}
	else{
		$sessionName = "step2";
		$backUrl = url('panel/sinavlar/yeni');
	}
?>
<div class="row">
	<div class="col-xs-12">
		<div class="box box-default">
			<form class="form-horizontal" role="form" method="POST" action="">
			{{  csrf_field() }}
			<input type="hidden" name="questions" id="questions" value="{{ session()->has($sessionName.'.questions') ? session($sessionName.'.questions') : (isset($exam_questions) ? $exam_questions : '') }}">
			<div class="box-body">
				<div class="progress active">
					<div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="3" style="width: 100%;">
					Adım 2 / 2
					</div>
				</div>

				<div class="alert alert-info alert-dismissable" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Kapat"><span aria-hidden="true">&times;</span></button>
	  				<strong>Not!</strong> Seçtiğiniz soruların başlığından tutup sürükleyerek sorularınız arasında sıralama yapabilirsiniz.
				</div>

				<div class="row" id="sortable" style="margin-left:0; margin-right:0;">

				</div>

				<div class="row" id="notSortable" style="margin-left:0; margin-right:0;">

					@if(count($questions) > 0)
					<?php $box = array("box-primary", "box-warning", "box-success") ?>
					@foreach($questions as $key=>$question)
					<div class="col-md-4 col-sm-6">
						<div class="box box-solid {{ $box[$key % 3] }}">
							<div class="box-header with-border">
								<h3 class="box-title">
									<input type="checkbox" class="sinavQC" value="{{ $question->id }}" {{ in_array($question->id, explode(",", session()->has($sessionName.'.questions') ? session($sessionName.'.questions') : (isset($exam_questions) ? $exam_questions : ''))) ? 'checked' : '' }}>
									<label>Seç</label>
								</h3>
								<div class="box-tools pull-right">
									<?php if($question->description<>""){ ?>
										<a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="İpucu"><i class="fa fa-question"></i></a>
									<?php } ?>
									<a href="#" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Küçült/Büyült"><i class="fa fa-minus"></i></a>
								</div>
							</div>
							<div class="box-body">
								<div class="soruIcerik">
									<div class="soru">
										{!! $question->content !!}
									</div>
									@foreach($question->answers as $answer)
										@if($question->type == 'bosluk')
										<?php
											$cevaplar = explode(',', $answer->cevap);
										?>
										@foreach($cevaplar as $cevap)
											<div class="cevap default">
												{{ $cevap }}
											</div>
										@endforeach
										@else
										<div class="cevap {{ $answer->dogru ? 'dogru' : 'yanlis' }}">
											{{ $answer->cevap }}
										</div>
										@endif
									@endforeach
								</div>
							</div>
							<div class="box-footer">
								<div class="pull-left" style="margin-right:10px;">
									<i class="fa fa-clock-o"></i>
									{{ $question->time }} sn
								</div>
								@if($question->category_id > 0)
								<div class="pull-left" style="margin-right:10px;">
									<i class="fa fa-tags"></i>
									{{ $question->category->name }}
								</div>
								@endif
								<div class="pull-left" style="margin-right:10px;">
									<i class="fa fa-file"></i>
									{{ $question->type=="coktan" ? "Çoktan Seçmeli" : "Boşluk Doldurma" }}
								</div>
								<!--
								<div class="pull-left" style="margin-right:10px;">
									<i class="fa fa-globe"></i>
									Herkese Açık
								</div>
								-->
								<div class="clearfix"></div>
							</div>
							<div class="sinav-q-checked">
								<i class="fa fa-check"></i>
							</div>
						</div>
					</div>
					@endforeach
					@else
					<div class="alert alert-warning alert-dismissable">
					    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					    <h4><i class="icon fa fa-warning"></i> Uyarı!</h4>
					    <a href="{{ url('panel/sorular/yeni') }}">Sorunuz bulunmamaktadır. Soru oluşturmak için burayı tıklayın.</a>
					  </div>
					@endif

				</div>

				<div class="row" style="margin-left:0; margin-right:0;">

					<div class="col-sm-12">
						<a href="{{ $backUrl }}" class="btn btn-info pull-left" style="margin-top:20px;">Geri</a>

			 			<input type="submit" class="btn btn-info pull-right" style="margin-top:20px;" value="Kaydet ve İlerle">
		 		 	</div>

					<div class="clearfix"></div>
				</div>

			</div>
			</form>

		</div><!-- box -->
	</div><!-- col -->
</div><!-- row -->
@endsection

@section('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script>
	$(function(){

		$('.box-body .soruIcerik').slimScroll({
		    height: '350px'
		});

		$('#sortable').css({"animation": "colorchange 50s infinite", "-webkit-animation": "colorchange 50s infinite"});

		var questions = [];

		$('input[type="checkbox"].sinavQC').each(function(){
		    var self = $(this),
		      label = self.next(),
		      label_text = label.text();

		    label.remove();
		    self.iCheck({
		      checkboxClass: 'icheckbox_line-grey',
		      insert: '<div class="icheck_line-icon"></div>' + label_text
		    });
	  	});

	  	$('input[type="checkbox"].sinavQC:checked').each(function(){
	  		questions.push($(this).val());
	  		$(this).closest('.col-md-4.col-sm-6').detach().appendTo('#sortable');
	  		$(this).closest('.box').find('.sinav-q-checked').css('height',
		  		($(this).closest('.box').find('.box-body').outerHeight() + $(this).closest('.box').find('.box-footer').outerHeight()) + 'px'
		  	).show();
	  	});

		$('input[type="checkbox"].sinavQC').on('ifChecked', function(event){
			questions.push($(this).val());
			$('#questions').val(questions);
			$(this).closest('.col-md-4.col-sm-6').detach().appendTo('#sortable');
		  	$(this).closest('.box').find('.sinav-q-checked').css('height',
		  		($(this).closest('.box').find('.box-body').outerHeight() + $(this).closest('.box').find('.box-footer').outerHeight()) + 'px'
		  	).show();
		});

		$('input[type="checkbox"].sinavQC').on('ifUnchecked', function(event){
			questions.splice(questions.indexOf($(this).val()),1);
			$('#questions').val(questions);
			$(this).closest('.col-md-4.col-sm-6').detach().appendTo('#notSortable');
		  	$(this).closest('.box').find('.sinav-q-checked').hide();
		});

		$("#sortable .box-header").css("cursor", "move");

        $("#sortable").sortable({
            connectWith: "#sortable",
		    handle: ".box-header",
		    forcePlaceholderSize: true,
		    'cursor': 'move',
		    zIndex: 999999,
            update: function( ) {

            	questions = [];

                $( '#sortable input[type="checkbox"].sinavQC' ).each(function( index, element ) {
                    questions.push($(element).val());
                });

				$('#questions').val(questions);
            }
        });

        $('form').submit(function(){
        	if($('#questions').val()==""){
        		Swal.fire('Uyarı', 'Sınav için soru seçmelisin', 'error');
        		return false;
        	}
        });

	});
</script>
@endsection

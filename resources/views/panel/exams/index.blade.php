@extends('panel.layouts.template')

@section('title', 'Sınavlar')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Sınavlarım
    <small>Sınavlarım sayfası</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('panel') }}"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li><i class="fa fa-trophy"></i> Sınav İşlemleri</li>
    <li class="active"><i class="fa fa-bank"></i> Sınavlarım</li>
  </ol>
</section>
@endsection

@section('content')
<div class="row">
	@if(count($exams) > 0)

	<?php
		$box = array("box-primary", "box-warning", "box-success");
		setlocale(LC_TIME, 'tr_TR@TL', 'tr_TR', 'tr', 'Turkish');
	?>
	@foreach($exams as $key=>$exam)
	<div class="col-md-4 col-sm-6">
		<div class="box box-solid {{ $box[$key % 3] }}">
			<div class="box-header with-border">
				<h3 class="box-title">{{ $exam->name }}</h3>
				<div class="box-tools pull-right">
					<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Küçült/Büyült"><i class="fa fa-minus"></i></a>
					<a class="btn btn-box-tool" data-toggle="tooltip" title="Sınavı Çoğalt"><i class="fa fa-clone"></i></a>
					<a @if($exam->exam_reports->count() > 0) id="duzenlemeSilmeUyari" @endif href="{{ url('panel/sinavlar/duzenle/'.$exam->id) }}" class="btn btn-box-tool" data-toggle="tooltip" title="Düzenle"><i class="fa fa-edit"></i></a>
					<a @if($exam->exam_reports->count() > 0) id="duzenlemeSilmeUyari" @else id="sinavSil" @endif href="#" data-id="{{ $exam->id }}" class="btn btn-box-tool" data-toggle="tooltip" title="Sınavı Sil"><i class="fa fa-times"></i></a>
				</div>
			</div>
			<div class="box-body">
				@if($exam->time_manual=="0" || !is_null($exam->start_time))
					<p>Başlangıç Tarihi: {{ \Carbon\Carbon::parse($exam->start_time)->formatLocalized('%d %B %Y %H:%M') }}</p>
			        <p>Bitiş tarihi: {{ is_null($exam->end_time) ? 'Belli Değil' : \Carbon\Carbon::parse($exam->end_time)->formatLocalized('%d %B %Y %H:%M') }}</p>
			        <p>Giriş İzni: {{ ($exam->public==0) ? 'Kod ile' : 'Herkese Açık' }}</p>
			        <p>
			        <p>Çözen Kişi Sayısı: {{ $exam-> exam_reports()->whereNotNull('end_time')->count() }}</p>
		        @endif
				<p>
	            	Sınav Kodu:
	            	<span class="label label-success" style="font-size:20px;">{{ $exam->code }}</span>
	          	</p>
				<p>Soru Sayısı: {{ $exam->exam_questions()->count() }}</p>
				@if($exam->time_manual==1)
					<p>Sınav Türü: {{ $exam->type=="senkron" ? "Eş zamanlı(senkron)" : "Eş zamansız(asenkron)" }}</p>
				@endif
			</div>
			<div class="box-footer">
				@if($exam->time_manual==1 && is_null($exam->start_time))
				<a id="sinavBaslat" @if($exam->type=="senkron") data-senkron="true" @endif data-id="{{ $exam->id }}" data-code="{{ $exam->code }}" class="btn btn-warning pull-right"><i class="fa fa-hourglass-start" style="margin-right:10px;"></i>Başlat</a>
				@endif
				@if($exam->time_manual==1 && $exam->type=="senkron" && !is_null($exam->start_time) && is_null($exam->end_time))
					<a id="sinavYenidenBaslat" data-id="{{ $exam->id }}" data-code="{{ $exam->code }}" class="btn btn-primary"><i class="fa fa-refresh" style="margin-right:10px;"></i>Yeniden Başlat</a>
				@endif
				@if(($exam->time_manual==1 && !is_null($exam->start_time) && is_null($exam->end_time) && $exam->type<>"senkron") || $exam->end_time > \Carbon\Carbon::now())
				<a id="sinavBitir" data-id="{{ $exam->id }}" class="btn btn-danger"><i class="fa fa-flag-checkered" style="margin-right:10px;"></i>Sınavı Bitir</a>
				@endif

				@if(!is_null($exam->start_time) && !is_null($exam->end_time) && $exam->end_time <= \Carbon\Carbon::now())
				<a href="{{ url('panel/sinavrapor/'.$exam->id) }}" class="btn btn-info"><i class="fa fa-bar-chart" style="margin-right:10px;"></i>Sınav Raporu</a>
				@endif
			</div>
		</div>
	</div>
	@endforeach
	@else
	<div class="alert alert-warning alert-dismissable">
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	    <h4><i class="icon fa fa-warning"></i> Uyarı!</h4>
	    <a href="{{ url('panel/sinavlar/yeni') }}">Sınavınız bulunmamaktadır. Sınav oluşturmak için burayı tıklayın.</a>
	  </div>
	@endif

</div>
<div class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Canlı Sınav Kuralları</h4>
      </div>
      <div class="modal-body">
        <p>
          <ul>
            <li>Sınavının raporunu görmek ve sınavın tamamlanmış olması için kullanıcılara tüm soruları cevaplandırmalısın</li>
            <li><b>Sınav anında internet bağlantın koparsa veya sınav penceresini kapatırsan sınav yarım kalmış sayılacak ve sınava giren herkes odadan atılacaktır</b></li>
            <li><b>Bu yüzden sınav anında sayfa yenileme, sayfayı kapatma gibi işlemler yapılmamalıdır</b></li>
            <li style="color: red;">Sınav esnasında sayfa yenileme işlemi yaparsan sınav verileri silinip sınav yeniden başlatılır. İnternet bağlantının kopması veya pencereyi kapatman durumunda bu sayfaya gelip Yeniden Başlat butonuna basıp sınavı yeniden başlatabilirsin.</li>
            <li><b>Sınav esnasında odadan çıkan kişinin sınavı tamamlanmış sayılmayacak ve sınav raporuna dahil olmayacaktır</b></li>
            <li>Sınav başlatıldıktan sonra sınav odasına girişler olmayacaktır</li>
            <li>Sınav raporunu sınav bittikten sonra Sınavlarım sayfasından sınav raporu butonu aracılığıyla görebilirsin.</li>
            <li>Sınav sonucu 100'lük sisteme göre puanlandırılacaktır</li>
          </ul>
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Anladım ve Sınava Başlatmak İstiyorum</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@section('scripts')
<script>
	$(function(){

		$(document).on('click', '#duzenlemeSilmeUyari', function(e){
			e.preventDefault();
			Swal.fire('Uyarı', 'Sınavı çözen kişi olduğu için düzenleme/silme işlemi yapılamaz.', 'warning');
		});

		$(document).on('click', '#sinavSil', function(){
			var th = $(this);
			Swal.fire({
				title: "Silmek istediğine emin misin?",
				text: "Sildiğin sınav geri getirilemez!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Evet, eminim",
				cancelButtonText: "Hayır, silmek istemiyorum",
				closeOnConfirm: false,
				closeOnCancel: true,
				showLoaderOnConfirm: true
				},
				function(isConfirm){
					if (isConfirm) {
						$.ajax({
				          url: "{{ url('panel/sinavlar/sil') }}" + '/' + $(th).data('id'),
				          type: 'POST'
				        })
				        .done(function() {
				          window.location.href= "{{ url('panel/sinavlar') }}";
				        })
				        .fail(function(jqXHR, textStatus) {
				          Swal.fire('Beklenmedik bir hata oluştu!' + textStatus);
				        });
					}
					else{
						swal.close();
					}
				});
		});

		var code, id;
		$(document).on('click', '#sinavBaslat', function(e){
			code = $(this).data('code');
			id = $(this).data('id');
			e.preventDefault();
			if($(this).data('senkron')==true)
            	$('.modal').modal();
            else
            	sinavBaslat();
	    });

	    $(document).on('click', ".modal .modal-footer button", function(){
	    	$(this).attr("disabled", "disabled");
	    	sinavBaslat();
	    });

	    function sinavBaslat(){
	    	$.ajax({
	          url: "{{ url('panel/sinavlar/baslat') }}" + '/' + id,
	          type: 'POST'
	        })
	        .done(function(data) {
	        	if(data.gidilcek_yer=="socket"){
	          		window.location.href = "{{ url('senkron') }}" + "/" + code;
	        	}
	          	else
	          		window.location.href= "{{ url('panel/sinavlar') }}";
	        })
	        .fail(function(jqXHR, textStatus) {
	          Swal.fire('Beklenmedik bir hata oluştu!' + textStatus);
	          $(this).removeAttr("disabled");
	        });
	    }

	    $(document).on('click', '#sinavYenidenBaslat', function(e){
	        window.location.href = "{{ url('senkron') }}" + "/" + $(this).data('code');
	    });

		$(document).on('click', '#sinavBitir', function(e){
			e.preventDefault();
			$.ajax({
	          url: "{{ url('panel/sinavlar/bitir') }}" + '/' + $(this).data('id'),
	          type: 'POST'
	        })
	        .done(function() {
	          window.location.href= "{{ url('panel/sinavlar') }}";
	        })
	        .fail(function(jqXHR, textStatus) {
	          Swal.fire('Beklenmedik bir hata oluştu!' + textStatus);
	        });
	    });
	});
</script>
@endsection

@extends('panel.layouts.template')

@section('title', 'Sınav Merkezi')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Anasayfa
    <small>Panel anasayfanız</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('panel') }}"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li class="active">Anasayfa</li>
  </ol>
</section>
@endsection

@section('content')
<!--
<div class="row">
    <div class="col-lg-4 col-xs-6">
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3>10</h3>
                <p>
                    Sınav
                </p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">
                Detaylı Bilgi <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-4 col-xs-6">
        <div class="small-box bg-green">
            <div class="inner">
                <h3>
                    15                </h3>
                <p>
                    Çözüm
                </p>
            </div>
            <div class="icon">
                <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">
                Detaylı Bilgi <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-4 col-xs-6">
        <div class="small-box bg-red">
            <div class="inner">
                <h3>
                    137                </h3>
                <p>
                    Soru
                </p>
            </div>
            <div class="icon">
                <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">
                Detaylı Bilgi <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
</div>
-->
@endsection
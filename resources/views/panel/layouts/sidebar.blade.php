<aside class="main-sidebar">

  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
      <li class="{{ Request::is('panel') ? 'active' : '' }}"><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> <span>Anasayfa</span></a></li>
      <li class="treeview {{ Request::is('panel/sorular*') ? 'active' : '' }}">
        <a href="#">
          <i class="fa fa-question"></i> <span>Soru İşlemleri</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="{{ Request::is('panel/sorular') ? 'active' : '' }}">
            <a href="{{ url('panel/sorular') }}">
              <i class="fa fa-bank"></i>
              Soru Bankam
            </a>
          </li>
          <li class="{{ Request::is('panel/sorular/yeni') ? 'active' : '' }}">
            <a href="{{ url('panel/sorular/yeni') }}">
              <i class="fa fa-plus"></i>
              Soru Ekle
            </a>
          </li>
        </ul>
      </li>
      <li class="treeview {{ Request::is('panel/kategoriler*') ? 'active' : '' }}">
        <a href="#">
          <i class="fa fa-list"></i> <span>Kategori İşlemleri</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="{{ Request::is('panel/kategoriler') ? 'active' : '' }}">
            <a href="{{ url('panel/kategoriler') }}">
              <i class="fa fa-list-alt"></i>
              Kategoriler
            </a>
          </li>
          <li class="{{ Request::is('panel/kategoriler/yeni') ? 'active' : '' }}">
            <a href="{{ url('panel/kategoriler/yeni') }}">
              <i class="fa fa-plus"></i>
              Kategori Ekle
            </a>
          </li>
        </ul>
      </li>
      <li class="treeview {{ Request::is('panel/sinavlar*') ? 'active' : '' }}">
        <a href="#">
          <i class="fa fa-trophy"></i> <span>Sınav İşlemleri</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="{{ Request::is('panel/sinavlar') ? 'active' : '' }}">
            <a href="{{ url('panel/sinavlar') }}">
              <i class="fa fa-bank"></i>
              Sınavlarım
            </a>
          </li>
          <li class="{{ Request::is('panel/sinavlar/yeni') ? 'active' : '' }}">
            <a href="{{ url('panel/sinavlar/yeni') }}">
              <i class="fa fa-plus"></i>
              Yeni Sınav
            </a>
          </li>
        </ul>
      </li>
      <li class="header">İstatistikler</li>
      <li class="{{ Request::is('panel/sonuclarim') ? 'active' : '' }}">
        <a href="{{ url('panel/sonuclarim') }}"><i class="fa fa-link"></i> <span>Sınav Sonuçlarım</span></a>
      </li>

    </ul><!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>
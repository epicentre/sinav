@extends('panel.layouts.template')

@section('title', 'Sınav Sonuçlarım')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Sınav Sonuçlarım
    <small>Sınav Sonuçlarınız</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('panel') }}"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li class="active">Sınav Sonuçlarım</li>
  </ol>
</section>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>No</th>
                  <th>Sınav Adı</th>
                  <th>Erişim</th>
                  <th>Durum</th>
                  <th>İşlem</th>
                </tr>
                @foreach($exam_reports as $key=>$exam_report)
                    <tr>
                        <td>{{ ($key + 1) }}</td>
                        <td>{{ $exam_report->exam->name }}</td>
                        <td>{{ $exam_report->exam->public == 1 ? 'Herkese Açık' : 'Kod ile:'.$exam_report->exam->code }}</td>
                        <td>
                            @if(is_null($exam_report->end_time))
                                <span class="label label-danger">Sınavınız yarım kalmış</span>
                            @elseif(!is_null($exam_report->exam->end_time) && \Carbon\Carbon::now() > $exam_report->exam->end_time)
                                <span class="label label-success">Sonuçlarınız hazır</span>
                            @elseif(($exam_report->exam->time_manual == 1 && is_null($exam_report->exam->end_time)) || $exam_report->end_time < $exam_report->exam->end_time)
                                <span class="label label-info">Sınav süresinin bitmesi bekleniyor</span>
                            @endif                            
                        </td>
                        <td>
                            @if(!is_null($exam_report->end_time) && \Carbon\Carbon::now() > $exam_report->exam->end_time)
                                <a href="{{ url('panel/sonuclarim/'.$exam_report->id) }}" class="btn btn-success">
                                    <span class="fa fa-arrow-right"></span>
                                    Gör
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
              </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- ./col -->
</div>
@endsection
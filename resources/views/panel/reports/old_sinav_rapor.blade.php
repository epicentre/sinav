@extends('panel.layouts.template')

@section('title', 'Sınav Raporu')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Sınav Raporu
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('panel') }}"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li class="active">Sınav Raporu</li>
  </ol>
</section>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title">Sıralama ve Puanım <small>Bu bölümden sınava giren herkes arasında sıralamanı ve puanını görebilirsin.</small></h3>
          <!-- tools box -->
          <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Küçült/Büyüt"><i class="fa fa-minus"></i></button>
          </div><!-- /. tools -->
        </div><!-- /.box-header -->
        <div class="box-body table-responsive">
          <table class="table table-hover">
            <tr>
              <th>Sıra</th>
              <th>Ad Soyad</th>
              <th>Doğru Sayısı</th>
              <th>Yanlış Sayısı</th>
              <th>Boş Sayısı</th>
              <th>Puan</th>
            </tr>
            @foreach($siralama_verisi as $key=>$veri)
                <tr style="{{ $veri['user_id']==auth()->user()->id ? 'background:#7FDBFF;' : '' }}">
                    <td>{{ ($key + 1) }}</td>
                    <td>{{ $veri['name'] }}</td>
                    <td>{{ $veri['dogru'] }}</td>
                    <td>{{ $veri['yanlis'] }}</td>
                    <td>{{ $veri['bos'] }}</td>
                    <td>{{ $veri['puan'] }}</td>
                </tr>
            @endforeach
          </table>
        </div><!-- /.box-body -->
      </div>
      <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title">Sınav soruları <small>Bu bölümden soruları ve verilen cevapları görebilirsin.</small></h3>
          <!-- tools box -->
          <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Küçült/Büyüt"><i class="fa fa-minus"></i></button>
          </div><!-- /. tools -->
        </div><!-- /.box-header -->
        <div class="box-body pad">
          
            <?php $box = array("box-primary", "box-warning", "box-success") ?>
            @foreach($questions as $key=>$question)
            <div class="row">
              <div class="col-md-6">
                <div class="box box-solid {{ $box[$key % 3] }}">
                  <div class="box-header with-border">
                    <h3 class="box-title">&nbsp;</h3>
                    <div class="box-tools pull-right">
                      <?php if($question->description<>""){ ?>
                        <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="İpucu"><i class="fa fa-question"></i></a>
                      <?php } ?>
                      <a href="#" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Küçült/Büyült"><i class="fa fa-minus"></i></a>
                    </div>
                  </div>
                  <div class="box-body">
                    <div class="soruIcerik">
                      <div class="soru">
                        {!! $question->content !!}
                      </div>
                      @foreach($question->answers as $answer)
                        @if($question->type == 'bosluk')
                          <?php
                            $cevaplar = explode(',', $answer->cevap);
                          ?>
                          @foreach($cevaplar as $cevap)
                            <div class="cevap default">
                              {{ $cevap }}
                            </div>
                          @endforeach
                        @else
                          <div class="cevap {{ $answer->dogru ? 'dogru' : 'yanlis' }}">
                            {{ $answer->cevap }}
                          </div>
                        @endif
                      @endforeach
                    </div>
                  </div>
                  <div class="box-footer">
                    @if($question->category_id > 0)
                    <div class="pull-left" style="margin-right:10px;">
                      <i class="fa fa-tags"></i>
                      {{ $question->category->name }}
                    </div>
                    @endif
                    <!--
                    <div class="pull-left" style="margin-right:10px;">
                      <i class="fa fa-globe"></i>
                      Herkese Açık
                    </div>
                    -->
                    <div class="clearfix"></div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <canvas id="grafik{{ $question->id }}" width="400" height="455"></canvas>  
              </div>
            </div>
            @endforeach
        </div>
      </div><!-- /.box -->
  </div><!-- col -->
</div>
@endsection

@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
  <script>
    $(function(){
      $('.box-body .soruIcerik').slimScroll({
          height: '350px'
      });
      
      @foreach($cevap_veri as $k=>$veri)
        var label_text = [];
        var cevap_data = [];
        <?php $i=0; ?>
        @foreach($veri as $key=>$value)
          <?php $i++; ?>
          if("{{ $key }}" == "0"){
              label_text.push("Boş");

              cevap_data.push({{ $value }});
          }
          else{
            if(!isNaN("{{ $key }}"))
              label_text.push("{{ $i }}" + ". Seçenek");
            else
              label_text.push("{{ $key }}");

            cevap_data.push({{ $value }});
          }

        @endforeach
        var graphic_data = {
          labels: label_text,
          datasets: [
              {
                  label: "My first dataset",
                  fillColor: "rgba(151,187,205,0.5)",
                  strokeColor: "rgba(151,187,205,0.8)",
                  highlightFill: "rgba(151,187,205,0.75)",
                  highlightStroke: "rgba(151,187,205,1)",
                  data: cevap_data
              }
          ]};

        $("#grafik{{ $k }}").css({"width": "100%", "height" : "455px"});
        
        var ctx{{ $k }} = $("#grafik{{ $k }}").get(0).getContext("2d");
        myBarChart = new Chart(ctx{{ $k }}).Bar(graphic_data, {});
      @endforeach
    });
  </script>
@endsection
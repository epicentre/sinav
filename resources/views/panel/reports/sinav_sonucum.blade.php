@extends('panel.layouts.template')

@section('title', 'Sınav Sonuçlarım')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Sınav Sonucum
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('panel') }}"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li class="active">Sınav Sonucum</li>
  </ol>
</section>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
      <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title">Sıralama ve Puanım <small>Bu bölümden sınava giren herkes arasında sıralamanı ve puanını görebilirsin.</small></h3>
          <!-- tools box -->
          <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Küçült/Büyüt"><i class="fa fa-minus"></i></button>
          </div><!-- /. tools -->
        </div><!-- /.box-header -->
        <div class="box-body table-responsive">
          <table class="table table-hover">
            <tr>
              <th>Sıra</th>
              <th>Ad Soyad</th>
              <th>Doğru Sayısı</th>
              <th>Yanlış Sayısı</th>
              <th>Boş Sayısı</th>
              <th>Puan</th>
            </tr>
            @foreach($siralama_verisi as $key=>$veri)
                <tr style="{{ $veri['user_id']==auth()->user()->id ? 'background:#7FDBFF;' : '' }}">
                    <td>{{ ($key + 1) }}</td>
                    <td>{{ $veri['name'] }}</td>
                    <td>{{ $veri['dogru'] }}</td>
                    <td>{{ $veri['yanlis'] }}</td>
                    <td>{{ $veri['bos'] }}</td>
                    <td>{{ $veri['puan'] }}</td>
                </tr>
            @endforeach
          </table>
        </div><!-- /.box-body -->
      </div>
      <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title">Çözdüğüm sorular <small>Bu bölümden verdiğiniz cevapları görebilirsiniz.</small></h3>
          <!-- tools box -->
          <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Küçült/Büyüt"><i class="fa fa-minus"></i></button>
          </div><!-- /. tools -->
        </div><!-- /.box-header -->
        <div class="box-body pad">
          <div class="row">
            <?php $box = array("box-primary", "box-warning", "box-success") ?>
            @foreach($exam_report->exam->exam_questions as $key=>$exam_question)
              <?php
                $question = $exam_question->question;
                $question_report = $user_question_reports->where('question_id', $question->id)->first();

              ?>
              <div class="col-md-4 col-sm-6">
                <div class="box box-solid {{ $box[$key % 3] }}">
                  <div class="box-header with-border">
                    <h3 class="box-title">&nbsp;</h3>
                    <div class="box-tools pull-right">
                      <?php if($question->description<>""){ ?>
                        <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="İpucu"><i class="fa fa-question"></i></a>
                      <?php } ?>
                      <a href="#" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Küçült/Büyült"><i class="fa fa-minus"></i></a>
                    </div>
                  </div>
                  <div class="box-body">
                    <div class="soruIcerik">
                      <div class="soru">
                        {!! $question->content !!}
                      </div>
                      @foreach($question->answers as $answer)
                        @if($question->type == 'bosluk')
                          <?php
                            $cevaplar = explode(',', $answer->cevap);
                          ?>
                          @foreach($cevaplar as $cevap)
                            <div class="cevap default">
                              {{ $cevap }}
                            </div>
                          @endforeach
                          <div class="animated infinite pulse cevap sari">
                            {{ $question_report->answer_string }}
                          </div>
                          @else
                            @if($question_report->answer_id == $answer->id)
                              <div class="animated infinite pulse cevap {{ $answer->dogru ? 'dogru' : 'yanlis' }}">
                                {{ $answer->cevap }}
                              </div>
                            @else
                              <div class="cevap {{ $answer->dogru ? 'dogru' : 'yanlis' }}">
                                {{ $answer->cevap }}
                              </div>
                            @endif
                        @endif
                      @endforeach
                    </div>
                  </div>
                  <div class="box-footer">
                    <div class="pull-left" style="margin-right:10px;">
                      <i class="fa fa-clock-o"></i>
                      <?php
                        $dt1 = new \Carbon\Carbon($question_report->end_time);
                        $dt2 = new \Carbon\Carbon($question_report->start_time);

                        $cozulen_saniye = $dt2->diffInSeconds($dt1);
                      ?>
                      {{ $cozulen_saniye.'/'.$question->time }} sn
                    </div>
                    @if($question->category_id > 0)
                    <div class="pull-left" style="margin-right:10px;">
                      <i class="fa fa-tags"></i>
                      {{ $question->category->name }}
                    </div>
                    @endif
                    <!--
                    <div class="pull-left" style="margin-right:10px;">
                      <i class="fa fa-globe"></i>
                      Herkese Açık
                    </div>
                    -->
                    <div class="clearfix"></div>
                  </div>
                </div>
              </div>
            @endforeach

          </div>
        </div>
      </div><!-- /.box -->
    </div><!-- ./col -->
</div>
@endsection

@section('scripts')
  <script>
    $(function(){
      $('.box-body .soruIcerik').slimScroll({
          height: '350px'
      });
    });
  </script>
@endsection
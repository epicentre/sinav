@extends('panel.layouts.template')

@section('title', 'Kategori Ekleme')

@section('breadcrumbs')
  @if(request()->segment(3)=="yeni")
    <section class="content-header">
      <h1>
        Kategori Ekle
        <small>Kategori ekleme sayfası</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('panel') }}"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
        <li><i class="fa fa-list"></i> Kategori İşlemleri</li>
        <li class="active"><i class="fa fa-plus"></i> Kategori Ekle</li>
      </ol>
    </section>
  @elseif(request()->segment(3)=="duzenle")
    <section class="content-header">
      <h1>
        Kategori Düzenle
        <small>Kategori düzenleme sayfası</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('panel') }}"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
        <li><i class="fa fa-list"></i> Kategori İşlemleri</li>
        <li class="active"><i class="fa fa-edit"></i> Kategori Düzenle</li>
      </ol>
    </section>
  @endif
@endsection

@section('content')
<div class="box box-info">
	<div class="box-header with-border">
	  <h3 class="box-title">Kategori Formu</h3>
	</div><!-- /.box-header -->
	<!-- form start -->
	<form class="form-horizontal" action="" method="post" role="form">
		{{ csrf_field() }}
	  <div class="box-body">
	    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
	      <label for="name" class="col-sm-2 control-label">Kategori Adı</label>
	      <div class="col-sm-10">
	        <input type="text" class="form-control" name="name" id="name" placeholder="kategori adı" value="{{ old('name', (isset($category) ? $category->name : '')) }}">
	        @if ($errors->has('name'))
              <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
              </span>
            @endif
	      </div>
	    </div>
	    <div class="form-group">
	      <label for="description" class="col-sm-2 control-label">Açıklama</label>
	      <div class="col-sm-10">
	        <textarea class="form-control" name="description" id="description" placeholder="açıklama">{{ old('description', (isset($category) ? $category->description : '')) }}</textarea>
	      </div>
	    </div>
	  </div><!-- /.box-body -->
	  <div class="box-footer">
	    <button type="submit" class="btn btn-default">Temizle</button>
	    <button type="submit" class="btn btn-info pull-right">Kaydet</button>
	  </div><!-- /.box-footer -->
	</form>
</div>
@endsection
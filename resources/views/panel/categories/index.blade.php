@extends('panel.layouts.template')

@section('title', 'Kategoriler')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Kategoriler
    <small>Kategorileriniz</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('panel') }}"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li><i class="fa fa-list"></i> Kategori İşlemleri</li>
    <li class="active"><i class="fa fa-list-alt"></i> Kategoriler</li>
  </ol>
</section>
@endsection

@section('content')
<div class="row">
	<div class="col-xs-12">
	  <div class="box">
	  	<div class="box-header">
	      <h3 class="box-title">&nbsp;</h3>
	      <div class="box-tools">
	        <div class="input-group" style="width: 150px;">
	          <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Ara">
	          <div class="input-group-btn">
	            <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
	          </div>
	        </div>
	      </div>
	    </div>
	    <div class="box-body table-responsive no-padding">
	      @if(count($categories) > 0)
	      <table class="table table-hover table-bordered">
	        <thead>
	          <tr>
	            <th>Kategori Adı</th>
	            <th>Açıklama</th>
	            <!--<th>Soru Sayım</th>-->
	            <th>İşlem</th>
	          </tr>
	        </thead>
	        <tbody>
	        	@foreach($categories as $category)
					<tr>
						<td>{{ $category->name }}</td>
						<td>{{ $category->description }}</td>
						<td>
						    <a class="btn btn-warning btn-flat" href="{{ url('panel/kategoriler/duzenle', $category->id) }}">Düzenle</a>
	              			<a href="#" class="btn btn-danger btn-flat" id="kategoriSil" role="button" data-id="{{ $category->id }}">Sil</a>
						</td>
					</tr>
	        	@endforeach
	    		<!--<tr>
	    			<td>Matematik</td>
	    			<td>Matematik konusuna ait sorular</td>
	    			<td>15</td>
	    			<td>
	                  	<a class="btn btn-warning btn-flat" href="#">Düzenle</a>
	                  	<button class="btn btn-danger btn-flat">Sil</button>
	        		</td>
	    		</tr>-->
	        </tbody>
	      </table>
	      @else
	      <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-warning"></i> Uyarı!</h4>
            <a href="{{ url('panel/kategoriler/yeni') }}">Kategoriniz bulunmamaktadır. Kategori oluşturmak için burayı tıklayın.</a>
          </div>
	      @endif
	    </div><!-- /.box-body -->
	  </div><!-- /.box -->
	</div><!-- /.col -->
</div><!-- /.row -->
@endsection

@section('scripts')
<script>
	$(function(){

		$(document).on('click', '#kategoriSil', function(e){
			e.preventDefault();
			var th = $(this);
			Swal.fire({
				title: "Kategoriyi silmek istediğine emin misin?",
				text: "Eğer sildiğin kategoriye ait soru varsa kategori silinmeyecek, pasife alınacaktır",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Evet, eminim",
				cancelButtonText: "Hayır, silmek istemiyorum",
				closeOnConfirm: false,
				closeOnCancel: false },
				function(isConfirm){
					if (isConfirm) {
						$.ajax({
				          url: "{{ url('panel/kategoriler/sil') }}" + '/' + $(th).data('id'),
				          type: 'POST'
				        })
				        .done(function() {
				        	window.location.href= "{{ url('panel/kategoriler') }}";
				        })
				        .fail(function(jqXHR, textStatus) {
				        	Swal.fire('Beklenmedik bir hata oluştu!');
				        });
					}
					else{
						swal.close();
					}
				});
		});

	});
</script>
@endsection

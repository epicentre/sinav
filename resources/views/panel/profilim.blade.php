@extends('panel.layouts.template')

@section('title', 'Sınav Merkezi')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Profilim
    <small>Profil bilgileriniz</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('panel') }}"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li class="active">Profilim</li>
  </ol>
</section>
@endsection

@section('content')
<div class="row">
    <div class="col-md-3">
    <!-- Profile Image -->
        <div class="box box-primary" id="resimBox">
            <div class="box-body box-profile">
                <img id="resim" class="profile-user-img img-responsive img-circle" src="{{ asset($user->resim) }}" alt="Profil Resmi">
                <form role="form" method="post" id="resimForm" action="" enctype="multipart/form-data">
                    <div class="form-group">
                      <input type="file" name="picture" id="picture">

                      <p class="help-block">Profil resminizi seçin</p>
                    </div>
                    <a href="#" id="resimYukle" class="btn btn-block btn-primary" style="margin-bottom:10px;">Güncelle</a>
                </form>
                <h3 class="profile-username text-center">{{ $user->name }}</h3>

                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <b>Soru Sayım</b> <a class="pull-right">{{ $user->questions->count() }}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Sınav Sayım</b> <a class="pull-right">{{ $user->exams->count() }}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Girdiğim Sınav</b> <a class="pull-right">{{ $user->exam_reports->count() }}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Çözdüğüm Soru</b> <a class="pull-right">{{ $user->question_reports->count() }}</a>
                    </li>
                </ul>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->

    <div class="col-md-9">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#bilgi" data-toggle="tab">Temel Bilgiler</a>
                </li>
                <li>
                    <a href="#sifre" data-toggle="tab">Şifre</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="active tab-pane" id="bilgi">
                    <form class="form-horizontal" method="post" action="{{ url('panel/profilbilgiguncelle') }}">
                        {{ csrf_field() }}
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label for="name" class="col-sm-2 control-label">Ad Soyad</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" id="name" placeholder="ad soyad" value="{{ is_null(old('name')) ? $user->name : old('name') }}">
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cinsiyet" class="col-sm-2 control-label">Cinsiyet</label>
                            <div class="col-sm-10">
                                <select name="cinsiyet" id="cinsiyet" class="form-control">
                                    <?php
                                        $cinsiyet = is_null(old('cinsiyet')) ? $user->cinsiyet : old('cinsiyet');
                                    ?>
                                    <option value="" @if($cinsiyet=="") selected @endif></option>
                                    <option value="Erkek" @if($cinsiyet=="Erkek") selected @endif>Erkek</option>
                                    <option value="Kadın" @if($cinsiyet=="Kadın") selected @endif>Kadın</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="il" class="col-sm-2 control-label">İl</label>
                            <div class="col-sm-10">
                                <select name="il" id="il" class="form-control">
                                    <?php
                                        $il = is_null(old('il')) ? $user->city_id : old('il');
                                    ?>
                                    <option value="0" @if($il==0) selected @endif></option>
                                    @foreach($cities as $city)
                                        <option value="{{ $city->id }}" @if($city->id==$il) selected @endif>{{ $city->city }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ilce" class="col-sm-2 control-label">İlçe</label>
                            <div class="col-sm-10">
                                <select name="ilce" id="ilce" class="form-control">
                                    <?php
                                        $ilce = is_null(old('ilce')) ? $user->county_id : old('ilce');
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tel" class="col-sm-2 control-label">Telefon</label>
                            <div class="col-sm-10">
                                <input type="text" name="tel" id="tel" class="form-control" value="{{ is_null(old('tel')) ? $user->tel : old('tel') }}" data-inputmask='"mask": "(999) 999-9999"' data-mask>
                            </div>
                        </div><!-- /.form group -->
                        <div class="form-group">
                            <label for="adres" class="col-sm-2 control-label">Adres</label>
                            <div class="col-sm-10">
                                <textarea name="adres" id="adres" class="form-control">{{ is_null(old('adres')) ? $user->adres : old('adres') }}</textarea>
                            </div>
                        </div><!-- /.form group -->
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-danger pull-right">Kaydet</button>
                            </div>
                        </div>
                    </form>
                </div><!-- /.tab-pane -->
                <div class="tab-pane" id="sifre">
                    <form class="form-horizontal" method="post" action="{{ url('panel/profilsifreguncelle') }}">
                        {{ csrf_field() }}
                        <div class="form-group {{ $errors->has('password_old') ? 'has-error' : '' }}">
                            <label for="password_old" class="col-sm-2 control-label">Şimdiki Şifreniz</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" name="password_old" id="password_old" value="{{ old('password_old') }}">
                                @if ($errors->has('password_old'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_old') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-sm-2 control-label">Yeni Şifre</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" name="password" id="password" value="{{ old('password') }}">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password" class="col-sm-2 control-label">Yeni Şifre Tekrarı</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" value="{{ old('password_confirmation') }}">
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-danger pull-right">Güncelle</button>
                            </div>
                        </div>
                        </div>
                    </form>
                </div><!-- /.tab-pane -->
            </div><!-- /.tab-content -->
        </div><!-- /.nav-tabs-custom -->
    </div><!-- /.col -->
</div><!-- /.row -->
@endsection

@section('scripts')
<script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script>
    $(function(){

        $("[data-mask]").inputmask();

        if("{{ session('sayfa') }}"=="sifre"){
            $('a[href="#sifre"]').tab('show');
        }

        function ilceGetir(){
            $.ajax({
                url: "{{ url('panel/ilcegetir') }}",
                type: 'POST',
                data:{"city_id" : $('#il').val() },
                dataType : 'json'
            })
            .done(function(data) {
                if($('#il').val() > 0){
                    //console.log(data);
                    var ilce = "{{ $ilce }}";
                    $('#ilce').html('');
                    var option = "";
                    if(data.data.length > 0){
                        $.each(data.data, function(key, value){
                            if(value.id == ilce)
                                option = '<option value="' + value.id + '" selected>' + value.county + '</option>';
                            else
                                option = '<option value="' + value.id + '">' + value.county + '</option>';

                            $('#ilce').append(option);
                        });
                    }
                }
                else
                    $('#ilce').html('<option value="0" selected></option>');
            })
            .fail(function(jqXHR, textStatus) {
                alert('Beklenmedik bir hata oluştu');
            });
        }

        ilceGetir();

        $('#il').change(function(){
            ilceGetir();
        });

        $("#resimYukle").click(function(e){
            e.preventDefault();
            $('#resimBox').append('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
            var formData = new FormData($("#resimForm")[0]);
            formData.append('picrture', $("#picture")[0].files[0]);
            $.ajax({
                  url:"{{ url('panel/profilresimyukle') }}",
                  data: formData,
                  dataType:'json',
                  type:'POST',
                  processData: false,
                  contentType: false,
                  success:function(data){
                    //console.log(data);
                    if(data.hata!="")
                        Swal.fire('Hata', data.hata, "error");

                    else {
                        Swal.fire('', data.mesaj, "success");
                        $('#resim').attr('src', data.resim);
                    }
                  },
                  error: function(){
                    alert('Beklenmedik bir hata oluştu');
                  },
                  complete: function() {
                      $("#resimForm")[0].reset();
                      $( "#resimBox .overlay" ).remove();
                  }
            });
        });
    });
</script>
@endsection

@extends('panel.layouts.template')

@section('title', 'Soru Ekleme')

@section('head')
<!-- Bootstrap Switch -->
<link rel="stylesheet" href="{{ asset('assets/css/bootstrap-switch.css') }}">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
<!-- Bootstrap Tag -->
<link rel="stylesheet" href="{{ asset('assets/css/bootstrap-tagsinput.css') }}">
<style>
    .ck-editor__editable_inline {
        min-height: 300px !important;
    }
</style>
@endsection

@section('breadcrumbs')
  @if(request()->segment(3)=="yeni")
    <section class="content-header">
      <h1>
        Soru Ekle
        <small>Soru ekleme sayfası</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('panel') }}"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
        <li><i class="fa fa-question"></i> Soru İşlemleri</li>
        <li class="active"><i class="fa fa-plus"></i> Soru Ekle</li>
      </ol>
    </section>
  @elseif(request()->segment(3)=="duzenle")
    <section class="content-header">
      <h1>
        Soru Düzenle
        <small>Soru düzenleme sayfası</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('panel') }}"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
        <li><i class="fa fa-question"></i> Soru İşlemleri</li>
        <li class="active"><i class="fa fa-edit"></i> Soru Düzenle</li>
      </ol>
    </section>
  @endif
@endsection

@section('content')
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row">
  <div class="col-md-12">
    <form class="form-horizontal" action="" method="POST" role="form">
    {{ csrf_field() }}
    <div class="box box-info">
      <div class="box-header">
        <h3 class="box-title">Soru İçeriği <small>Bu bölümden sorunuzun içeriğini girebilirsiniz.</small></h3>
        <!-- tools box -->
        <div class="pull-right box-tools">
          <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
        </div><!-- /. tools -->
      </div><!-- /.box-header -->
      <div class="box-body pad">
          <textarea id="content" name="content" rows="10" cols="80">{!! isset($question) ? $question->content : "" !!}</textarea>
      </div>
    </div><!-- /.box -->

    <div class="box box-info collapsed-box">
      <div class="box-header">
        <h3 class="box-title">Soru Açıklama (Opsiyonel)<small>Bu bölümde sorunuzla ilgili ipucu, açıklama gibi bilgiler girebilirsiniz</small></h3>
        <!-- tools box -->
        <div class="pull-right box-tools">
          <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
        </div><!-- /. tools -->
      </div><!-- /.box-header -->
      <div class="box-body pad">
          <textarea name="description" class="description" placeholder="Soru açıklaması, ipucu girin." style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{!! isset($question) ? $question->description : "" !!}</textarea>
      </div>
    </div>
    <div class="box box-info">
      <div class="box-header">
        <h3 class="box-title">Soru Bilgisi</h3>
        <!-- tools box -->
        <div class="pull-right box-tools">
          <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
        </div><!-- /. tools -->
      </div><!-- /.box-header -->
      <div class="box-body pad">
        <div class="form-group">
          <label for="soru_sure" class="col-sm-2 control-label">Süre</label>
          <div class="col-sm-10">
            <div class="input-group">
              <input type="number" min="0" max="32768" class="form-control" name="time" id="time" value="{!! isset($question) ? $question->time : '60' !!}">
              <span class="input-group-addon">saniye</span>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Kategori</label>
          <div class="col-sm-10">
            <select name="category" class="form-control">
              <option value="0"></option>
              @foreach($categories as $category)
                <option value="{{ $category->id }}" {!! (isset($question) && $category->id == $question->category_id) ? "selected" : "" !!}>{{ $category->name }}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="type" class="col-sm-2 control-label">Soru Tipi</label>
          <div class="col-sm-10">
            <select class="form-control" name="type" id="type">
              <option value="coktan" {!! (isset($question) && $question->type=="coktan") ? "selected" : "" !!}>Çoktan Seçmeli</option>
              <option value="bosluk" {!! (isset($question) && $question->type=="bosluk") ? "selected" : "" !!}>Boşluk Doldurma</option>
            </select>
          </div>
        </div>
      </div><!-- /.box-body -->

    </div>

      <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title">Cevaplar</h3>
          <!-- tools box -->
          <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
          </div><!-- /. tools -->
        </div><!-- /.box-header -->
        <div class="box-body pad">
          <div class="coktan-secmeli" id="coktanSecmeli">
            @if(isset($question))
              @foreach($question->answers as $key=>$answer)
                <div class="form-group">
                  <div class="col-sm-2 chk-dogru-container">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" name="chkDogru[]" id="chkDogru" value="cevap{{ ($key+1) }}" {{ $answer->dogru ? "checked" : "" }}>
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-10">
                    <div class="input-group">
                      <input type="text" class="form-control" name="cevap[]" value="{{ $answer->cevap }}" placeholder="cevabı girin">
                      <div class="input-group-btn">
                        <a href="#" class="btn btn-default" id="cevapEkle"><i class="fa fa-plus"></i></a>
                        <a href="#" class="btn btn-default" id="cevapSil"><i class="fa fa-times"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
              @endforeach
            @else
            <div class="form-group">
              <div class="col-sm-2 chk-dogru-container">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="chkDogru[]" id="chkDogru" value="cevap1">
                  </label>
                </div>
              </div>
              <div class="col-sm-10">
                <div class="input-group">
                  <input type="text" class="form-control" name="cevap[]" value="" placeholder="cevabı girin">
                  <div class="input-group-btn">
                    <a href="#" class="btn btn-default" id="cevapEkle"><i class="fa fa-plus"></i></a>
                    <a href="#" class="btn btn-default" id="cevapSil"><i class="fa fa-times"></i></a>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-2 chk-dogru-container">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="chkDogru[]" id="chkDogru" value="cevap2">
                  </label>
                </div>
              </div>
              <div class="col-sm-10">
                <div class="input-group">
                  <input type="text" class="form-control" name="cevap[]" value="" placeholder="cevabı girin">
                  <div class="input-group-btn">
                    <a href="#" class="btn btn-default" id="cevapEkle"><i class="fa fa-plus"></i></a>
                    <a href="#" class="btn btn-default" id="cevapSil"><i class="fa fa-times"></i></a>
                  </div>
                </div>

              </div>
            </div>
            @endif
          </div>

          <div id="boslukDoldurma">

            <div class="form-group">
              <label for="boslukdoldurma" class="col-sm-2 control-label">Cevaplar</label>
              <div class="col-sm-10">
                <input type="text" name="boslukdoldurma" data-role="tagsinput" class="form-control" value="{!! (isset($question) && $question->type=='bosluk') ? $question->answers->first()->cevap : "" !!}">
              </div>
            </div>

          </div>

        </div><!-- /.box-body -->

      </div>

      <div class="box box-info">
        <div class="box-body">
          <button type="submit" class="btn btn-default">Temizle</button>
          <button type="submit" class="btn btn-info pull-right">Kaydet</button>
        </div>
      </div>
    </form>
  </div><!-- /.col-md-12 -->
</div><!-- /.row -->
@endsection

@section('scripts')
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- Bootstrap Tag -->
<script src="{{ asset('assets/js/bootstrap-tagsinput.min.js') }}"></script>
<!-- Bootstrap Switch -->
<script src="{{ asset('assets/js/bootstrap-switch.js') }}"></script>
<script>
$(function(){
    let editor;
    ClassicEditor
        .create( document.querySelector( '#content' ), {
            toolbar: ['heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote', 'undo', 'redo']
        })
        .then( newEditor => {
            editor = newEditor;
        } )
        .catch( error => {
            console.error( error );
        } );

  //bootstrap WYSIHTML5 - text editor
  $(".description").wysihtml5();

  var switchOptions = {
    size:'small',
    onText : 'Doğru',
    offText : 'Yanlış',
    onColor : 'success',
    offColor : 'danger',
    labelWidth : 0
  };

  $("[name='chkDogru[]']").bootstrapSwitch(switchOptions);

  if($('#type').val()=="coktan")
  {
    $('#boslukDoldurma').hide();
    $('#coktanSecmeli').show();
  }
  else
  {
    $('#boslukDoldurma').show();
    $('#coktanSecmeli').hide();
  }

  $('#type').change(function(){
    if($(this).val()=="coktan")
    {
      $('#boslukDoldurma').hide(1000);
      $('#coktanSecmeli').show(1000);
    }
    else
    {
      $('#boslukDoldurma').show(1000);
      $('#coktanSecmeli').hide(1000);
    }
  });

  String.prototype.turkishLowerCase = function () {
    return this.replace(/Ğ/g, 'ğ')
        .replace(/Ü/g, 'ü')
        .replace(/Ş/g, 'ş')
        .replace(/I/g, 'ı')
        .replace(/İ/g, 'i')
        .replace(/Ö/g, 'ö')
        .replace(/Ç/g, 'ç')
        .toLowerCase();
  };//String.turkishLowerCase

  $(document).on('keyup', '.bootstrap-tagsinput input[type="text"]', function (e) {
    console.log(e.which);
      if (e.which >= 97 && e.which <= 122) {
          var newKey = e.which - 32;
          // I have tried setting those
          e.keyCode = newKey;
          e.charCode = newKey;
      }

      $(this).val(($(this).val()).turkishLowerCase());
  });

  var index, parent;
  $(document.body).on('click', '#cevapEkle', function(e){
    e.preventDefault();
    parent = $(this).parents(".form-group");
    index = $(parent).index() + 2;
    var cevap=$('<div class="form-group" style="display:none;"><div class="col-sm-2 chk-dogru-container"><div class="checkbox"><label><input type="checkbox" name="chkDogru[]" id="chkDogru" value="cevap'+ index +'"></label></div></div><div class="col-sm-10"><div class="input-group"><input type="text" class="form-control" name="cevap[]" value="" placeholder="cevabı girin"><div class="input-group-btn"><a href="#" class="btn btn-default" id="cevapEkle"><i class="fa fa-plus"></i></a><a href="#" class="btn btn-default" id="cevapSil"><i class="fa fa-times"></i></a></div></div></div></div>');

    $('#coktanSecmeli .form-group').each(function(i){
      if(i > index - 2)
      {
        $(this).find("#chkDogru").val("cevap" + (i + 2));
      }
    });

    $(parent).after(cevap);
    $(cevap).show(500);
    $("[name='chkDogru[]']").bootstrapSwitch(switchOptions);

  });

  $(document.body).on('click','#cevapSil', function(e){
    e.preventDefault();
    parent = $(this).parents(".form-group");
    index = $(parent).index() + 2;
    if($('.coktan-secmeli .form-group').length > 2)
      $(parent).hide(500, function(){ $(this).remove(); });
    else
      alert('En az iki seçenek olmalıdır.');

    $('#coktanSecmeli .form-group').each(function(i){
      if(i >= index - 2)
      {
        $(this).find("#chkDogru").val("cevap" + i);
      }
    });
  });

  $('form').submit(function(){
    var rtn = true;
    if(!editor || editor.getData()==""){
      alert('Soru içeriği doldurulmalıdır.');
      rtn = false;
    }

    if($('#time').val()==""){
      $('#time').parents('.form-group').addClass('has-error');
      $('#time').nextAll().remove();
      $('#time').after("<span class='help-block'>Bu alan doldurulmalıdır.</span>");
      rtn = false;
    }

    if($('#type').val()=='coktan'){
      $("[name='cevap[]']").each(function() {
        if($(this).val()==""){
          $(this).closest('.form-group').addClass('has-error');
          $(this).closest("[class^='col-']").find(".help-block").remove();
          $(this).closest("[class^='col-']").append("<span class='help-block'>Bu alan doldurulmalıdır.</span>");
          rtn = false;
        }
      });
    }
    else{
      if($("[name='boslukdoldurma']").val()==""){
        $("[name='boslukdoldurma']").parents('.form-group').addClass('has-error');
        $("[name='boslukdoldurma']").nextAll(".help-block").remove();
        $("[name='boslukdoldurma']").after("<span class='help-block'>Bu alan doldurulmalıdır.</span>");
        rtn = false;
      }
    }

    if(rtn)
      $('html, body').animate({
        scrollTop: $(".has-error").first().offset().top
      }, 2000);
    return rtn;
  });

});
</script>
@endsection

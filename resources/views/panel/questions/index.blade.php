@extends('panel.layouts.template')

@section('title', 'Sorular')

@section('content')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Soru Bankam
    <small>Sorularınız</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('panel') }}"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li><i class="fa fa-question"></i> Soru İşlemleri</li>
    <li class="active"><i class="fa fa-bank"></i> Soru Bankam</li>
  </ol>
</section>
@endsection

<div class="row">

	@if(count($questions) > 0)

	<?php $box = array("box-primary", "box-warning", "box-success") ?>
	@foreach($questions as $key=>$question)
	<div class="col-md-4 col-sm-6">
		<div class="box box-solid {{ $box[$key % 3] }}">
			<div class="box-header with-border">
				<h3 class="box-title">&nbsp;</h3>
				<div class="box-tools pull-right">
					<a href="#" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Küçült/Büyült"><i class="fa fa-minus"></i></a>
					<?php if($question->description<>""){ ?>
						<a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="İpucu"><i class="fa fa-question"></i></a>
					<?php } ?>
					<a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Çoğalt"><i class="fa fa-clone"></i></a>
					<a @if($question->question_reports->count() > 0) id="duzenlemeSilmeUyari" @endif href="{{ url("panel/sorular/duzenle/".$question->id) }}" class="btn btn-box-tool" data-toggle="tooltip" title="Düzenle"><i class="fa fa-edit"></i></a>
					<a @if($question->question_reports->count() > 0) id="duzenlemeSilmeUyari" @else id="soruSil" @endif href="#" class="btn btn-box-tool" data-id="{{ $question->id }}" data-toggle="tooltip" title="Soruyu Sil"><i class="fa fa-times"></i></a>
				</div>
			</div>
			<div class="box-body ">
				<div class="soruIcerik">
					<div class="soru">
						{!! $question->content !!}
					</div>
					@foreach($question->answers as $answer)
						@if($question->type == 'bosluk')
						<?php
							$cevaplar = explode(',', $answer->cevap);

						?>
						@foreach($cevaplar as $cevap)
							<div class="cevap default">{{ $cevap }}</div>
						@endforeach
						@else
							<div class="cevap {{ $answer->dogru ? 'dogru' : 'yanlis' }}">{{ $answer->cevap }}</div>
						@endif
					@endforeach
				</div>
			</div>
			<div class="box-footer">
				<div class="pull-left" style="margin-right:10px;">
					<i class="fa fa-clock-o"></i>
					{{ $question->time }} sn
				</div>
				@if($question->category_id > 0)
				<div class="pull-left" style="margin-right:10px;">
					<i class="fa fa-tags"></i>
					{{ $question->category->name }}
				</div>
				@endif
				<div class="pull-left" style="margin-right:10px;">
					<i class="fa fa-file"></i>
					{{ $question->type=="coktan" ? "Çoktan Seçmeli" : "Boşluk Doldurma" }}
				</div>
				<!--
				<div class="pull-left" style="margin-right:10px;">
					<i class="fa fa-globe"></i>
					Herkese Açık
				</div>
				-->
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	@endforeach
	@else
	<div class="alert alert-warning alert-dismissable">
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	    <h4><i class="icon fa fa-warning"></i> Uyarı!</h4>
	    <a href="{{ url('panel/sorular/yeni') }}">Sorunuz bulunmamaktadır. Soru oluşturmak için burayı tıklayın.</a>
	  </div>
	@endif

</div>
@endsection

@section('scripts')
<script>
	$(function(){
		$('.box-body .soruIcerik').slimScroll({
		    height: '350px'
		});

		$(document).on('click', '#duzenlemeSilmeUyari', function(e){
			e.preventDefault();
			Swal.fire('Uyarı', 'Bu soru sınavlarda çözüldüğü için düzenleme/silme işlemi yapılamaz.', 'warning');
		});

		$(document).on('click', '#soruSil', function(){
			var th = $(this);
			Swal.fire({
				title: "Silmek istediğine emin misin?",
				text: "Sildiğin soru geri getirilemez!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Evet, eminim",
				cancelButtonText: "Hayır, silmek istemiyorum",
				closeOnConfirm: false,
				closeOnCancel: true,
				showLoaderOnConfirm: true
				},
				function(isConfirm){
					if (isConfirm) {
						$.ajax({
				          url: "{{ url('panel/sorular/sil') }}" + '/' + $(th).data('id'),
				          type: 'POST'
				        })
				        .done(function() {
				          window.location.href= "{{ url('panel/sorular') }}";
				        })
				        .fail(function(jqXHR, textStatus) {
				          Swal.fire('Beklenmedik bir hata oluştu!' + textStatus);
				        });
					}
					else{
						swal.close();
					}
				});
		});


	});
</script>

@endsection

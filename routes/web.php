<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index');

Route::get('confirm/{token}', 'Auth\AuthController@getConfirm');
Route::get('auth/resend', 'Auth\AuthController@getResend');

Route::get('kodilegir', 'Panel\ExamController@kodIleGir');

Route::group(['middleware' => 'auth'], function () {
    Route::get('sinav/{code}', 'Panel\ExamController@exam');
    Route::post('sinavgiriskontrol/{code}', 'Panel\ExamController@sinavGirisKontrol');
    Route::post('cevapla', 'Panel\ExamController@cevapla');
    Route::post('senkroncevapla', 'Panel\ExamController@senkronCevapla');
    Route::get('senkron/{code}', 'Panel\ExamController@senkronSinav');
    Route::post('senkronverial', 'Panel\ExamController@senkronVeriAl');
    Route::post('senkronsinavbitir', 'Panel\ExamController@senkronSinavBitir');
});

Route::group(['prefix' => 'panel', 'as' => 'panel.', 'middleware' => 'auth'], function(){

    Route::get('/', 'Panel\PanelController@index');

    Route::get('profilim', 'Panel\PanelController@profilim');
    Route::post('profilresimyukle', 'Panel\PanelController@profilResimYukle');
    Route::post('profilbilgiguncelle', 'Panel\PanelController@profilBilgiGuncelle');
    Route::post('profilsifreguncelle', 'Panel\PanelController@profilSifreGuncelle');
    Route::post('ilcegetir', 'Panel\PanelController@ilceGetir');

    Route::get('sorular', 'Panel\QuestionController@index');
    Route::get('sorular/yeni', 'Panel\QuestionController@getCreate');
    Route::post('sorular/yeni', 'Panel\QuestionController@postCreate');
    Route::get('sorular/duzenle/{id}', 'Panel\QuestionController@getEdit');
    Route::post('sorular/duzenle/{id}', 'Panel\QuestionController@postEdit');
    Route::post('sorular/sil/{id}', 'Panel\QuestionController@destroy');

    Route::get('kategoriler', 'Panel\CategoryController@index');
    Route::get('kategoriler/yeni', 'Panel\CategoryController@getCreate');
    Route::post('kategoriler/yeni', 'Panel\CategoryController@postCreate');
    Route::get('kategoriler/duzenle/{id}', 'Panel\CategoryController@getEdit');
    Route::post('kategoriler/duzenle/{id}', 'Panel\CategoryController@postEdit');
    Route::post('kategoriler/sil/{id}', 'Panel\CategoryController@destroy');

    Route::get('sinavlar', 'Panel\ExamController@index');
    Route::get('sinavlar/yeni', 'Panel\ExamController@getStep1');
    Route::get('sinavlar/yeni/2', 'Panel\ExamController@getStep2');
    //Route::get('sinavlar/yeni/3', 'Panel\ExamController@getStep3');
    Route::post('sinavlar/yeni/{step?}', 'Panel\ExamController@postCreate');
    Route::get('sinavlar/duzenle/{id}', 'Panel\ExamController@getEditStep1');
    Route::get('sinavlar/duzenle/{id}/2', 'Panel\ExamController@getEditStep2');
    //Route::get('sinavlar/duzenle/{id}/3', 'Panel\ExamController@getEditStep3');
    Route::post('sinavlar/duzenle/{id}/{step?}', 'Panel\ExamController@postEdit');
    Route::post('sinavlar/sil/{id}', 'Panel\ExamController@destroy');
    Route::post('sinavlar/kodgetir', 'Panel\ExamController@kodGetir');
    Route::post('sinavlar/baslat/{id}', 'Panel\ExamController@sinavManuelBaslat');
    Route::post('sinavlar/bitir/{id}', 'Panel\ExamController@sinavManuelBitir');

    Route::get('sonuclarim', 'Panel\ReportController@sinavSonuclarim');
    Route::get('sonuclarim/{id}', 'Panel\ReportController@sinavSonucumuGetir');

    Route::get('sinavrapor/{id}', 'Panel\ReportController@sinavRapor');

});

﻿(function ($) {
    $.fn.progressTimer = function (options) {
        var settings = $.extend($.fn.progressTimer.defaults, options);

        var $jq = this;
        var output = {
            
            'stop':function(){
                $jq.each(function(){
                    var _this = this;
                    window.clearInterval(_this.interval);
                });
            },

            'init': function () {
                $jq.each(function() {
                    var _this = this;

                    $(_this).empty();
                    var barContainer = $("<div>").addClass("progress active progress-striped");
                    var bar = $("<div>").addClass("progress-bar").addClass(settings.baseStyle)
                        .attr("role", "progressbar")
                        .attr("aria-valuenow", "100")
                        .attr("aria-valuemin", "0")
                        .attr("aria-valuemax", settings.timeLimit)
                        .attr("style", "width:100%");

                    bar.text(settings.timeLimit + " sn");
                    bar.appendTo(barContainer);
                    barContainer.appendTo($(_this));
                    
                    var start = new Date();
                    var limit = settings.timeLimit * 1000;
                    _this.interval = window.setInterval(function () {
                        var elapsed = new Date() - start;
                        bar.width(( 100 - ((elapsed / limit) * 100)) + "%");
                        bar.text(Math.round((settings.timeLimit - (elapsed / 1000))) + ' sn');

                        if (limit - elapsed <= settings.warningThreshold * 1000)
                            bar.removeClass(settings.baseStyle)
                               .removeClass(settings.completeStyle)
                               .addClass(settings.warningStyle);

                        if (elapsed >= limit) {
                            window.clearInterval(_this.interval);

                            bar.removeClass(settings.baseStyle)
                                .removeClass(settings.warningStyle)
                                .addClass(settings.completeStyle);

                            settings.onFinish.call(_this);
                        }

                    }, 250);
                });
            }
        }

        output.init();

        return output;
    };

    $.fn.progressTimer.defaults = {
        timeLimit: 60,  //total number of seconds
        warningThreshold: 5,  //seconds remaining triggering switch to warning color
        onFinish: function () {},  //invoked once the timer expires
        baseStyle: '',  //bootstrap progress bar style at the beginning of the timer
        warningStyle: 'progress-bar-danger',  //bootstrap progress bar style in the warning phase
        completeStyle: 'progress-bar-success'  //bootstrap progress bar style at completion of timer
    };
    
}(jQuery));
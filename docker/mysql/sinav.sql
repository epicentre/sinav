/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 50727
 Source Host           : 127.0.0.1:3306
 Source Schema         : sinav

 Target Server Type    : MySQL
 Target Server Version : 50727
 File Encoding         : 65001

 Date: 09/08/2019 14:10:47
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

--
-- Veritabanı: `sinav`
--
CREATE DATABASE IF NOT EXISTS `sinav` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `sinav`;

-- ----------------------------
-- Table structure for answers
-- ----------------------------
DROP TABLE IF EXISTS `answers`;
CREATE TABLE `answers`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `question_id` bigint(20) UNSIGNED NOT NULL,
  `dogru` tinyint(1) NOT NULL DEFAULT 0,
  `cevap` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 49 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of answers
-- ----------------------------
INSERT INTO `answers` VALUES (1, 1, 1, 'CHAPTER III. A.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (2, 2, 0, 'Majesty!\' the.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (3, 2, 1, 'That your eye was.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (4, 2, 1, 'Rabbit coming to.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (5, 3, 1, 'Mock Turtle.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (6, 4, 1, 'I\'m grown up now,\'.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (7, 5, 1, 'Alice cautiously.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (8, 5, 1, 'I suppose I ought.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (9, 5, 1, 'The Antipathies, I.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (10, 5, 0, 'King repeated.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (11, 6, 1, 'IS that to be a.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (12, 7, 0, 'Alice had no.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (13, 7, 1, 'Puss,\' she began.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (14, 7, 0, 'Which way?\'.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (15, 8, 1, 'Then came a.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (16, 9, 0, 'Alice had learnt.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (17, 9, 0, 'CAN all that green.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (18, 9, 1, 'I suppose, by.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (19, 10, 1, 'Queen had only one.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (20, 11, 1, 'I must have got.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (21, 12, 0, 'I\'m quite tired.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (22, 12, 0, 'Duchess was VERY.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (23, 12, 0, 'Come on!\'.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (24, 13, 1, 'I\'m sure I can\'t.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (25, 13, 0, 'Mock Turtle went.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (26, 13, 1, 'All the time when.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (27, 13, 1, 'Mock Turtle Soup.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (28, 13, 1, 'Gryphon. \'They.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (29, 14, 0, 'King: \'however, it.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (30, 14, 1, 'I almost wish I.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (31, 14, 0, 'Conqueror, whose.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (32, 14, 0, 'Alice hastily.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (33, 15, 1, 'Dormouse went on.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (34, 16, 1, 'For some minutes.', '2019-08-09 11:10:20', '2019-08-09 11:10:20');
INSERT INTO `answers` VALUES (35, 17, 1, 'Alice, as she did.', '2019-08-09 11:10:21', '2019-08-09 11:10:21');
INSERT INTO `answers` VALUES (36, 17, 0, 'I suppose you\'ll.', '2019-08-09 11:10:21', '2019-08-09 11:10:21');
INSERT INTO `answers` VALUES (37, 17, 0, 'Gryphon answered.', '2019-08-09 11:10:21', '2019-08-09 11:10:21');
INSERT INTO `answers` VALUES (38, 17, 1, 'What happened to.', '2019-08-09 11:10:21', '2019-08-09 11:10:21');
INSERT INTO `answers` VALUES (39, 17, 0, 'Oh dear! I wish.', '2019-08-09 11:10:21', '2019-08-09 11:10:21');
INSERT INTO `answers` VALUES (40, 18, 0, 'But here, to.', '2019-08-09 11:10:21', '2019-08-09 11:10:21');
INSERT INTO `answers` VALUES (41, 18, 0, 'I should like to.', '2019-08-09 11:10:21', '2019-08-09 11:10:21');
INSERT INTO `answers` VALUES (42, 18, 0, 'Duchess: \'and the.', '2019-08-09 11:10:21', '2019-08-09 11:10:21');
INSERT INTO `answers` VALUES (43, 18, 0, 'I wish you could.', '2019-08-09 11:10:21', '2019-08-09 11:10:21');
INSERT INTO `answers` VALUES (44, 19, 0, 'The Antipathies, I.', '2019-08-09 11:10:21', '2019-08-09 11:10:21');
INSERT INTO `answers` VALUES (45, 19, 1, 'Majesty,\' he.', '2019-08-09 11:10:21', '2019-08-09 11:10:21');
INSERT INTO `answers` VALUES (46, 19, 1, 'The soldiers were.', '2019-08-09 11:10:21', '2019-08-09 11:10:21');
INSERT INTO `answers` VALUES (47, 19, 0, 'Dodo managed it.).', '2019-08-09 11:10:21', '2019-08-09 11:10:21');
INSERT INTO `answers` VALUES (48, 20, 1, 'So she sat down.', '2019-08-09 11:10:21', '2019-08-09 11:10:21');

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `categories_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `categories_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cities
-- ----------------------------
DROP TABLE IF EXISTS `cities`;
CREATE TABLE `cities`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `city` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 82 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cities
-- ----------------------------
INSERT INTO `cities` VALUES (1, 'Adana');
INSERT INTO `cities` VALUES (2, 'Adıyaman');
INSERT INTO `cities` VALUES (3, 'Afyonkarahisar');
INSERT INTO `cities` VALUES (4, 'Ağrı');
INSERT INTO `cities` VALUES (5, 'Aksaray');
INSERT INTO `cities` VALUES (6, 'Amasya');
INSERT INTO `cities` VALUES (7, 'Ankara');
INSERT INTO `cities` VALUES (8, 'Antalya');
INSERT INTO `cities` VALUES (9, 'Ardahan');
INSERT INTO `cities` VALUES (10, 'Artvin');
INSERT INTO `cities` VALUES (11, 'Aydın');
INSERT INTO `cities` VALUES (12, 'Balıkesir');
INSERT INTO `cities` VALUES (13, 'Bartın');
INSERT INTO `cities` VALUES (14, 'Batman');
INSERT INTO `cities` VALUES (15, 'Bayburt');
INSERT INTO `cities` VALUES (16, 'Bilecik');
INSERT INTO `cities` VALUES (17, 'Bingöl');
INSERT INTO `cities` VALUES (18, 'Bitlis');
INSERT INTO `cities` VALUES (19, 'Bolu');
INSERT INTO `cities` VALUES (20, 'Burdur');
INSERT INTO `cities` VALUES (21, 'Bursa');
INSERT INTO `cities` VALUES (22, 'Çanakkale');
INSERT INTO `cities` VALUES (23, 'Çankırı');
INSERT INTO `cities` VALUES (24, 'Çorum');
INSERT INTO `cities` VALUES (25, 'Denizli');
INSERT INTO `cities` VALUES (26, 'Diyarbakır');
INSERT INTO `cities` VALUES (27, 'Düzce');
INSERT INTO `cities` VALUES (28, 'Edirne');
INSERT INTO `cities` VALUES (29, 'Elazığ');
INSERT INTO `cities` VALUES (30, 'Erzincan');
INSERT INTO `cities` VALUES (31, 'Erzurum');
INSERT INTO `cities` VALUES (32, 'Eskişehir');
INSERT INTO `cities` VALUES (33, 'Gaziantep');
INSERT INTO `cities` VALUES (34, 'Giresun');
INSERT INTO `cities` VALUES (35, 'Gümüşhane');
INSERT INTO `cities` VALUES (36, 'Hakkari');
INSERT INTO `cities` VALUES (37, 'Hatay');
INSERT INTO `cities` VALUES (38, 'Iğdır');
INSERT INTO `cities` VALUES (39, 'Isparta');
INSERT INTO `cities` VALUES (40, 'İstanbul');
INSERT INTO `cities` VALUES (41, 'İzmir');
INSERT INTO `cities` VALUES (42, 'Kahramanmaraş');
INSERT INTO `cities` VALUES (43, 'Karabük');
INSERT INTO `cities` VALUES (44, 'Karaman');
INSERT INTO `cities` VALUES (45, 'Kars');
INSERT INTO `cities` VALUES (46, 'Kastamonu');
INSERT INTO `cities` VALUES (47, 'Kayseri');
INSERT INTO `cities` VALUES (48, 'Kırıkkale');
INSERT INTO `cities` VALUES (49, 'Kırklareli');
INSERT INTO `cities` VALUES (50, 'Kırşehir');
INSERT INTO `cities` VALUES (51, 'Kilis');
INSERT INTO `cities` VALUES (52, 'Kocaeli');
INSERT INTO `cities` VALUES (53, 'Konya');
INSERT INTO `cities` VALUES (54, 'Kütahya');
INSERT INTO `cities` VALUES (55, 'Malatya');
INSERT INTO `cities` VALUES (56, 'Manisa');
INSERT INTO `cities` VALUES (57, 'Mardin');
INSERT INTO `cities` VALUES (58, 'Mersin');
INSERT INTO `cities` VALUES (59, 'Muğla');
INSERT INTO `cities` VALUES (60, 'Muş');
INSERT INTO `cities` VALUES (61, 'Nevşehir');
INSERT INTO `cities` VALUES (62, 'Niğde');
INSERT INTO `cities` VALUES (63, 'Ordu');
INSERT INTO `cities` VALUES (64, 'Osmaniye');
INSERT INTO `cities` VALUES (65, 'Rize');
INSERT INTO `cities` VALUES (66, 'Sakarya');
INSERT INTO `cities` VALUES (67, 'Samsun');
INSERT INTO `cities` VALUES (68, 'Siirt');
INSERT INTO `cities` VALUES (69, 'Sinop');
INSERT INTO `cities` VALUES (70, 'Sivas');
INSERT INTO `cities` VALUES (71, 'Şanlıurfa');
INSERT INTO `cities` VALUES (72, 'Şırnak');
INSERT INTO `cities` VALUES (73, 'Tekirdağ');
INSERT INTO `cities` VALUES (74, 'Tokat');
INSERT INTO `cities` VALUES (75, 'Trabzon');
INSERT INTO `cities` VALUES (76, 'Tunceli');
INSERT INTO `cities` VALUES (77, 'Uşak');
INSERT INTO `cities` VALUES (78, 'Van');
INSERT INTO `cities` VALUES (79, 'Yalova');
INSERT INTO `cities` VALUES (80, 'Yozgat');
INSERT INTO `cities` VALUES (81, 'Zonguldak');

-- ----------------------------
-- Table structure for counties
-- ----------------------------
DROP TABLE IF EXISTS `counties`;
CREATE TABLE `counties`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `city_id` bigint(20) UNSIGNED NOT NULL,
  `county` varchar(75) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `counties_city_id_foreign`(`city_id`) USING BTREE,
  CONSTRAINT `counties_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 897 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of counties
-- ----------------------------
INSERT INTO `counties` VALUES (1, 1, 'Aladağ');
INSERT INTO `counties` VALUES (2, 1, 'Ceyhan');
INSERT INTO `counties` VALUES (3, 1, 'Çukurova');
INSERT INTO `counties` VALUES (4, 1, 'Feke');
INSERT INTO `counties` VALUES (5, 1, 'İmamoğlu');
INSERT INTO `counties` VALUES (6, 1, 'Karaisalı');
INSERT INTO `counties` VALUES (7, 1, 'Karataş');
INSERT INTO `counties` VALUES (8, 1, 'Kozan');
INSERT INTO `counties` VALUES (9, 1, 'Pozantı');
INSERT INTO `counties` VALUES (10, 1, 'Saimbeyli');
INSERT INTO `counties` VALUES (11, 1, 'Sarıçam');
INSERT INTO `counties` VALUES (12, 1, 'Seyhan');
INSERT INTO `counties` VALUES (13, 1, 'Tufanbeyli');
INSERT INTO `counties` VALUES (14, 1, 'Yumurtalık');
INSERT INTO `counties` VALUES (15, 1, 'Yüreğir');
INSERT INTO `counties` VALUES (16, 2, 'Besni');
INSERT INTO `counties` VALUES (17, 2, 'Çelikhan');
INSERT INTO `counties` VALUES (18, 2, 'Gerger');
INSERT INTO `counties` VALUES (19, 2, 'Gölbaşı');
INSERT INTO `counties` VALUES (20, 2, 'Kahta');
INSERT INTO `counties` VALUES (21, 2, 'Merkez');
INSERT INTO `counties` VALUES (22, 2, 'Samsat');
INSERT INTO `counties` VALUES (23, 2, 'Sincik');
INSERT INTO `counties` VALUES (24, 2, 'Tut');
INSERT INTO `counties` VALUES (25, 3, 'Başmakçı');
INSERT INTO `counties` VALUES (26, 3, 'Bayat');
INSERT INTO `counties` VALUES (27, 3, 'Bolvadin');
INSERT INTO `counties` VALUES (28, 3, 'Çay');
INSERT INTO `counties` VALUES (29, 3, 'Çobanlar');
INSERT INTO `counties` VALUES (30, 3, 'Dazkırı');
INSERT INTO `counties` VALUES (31, 3, 'Dinar');
INSERT INTO `counties` VALUES (32, 3, 'Emirdağ');
INSERT INTO `counties` VALUES (33, 3, 'Evciler');
INSERT INTO `counties` VALUES (34, 3, 'Hocalar');
INSERT INTO `counties` VALUES (35, 3, 'İhsaniye');
INSERT INTO `counties` VALUES (36, 3, 'İscehisar');
INSERT INTO `counties` VALUES (37, 3, 'Kızılören');
INSERT INTO `counties` VALUES (38, 3, 'Sandıklı');
INSERT INTO `counties` VALUES (39, 3, 'Sinanpaşa');
INSERT INTO `counties` VALUES (40, 3, 'Sultandağı');
INSERT INTO `counties` VALUES (41, 3, 'Şuhut');
INSERT INTO `counties` VALUES (42, 4, 'Diyadin');
INSERT INTO `counties` VALUES (43, 4, 'Doğubayazıt');
INSERT INTO `counties` VALUES (44, 4, 'Eleşkirt');
INSERT INTO `counties` VALUES (45, 4, 'Hamur');
INSERT INTO `counties` VALUES (46, 4, 'Patnos');
INSERT INTO `counties` VALUES (47, 4, 'Taşlıçay');
INSERT INTO `counties` VALUES (48, 4, 'Tutak');
INSERT INTO `counties` VALUES (49, 5, 'Ağaçören');
INSERT INTO `counties` VALUES (50, 5, 'Eskil');
INSERT INTO `counties` VALUES (51, 5, 'Gülağaç');
INSERT INTO `counties` VALUES (52, 5, 'Güzelyurt');
INSERT INTO `counties` VALUES (53, 5, 'Ortaköy');
INSERT INTO `counties` VALUES (54, 5, 'Sarıyahşi');
INSERT INTO `counties` VALUES (55, 6, 'Göynücek');
INSERT INTO `counties` VALUES (56, 6, 'Gümüşhacıköy');
INSERT INTO `counties` VALUES (57, 6, 'Hamamözü');
INSERT INTO `counties` VALUES (58, 6, 'Merzifon');
INSERT INTO `counties` VALUES (59, 6, 'Suluova');
INSERT INTO `counties` VALUES (60, 6, 'Taşova');
INSERT INTO `counties` VALUES (61, 7, 'Akyurt');
INSERT INTO `counties` VALUES (62, 7, 'Altındağ');
INSERT INTO `counties` VALUES (63, 7, 'Ayaş');
INSERT INTO `counties` VALUES (64, 7, 'Bala');
INSERT INTO `counties` VALUES (65, 7, 'Beypazarı');
INSERT INTO `counties` VALUES (66, 7, 'Çamlıdere');
INSERT INTO `counties` VALUES (67, 7, 'Çankaya');
INSERT INTO `counties` VALUES (68, 7, 'Çubuk');
INSERT INTO `counties` VALUES (69, 7, 'Elmadağ');
INSERT INTO `counties` VALUES (70, 7, 'Etimesgut');
INSERT INTO `counties` VALUES (71, 7, 'Evren');
INSERT INTO `counties` VALUES (72, 7, 'Güdül');
INSERT INTO `counties` VALUES (73, 7, 'Haymana');
INSERT INTO `counties` VALUES (74, 7, 'Kalecik');
INSERT INTO `counties` VALUES (75, 7, 'Kazan');
INSERT INTO `counties` VALUES (76, 7, 'Keçiören');
INSERT INTO `counties` VALUES (77, 7, 'Kızılcahamam');
INSERT INTO `counties` VALUES (78, 7, 'Mamak');
INSERT INTO `counties` VALUES (79, 7, 'Nallıhan');
INSERT INTO `counties` VALUES (80, 7, 'Polatlı');
INSERT INTO `counties` VALUES (81, 7, 'Pursaklar');
INSERT INTO `counties` VALUES (82, 7, 'Sincan');
INSERT INTO `counties` VALUES (83, 7, 'Şereflikoçhisar');
INSERT INTO `counties` VALUES (84, 7, 'Yenimahalle');
INSERT INTO `counties` VALUES (85, 8, 'Akseki');
INSERT INTO `counties` VALUES (86, 8, 'Aksu');
INSERT INTO `counties` VALUES (87, 8, 'Alanya');
INSERT INTO `counties` VALUES (88, 8, 'Demre');
INSERT INTO `counties` VALUES (89, 8, 'Döşemealtı');
INSERT INTO `counties` VALUES (90, 8, 'Elmalı');
INSERT INTO `counties` VALUES (91, 8, 'Finike');
INSERT INTO `counties` VALUES (92, 8, 'Gazipaşa');
INSERT INTO `counties` VALUES (93, 8, 'Gündoğmuş');
INSERT INTO `counties` VALUES (94, 8, 'İbradı');
INSERT INTO `counties` VALUES (95, 8, 'Kaş');
INSERT INTO `counties` VALUES (96, 8, 'Kemer');
INSERT INTO `counties` VALUES (97, 8, 'Kepez');
INSERT INTO `counties` VALUES (98, 8, 'Konyaaltı');
INSERT INTO `counties` VALUES (99, 8, 'Korkuteli');
INSERT INTO `counties` VALUES (100, 8, 'Kumluca');
INSERT INTO `counties` VALUES (101, 8, 'Manavgat');
INSERT INTO `counties` VALUES (102, 8, 'Muratpaşa');
INSERT INTO `counties` VALUES (103, 8, 'Serik');
INSERT INTO `counties` VALUES (104, 9, 'Çıldır');
INSERT INTO `counties` VALUES (105, 9, 'Damal');
INSERT INTO `counties` VALUES (106, 9, 'Göle');
INSERT INTO `counties` VALUES (107, 9, 'Hanak');
INSERT INTO `counties` VALUES (108, 9, 'Posof');
INSERT INTO `counties` VALUES (109, 10, 'Ardanuç');
INSERT INTO `counties` VALUES (110, 10, 'Arhavi');
INSERT INTO `counties` VALUES (111, 10, 'Borçka');
INSERT INTO `counties` VALUES (112, 10, 'Hopa');
INSERT INTO `counties` VALUES (113, 10, 'Murgul');
INSERT INTO `counties` VALUES (114, 10, 'Şavşat');
INSERT INTO `counties` VALUES (115, 10, 'Yusufeli');
INSERT INTO `counties` VALUES (116, 11, 'Bozdoğan');
INSERT INTO `counties` VALUES (117, 11, 'Buharkent');
INSERT INTO `counties` VALUES (118, 11, 'Çine');
INSERT INTO `counties` VALUES (119, 11, 'Didim');
INSERT INTO `counties` VALUES (120, 11, 'Efeler');
INSERT INTO `counties` VALUES (121, 11, 'Germencik');
INSERT INTO `counties` VALUES (122, 11, 'İncirliova');
INSERT INTO `counties` VALUES (123, 11, 'Karacasu');
INSERT INTO `counties` VALUES (124, 11, 'Karpuzlu');
INSERT INTO `counties` VALUES (125, 11, 'Koçarlı');
INSERT INTO `counties` VALUES (126, 11, 'Köşk');
INSERT INTO `counties` VALUES (127, 11, 'Kuşadası');
INSERT INTO `counties` VALUES (128, 11, 'Kuyucak');
INSERT INTO `counties` VALUES (129, 11, 'Nazilli');
INSERT INTO `counties` VALUES (130, 11, 'Söke');
INSERT INTO `counties` VALUES (131, 11, 'Sultanhisar');
INSERT INTO `counties` VALUES (132, 11, 'Yenipazar');
INSERT INTO `counties` VALUES (133, 12, 'Altıeylül');
INSERT INTO `counties` VALUES (134, 12, 'Ayvalık');
INSERT INTO `counties` VALUES (135, 12, 'Balya');
INSERT INTO `counties` VALUES (136, 12, 'Bandırma');
INSERT INTO `counties` VALUES (137, 12, 'Bigadiç');
INSERT INTO `counties` VALUES (138, 12, 'Burhaniye');
INSERT INTO `counties` VALUES (139, 12, 'Dursunbey');
INSERT INTO `counties` VALUES (140, 12, 'Edremit');
INSERT INTO `counties` VALUES (141, 12, 'Erdek');
INSERT INTO `counties` VALUES (142, 12, 'Gömeç');
INSERT INTO `counties` VALUES (143, 12, 'Gönen');
INSERT INTO `counties` VALUES (144, 12, 'Havran');
INSERT INTO `counties` VALUES (145, 12, 'İvrindi');
INSERT INTO `counties` VALUES (146, 12, 'Karesi');
INSERT INTO `counties` VALUES (147, 12, 'Kepsut');
INSERT INTO `counties` VALUES (148, 12, 'Manyas');
INSERT INTO `counties` VALUES (149, 12, 'Marmara');
INSERT INTO `counties` VALUES (150, 12, 'Savaştepe');
INSERT INTO `counties` VALUES (151, 12, 'Sındırgı');
INSERT INTO `counties` VALUES (152, 12, 'Susurluk');
INSERT INTO `counties` VALUES (153, 13, 'Amasra');
INSERT INTO `counties` VALUES (154, 13, 'Kurucaşile');
INSERT INTO `counties` VALUES (155, 13, 'Ulus');
INSERT INTO `counties` VALUES (156, 14, 'Beşiri');
INSERT INTO `counties` VALUES (157, 14, 'Gercüş');
INSERT INTO `counties` VALUES (158, 14, 'Hasankeyf');
INSERT INTO `counties` VALUES (159, 14, 'Kozluk');
INSERT INTO `counties` VALUES (160, 14, 'Sason');
INSERT INTO `counties` VALUES (161, 15, 'Aydıntepe');
INSERT INTO `counties` VALUES (162, 15, 'Demirözü');
INSERT INTO `counties` VALUES (163, 16, 'Bozüyük');
INSERT INTO `counties` VALUES (164, 16, 'Gölpazarı');
INSERT INTO `counties` VALUES (165, 16, 'İnhisar');
INSERT INTO `counties` VALUES (166, 16, 'Osmaneli');
INSERT INTO `counties` VALUES (167, 16, 'Pazaryeri');
INSERT INTO `counties` VALUES (168, 16, 'Söğüt');
INSERT INTO `counties` VALUES (169, 17, 'Adaklı');
INSERT INTO `counties` VALUES (170, 17, 'Genç');
INSERT INTO `counties` VALUES (171, 17, 'Karlıova');
INSERT INTO `counties` VALUES (172, 17, 'Kiğı');
INSERT INTO `counties` VALUES (173, 17, 'Solhan');
INSERT INTO `counties` VALUES (174, 17, 'Yayladere');
INSERT INTO `counties` VALUES (175, 17, 'Yedisu');
INSERT INTO `counties` VALUES (176, 18, 'Adilcevaz');
INSERT INTO `counties` VALUES (177, 18, 'Ahlat');
INSERT INTO `counties` VALUES (178, 18, 'Güroymak');
INSERT INTO `counties` VALUES (179, 18, 'Hizan');
INSERT INTO `counties` VALUES (180, 18, 'Mutki');
INSERT INTO `counties` VALUES (181, 18, 'Tatvan');
INSERT INTO `counties` VALUES (182, 19, 'Dörtdivan');
INSERT INTO `counties` VALUES (183, 19, 'Gerede');
INSERT INTO `counties` VALUES (184, 19, 'Göynük');
INSERT INTO `counties` VALUES (185, 19, 'Kıbrıscık');
INSERT INTO `counties` VALUES (186, 19, 'Mengen');
INSERT INTO `counties` VALUES (187, 19, 'Mudurnu');
INSERT INTO `counties` VALUES (188, 19, 'Seben');
INSERT INTO `counties` VALUES (189, 19, 'Yeniçağa');
INSERT INTO `counties` VALUES (190, 20, 'Ağlasun');
INSERT INTO `counties` VALUES (191, 20, 'Altınyayla');
INSERT INTO `counties` VALUES (192, 20, 'Bucak');
INSERT INTO `counties` VALUES (193, 20, 'Çavdır');
INSERT INTO `counties` VALUES (194, 20, 'Çeltikçi');
INSERT INTO `counties` VALUES (195, 20, 'Gölhisar');
INSERT INTO `counties` VALUES (196, 20, 'Karamanlı');
INSERT INTO `counties` VALUES (197, 20, 'Tefenni');
INSERT INTO `counties` VALUES (198, 20, 'Yeşilova');
INSERT INTO `counties` VALUES (199, 21, 'Büyükorhan');
INSERT INTO `counties` VALUES (200, 21, 'Gemlik');
INSERT INTO `counties` VALUES (201, 21, 'Gürsu');
INSERT INTO `counties` VALUES (202, 21, 'Harmancık');
INSERT INTO `counties` VALUES (203, 21, 'İnegöl');
INSERT INTO `counties` VALUES (204, 21, 'İznik');
INSERT INTO `counties` VALUES (205, 21, 'Karacabey');
INSERT INTO `counties` VALUES (206, 21, 'Keles');
INSERT INTO `counties` VALUES (207, 21, 'Kestel');
INSERT INTO `counties` VALUES (208, 21, 'Mudanya');
INSERT INTO `counties` VALUES (209, 21, 'Mustafakemalpaşa');
INSERT INTO `counties` VALUES (210, 21, 'Nilüfer');
INSERT INTO `counties` VALUES (211, 21, 'Orhaneli');
INSERT INTO `counties` VALUES (212, 21, 'Orhangazi');
INSERT INTO `counties` VALUES (213, 21, 'Osmangazi');
INSERT INTO `counties` VALUES (214, 21, 'Yenişehir');
INSERT INTO `counties` VALUES (215, 21, 'Yıldırım');
INSERT INTO `counties` VALUES (216, 22, 'Ayvacık');
INSERT INTO `counties` VALUES (217, 22, 'Bayramiç');
INSERT INTO `counties` VALUES (218, 22, 'Biga');
INSERT INTO `counties` VALUES (219, 22, 'Bozcaada');
INSERT INTO `counties` VALUES (220, 22, 'Çan');
INSERT INTO `counties` VALUES (221, 22, 'Eceabat');
INSERT INTO `counties` VALUES (222, 22, 'Ezine');
INSERT INTO `counties` VALUES (223, 22, 'Gelibolu');
INSERT INTO `counties` VALUES (224, 22, 'Gökçeada');
INSERT INTO `counties` VALUES (225, 22, 'Lapseki');
INSERT INTO `counties` VALUES (226, 22, 'Yenice');
INSERT INTO `counties` VALUES (227, 23, 'Atkaracalar');
INSERT INTO `counties` VALUES (228, 23, 'Bayramören');
INSERT INTO `counties` VALUES (229, 23, 'Çerkeş');
INSERT INTO `counties` VALUES (230, 23, 'Eldivan');
INSERT INTO `counties` VALUES (231, 23, 'Ilgaz');
INSERT INTO `counties` VALUES (232, 23, 'Kızılırmak');
INSERT INTO `counties` VALUES (233, 23, 'Korgun');
INSERT INTO `counties` VALUES (234, 23, 'Kurşunlu');
INSERT INTO `counties` VALUES (235, 23, 'Orta');
INSERT INTO `counties` VALUES (236, 23, 'Şabanözü');
INSERT INTO `counties` VALUES (237, 23, 'Yapraklı');
INSERT INTO `counties` VALUES (238, 24, 'Alaca');
INSERT INTO `counties` VALUES (239, 24, 'Boğazkale');
INSERT INTO `counties` VALUES (240, 24, 'Dodurga');
INSERT INTO `counties` VALUES (241, 24, 'İskilip');
INSERT INTO `counties` VALUES (242, 24, 'Kargı');
INSERT INTO `counties` VALUES (243, 24, 'Laçin');
INSERT INTO `counties` VALUES (244, 24, 'Mecitözü');
INSERT INTO `counties` VALUES (245, 24, 'Oğuzlar');
INSERT INTO `counties` VALUES (246, 24, 'Osmancık');
INSERT INTO `counties` VALUES (247, 24, 'Sungurlu');
INSERT INTO `counties` VALUES (248, 24, 'Uğurludağ');
INSERT INTO `counties` VALUES (249, 25, 'Acıpayam');
INSERT INTO `counties` VALUES (250, 25, 'Babadağ');
INSERT INTO `counties` VALUES (251, 25, 'Baklan');
INSERT INTO `counties` VALUES (252, 25, 'Bekilli');
INSERT INTO `counties` VALUES (253, 25, 'Beyağaç');
INSERT INTO `counties` VALUES (254, 25, 'Bozkurt');
INSERT INTO `counties` VALUES (255, 25, 'Buldan');
INSERT INTO `counties` VALUES (256, 25, 'Çal');
INSERT INTO `counties` VALUES (257, 25, 'Çameli');
INSERT INTO `counties` VALUES (258, 25, 'Çardak');
INSERT INTO `counties` VALUES (259, 25, 'Çivril');
INSERT INTO `counties` VALUES (260, 25, 'Güney');
INSERT INTO `counties` VALUES (261, 25, 'Honaz');
INSERT INTO `counties` VALUES (262, 25, 'Kale');
INSERT INTO `counties` VALUES (263, 25, 'Merkezefendi');
INSERT INTO `counties` VALUES (264, 25, 'Pamukkale');
INSERT INTO `counties` VALUES (265, 25, 'Sarayköy');
INSERT INTO `counties` VALUES (266, 25, 'Serinhisar');
INSERT INTO `counties` VALUES (267, 25, 'Tavas');
INSERT INTO `counties` VALUES (268, 26, 'Bağlar');
INSERT INTO `counties` VALUES (269, 26, 'Bismil');
INSERT INTO `counties` VALUES (270, 26, 'Çermik');
INSERT INTO `counties` VALUES (271, 26, 'Çınar');
INSERT INTO `counties` VALUES (272, 26, 'Çüngüş');
INSERT INTO `counties` VALUES (273, 26, 'Dicle');
INSERT INTO `counties` VALUES (274, 26, 'Eğil');
INSERT INTO `counties` VALUES (275, 26, 'Ergani');
INSERT INTO `counties` VALUES (276, 26, 'Hani');
INSERT INTO `counties` VALUES (277, 26, 'Hazro');
INSERT INTO `counties` VALUES (278, 26, 'Kayapınar');
INSERT INTO `counties` VALUES (279, 26, 'Kocaköy');
INSERT INTO `counties` VALUES (280, 26, 'Kulp');
INSERT INTO `counties` VALUES (281, 26, 'Lice');
INSERT INTO `counties` VALUES (282, 26, 'Silvan');
INSERT INTO `counties` VALUES (283, 26, 'Sur');
INSERT INTO `counties` VALUES (284, 27, 'Akçakoca');
INSERT INTO `counties` VALUES (285, 27, 'Cumayeri');
INSERT INTO `counties` VALUES (286, 27, 'Çilimli');
INSERT INTO `counties` VALUES (287, 27, 'Gölyaka');
INSERT INTO `counties` VALUES (288, 27, 'Gümüşova');
INSERT INTO `counties` VALUES (289, 27, 'Kaynaşlı');
INSERT INTO `counties` VALUES (290, 27, 'Yığılca');
INSERT INTO `counties` VALUES (291, 28, 'Enez');
INSERT INTO `counties` VALUES (292, 28, 'Havsa');
INSERT INTO `counties` VALUES (293, 28, 'İpsala');
INSERT INTO `counties` VALUES (294, 28, 'Keşan');
INSERT INTO `counties` VALUES (295, 28, 'Lalapaşa');
INSERT INTO `counties` VALUES (296, 28, 'Meriç');
INSERT INTO `counties` VALUES (297, 28, 'Süloğlu');
INSERT INTO `counties` VALUES (298, 28, 'Uzunköprü');
INSERT INTO `counties` VALUES (299, 29, 'Ağın');
INSERT INTO `counties` VALUES (300, 29, 'Alacakaya');
INSERT INTO `counties` VALUES (301, 29, 'Arıcak');
INSERT INTO `counties` VALUES (302, 29, 'Baskil');
INSERT INTO `counties` VALUES (303, 29, 'Karakoçan');
INSERT INTO `counties` VALUES (304, 29, 'Keban');
INSERT INTO `counties` VALUES (305, 29, 'Kovancılar');
INSERT INTO `counties` VALUES (306, 29, 'Maden');
INSERT INTO `counties` VALUES (307, 29, 'Palu');
INSERT INTO `counties` VALUES (308, 29, 'Sivrice');
INSERT INTO `counties` VALUES (309, 30, 'Çayırlı');
INSERT INTO `counties` VALUES (310, 30, 'İliç');
INSERT INTO `counties` VALUES (311, 30, 'Kemah');
INSERT INTO `counties` VALUES (312, 30, 'Kemaliye');
INSERT INTO `counties` VALUES (313, 30, 'Otlukbeli');
INSERT INTO `counties` VALUES (314, 30, 'Refahiye');
INSERT INTO `counties` VALUES (315, 30, 'Tercan');
INSERT INTO `counties` VALUES (316, 30, 'Üzümlü');
INSERT INTO `counties` VALUES (317, 31, 'Aşkale');
INSERT INTO `counties` VALUES (318, 31, 'Aziziye');
INSERT INTO `counties` VALUES (319, 31, 'Çat');
INSERT INTO `counties` VALUES (320, 31, 'Hınıs');
INSERT INTO `counties` VALUES (321, 31, 'Horasan');
INSERT INTO `counties` VALUES (322, 31, 'İspir');
INSERT INTO `counties` VALUES (323, 31, 'Karaçoban');
INSERT INTO `counties` VALUES (324, 31, 'Karayazı');
INSERT INTO `counties` VALUES (325, 31, 'Köprüköy');
INSERT INTO `counties` VALUES (326, 31, 'Narman');
INSERT INTO `counties` VALUES (327, 31, 'Oltu');
INSERT INTO `counties` VALUES (328, 31, 'Olur');
INSERT INTO `counties` VALUES (329, 31, 'Palandöken');
INSERT INTO `counties` VALUES (330, 31, 'Pasinler');
INSERT INTO `counties` VALUES (331, 31, 'Pazaryolu');
INSERT INTO `counties` VALUES (332, 31, 'Şenkaya');
INSERT INTO `counties` VALUES (333, 31, 'Tekman');
INSERT INTO `counties` VALUES (334, 31, 'Tortum');
INSERT INTO `counties` VALUES (335, 31, 'Uzundere');
INSERT INTO `counties` VALUES (336, 31, 'Yakutiye');
INSERT INTO `counties` VALUES (337, 32, 'Alpu');
INSERT INTO `counties` VALUES (338, 32, 'Beylikova');
INSERT INTO `counties` VALUES (339, 32, 'Çifteler');
INSERT INTO `counties` VALUES (340, 32, 'Günyüzü');
INSERT INTO `counties` VALUES (341, 32, 'Han');
INSERT INTO `counties` VALUES (342, 32, 'İnönü');
INSERT INTO `counties` VALUES (343, 32, 'Mahmudiye');
INSERT INTO `counties` VALUES (344, 32, 'Mihalgazi');
INSERT INTO `counties` VALUES (345, 32, 'Mihalıççık');
INSERT INTO `counties` VALUES (346, 32, 'Odunpazarı');
INSERT INTO `counties` VALUES (347, 32, 'Sarıcakaya');
INSERT INTO `counties` VALUES (348, 32, 'Seyitgazi');
INSERT INTO `counties` VALUES (349, 32, 'Sivrihisar');
INSERT INTO `counties` VALUES (350, 32, 'Tepebaşı');
INSERT INTO `counties` VALUES (351, 33, 'Araban');
INSERT INTO `counties` VALUES (352, 33, 'İslahiye');
INSERT INTO `counties` VALUES (353, 33, 'Karkamış');
INSERT INTO `counties` VALUES (354, 33, 'Nizip');
INSERT INTO `counties` VALUES (355, 33, 'Nurdağı');
INSERT INTO `counties` VALUES (356, 33, 'Oğuzeli');
INSERT INTO `counties` VALUES (357, 33, 'Şahinbey');
INSERT INTO `counties` VALUES (358, 33, 'Şehitkamil');
INSERT INTO `counties` VALUES (359, 33, 'Yavuzeli');
INSERT INTO `counties` VALUES (360, 34, 'Alucra');
INSERT INTO `counties` VALUES (361, 34, 'Bulancak');
INSERT INTO `counties` VALUES (362, 34, 'Çamoluk');
INSERT INTO `counties` VALUES (363, 34, 'Çanakçı');
INSERT INTO `counties` VALUES (364, 34, 'Dereli');
INSERT INTO `counties` VALUES (365, 34, 'Doğankent');
INSERT INTO `counties` VALUES (366, 34, 'Espiye');
INSERT INTO `counties` VALUES (367, 34, 'Eynesil');
INSERT INTO `counties` VALUES (368, 34, 'Görele');
INSERT INTO `counties` VALUES (369, 34, 'Güce');
INSERT INTO `counties` VALUES (370, 34, 'Keşap');
INSERT INTO `counties` VALUES (371, 34, 'Piraziz');
INSERT INTO `counties` VALUES (372, 34, 'Şebinkarahisar');
INSERT INTO `counties` VALUES (373, 34, 'Tirebolu');
INSERT INTO `counties` VALUES (374, 34, 'Yağlıdere');
INSERT INTO `counties` VALUES (375, 35, 'Kelkit');
INSERT INTO `counties` VALUES (376, 35, 'Köse');
INSERT INTO `counties` VALUES (377, 35, 'Kürtün');
INSERT INTO `counties` VALUES (378, 35, 'Şiran');
INSERT INTO `counties` VALUES (379, 35, 'Torul');
INSERT INTO `counties` VALUES (380, 36, 'Çukurca');
INSERT INTO `counties` VALUES (381, 36, 'Şemdinli');
INSERT INTO `counties` VALUES (382, 36, 'Yüksekova');
INSERT INTO `counties` VALUES (383, 37, 'Altınözü');
INSERT INTO `counties` VALUES (384, 37, 'Antakya');
INSERT INTO `counties` VALUES (385, 37, 'Arsuz');
INSERT INTO `counties` VALUES (386, 37, 'Belen');
INSERT INTO `counties` VALUES (387, 37, 'Defne');
INSERT INTO `counties` VALUES (388, 37, 'Dörtyol');
INSERT INTO `counties` VALUES (389, 37, 'Erzin');
INSERT INTO `counties` VALUES (390, 37, 'Hassa');
INSERT INTO `counties` VALUES (391, 37, 'İskenderun');
INSERT INTO `counties` VALUES (392, 37, 'Kırıkhan');
INSERT INTO `counties` VALUES (393, 37, 'Kumlu');
INSERT INTO `counties` VALUES (394, 37, 'Payas');
INSERT INTO `counties` VALUES (395, 37, 'Reyhanlı');
INSERT INTO `counties` VALUES (396, 37, 'Samandağ');
INSERT INTO `counties` VALUES (397, 37, 'Yayladağı');
INSERT INTO `counties` VALUES (398, 38, 'Aralık');
INSERT INTO `counties` VALUES (399, 38, 'Karakoyunlu');
INSERT INTO `counties` VALUES (400, 38, 'Tuzluca');
INSERT INTO `counties` VALUES (401, 39, 'Atabey');
INSERT INTO `counties` VALUES (402, 39, 'Eğirdir');
INSERT INTO `counties` VALUES (403, 39, 'Gelendost');
INSERT INTO `counties` VALUES (404, 39, 'Keçiborlu');
INSERT INTO `counties` VALUES (405, 39, 'Senirkent');
INSERT INTO `counties` VALUES (406, 39, 'Sütçüler');
INSERT INTO `counties` VALUES (407, 39, 'Şarkikaraağaç');
INSERT INTO `counties` VALUES (408, 39, 'Uluborlu');
INSERT INTO `counties` VALUES (409, 39, 'Yalvaç');
INSERT INTO `counties` VALUES (410, 39, 'Yenişarbademli');
INSERT INTO `counties` VALUES (411, 40, 'Adalar');
INSERT INTO `counties` VALUES (412, 40, 'Arnavutköy');
INSERT INTO `counties` VALUES (413, 40, 'Ataşehir');
INSERT INTO `counties` VALUES (414, 40, 'Avcılar');
INSERT INTO `counties` VALUES (415, 40, 'Bağcılar');
INSERT INTO `counties` VALUES (416, 40, 'Bahçelievler');
INSERT INTO `counties` VALUES (417, 40, 'Bakırköy');
INSERT INTO `counties` VALUES (418, 40, 'Başakşehir');
INSERT INTO `counties` VALUES (419, 40, 'Bayrampaşa');
INSERT INTO `counties` VALUES (420, 40, 'Beşiktaş');
INSERT INTO `counties` VALUES (421, 40, 'Beykoz');
INSERT INTO `counties` VALUES (422, 40, 'Beylikdüzü');
INSERT INTO `counties` VALUES (423, 40, 'Beyoğlu');
INSERT INTO `counties` VALUES (424, 40, 'Büyükçekmece');
INSERT INTO `counties` VALUES (425, 40, 'Çatalca');
INSERT INTO `counties` VALUES (426, 40, 'Çekmeköy');
INSERT INTO `counties` VALUES (427, 40, 'Esenler');
INSERT INTO `counties` VALUES (428, 40, 'Esenyurt');
INSERT INTO `counties` VALUES (429, 40, 'Eyüp');
INSERT INTO `counties` VALUES (430, 40, 'Fatih');
INSERT INTO `counties` VALUES (431, 40, 'Gaziosmanpaşa');
INSERT INTO `counties` VALUES (432, 40, 'Güngören');
INSERT INTO `counties` VALUES (433, 40, 'Kadıköy');
INSERT INTO `counties` VALUES (434, 40, 'Kağıthane');
INSERT INTO `counties` VALUES (435, 40, 'Kartal');
INSERT INTO `counties` VALUES (436, 40, 'Küçükçekmece');
INSERT INTO `counties` VALUES (437, 40, 'Maltepe');
INSERT INTO `counties` VALUES (438, 40, 'Pendik');
INSERT INTO `counties` VALUES (439, 40, 'Sancaktepe');
INSERT INTO `counties` VALUES (440, 40, 'Sarıyer');
INSERT INTO `counties` VALUES (441, 40, 'Silivri');
INSERT INTO `counties` VALUES (442, 40, 'Sultanbeyli');
INSERT INTO `counties` VALUES (443, 40, 'Sultangazi');
INSERT INTO `counties` VALUES (444, 40, 'Şile');
INSERT INTO `counties` VALUES (445, 40, 'Şişli');
INSERT INTO `counties` VALUES (446, 40, 'Tuzla');
INSERT INTO `counties` VALUES (447, 40, 'Ümraniye');
INSERT INTO `counties` VALUES (448, 40, 'Üsküdar');
INSERT INTO `counties` VALUES (449, 40, 'Zeytinburnu');
INSERT INTO `counties` VALUES (450, 41, 'Aliağa');
INSERT INTO `counties` VALUES (451, 41, 'Balçova');
INSERT INTO `counties` VALUES (452, 41, 'Bayındır');
INSERT INTO `counties` VALUES (453, 41, 'Bayraklı');
INSERT INTO `counties` VALUES (454, 41, 'Bergama');
INSERT INTO `counties` VALUES (455, 41, 'Beydağ');
INSERT INTO `counties` VALUES (456, 41, 'Bornova');
INSERT INTO `counties` VALUES (457, 41, 'Buca');
INSERT INTO `counties` VALUES (458, 41, 'Çeşme');
INSERT INTO `counties` VALUES (459, 41, 'Çiğli');
INSERT INTO `counties` VALUES (460, 41, 'Dikili');
INSERT INTO `counties` VALUES (461, 41, 'Foça');
INSERT INTO `counties` VALUES (462, 41, 'Gaziemir');
INSERT INTO `counties` VALUES (463, 41, 'Güzelbahçe');
INSERT INTO `counties` VALUES (464, 41, 'Karabağlar');
INSERT INTO `counties` VALUES (465, 41, 'Karaburun');
INSERT INTO `counties` VALUES (466, 41, 'Karşıyaka');
INSERT INTO `counties` VALUES (467, 41, 'Kemalpaşa');
INSERT INTO `counties` VALUES (468, 41, 'Kınık');
INSERT INTO `counties` VALUES (469, 41, 'Kiraz');
INSERT INTO `counties` VALUES (470, 41, 'Konak');
INSERT INTO `counties` VALUES (471, 41, 'Menderes');
INSERT INTO `counties` VALUES (472, 41, 'Menemen');
INSERT INTO `counties` VALUES (473, 41, 'Narlıdere');
INSERT INTO `counties` VALUES (474, 41, 'Ödemiş');
INSERT INTO `counties` VALUES (475, 41, 'Seferihisar');
INSERT INTO `counties` VALUES (476, 41, 'Selçuk');
INSERT INTO `counties` VALUES (477, 41, 'Tire');
INSERT INTO `counties` VALUES (478, 41, 'Torbalı');
INSERT INTO `counties` VALUES (479, 41, 'Urla');
INSERT INTO `counties` VALUES (480, 42, 'Afşin');
INSERT INTO `counties` VALUES (481, 42, 'Andırın');
INSERT INTO `counties` VALUES (482, 42, 'Çağlayancerit');
INSERT INTO `counties` VALUES (483, 42, 'Dulkadiroğlu');
INSERT INTO `counties` VALUES (484, 42, 'Ekinözü');
INSERT INTO `counties` VALUES (485, 42, 'Elbistan');
INSERT INTO `counties` VALUES (486, 42, 'Göksun');
INSERT INTO `counties` VALUES (487, 42, 'Nurhak');
INSERT INTO `counties` VALUES (488, 42, 'Onikişubat');
INSERT INTO `counties` VALUES (489, 42, 'Pazarcık');
INSERT INTO `counties` VALUES (490, 42, 'Türkoğlu');
INSERT INTO `counties` VALUES (491, 43, 'Eflani');
INSERT INTO `counties` VALUES (492, 43, 'Eskipazar');
INSERT INTO `counties` VALUES (493, 43, 'Ovacık');
INSERT INTO `counties` VALUES (494, 43, 'Safranbolu');
INSERT INTO `counties` VALUES (495, 44, 'Ayrancı');
INSERT INTO `counties` VALUES (496, 44, 'Başyayla');
INSERT INTO `counties` VALUES (497, 44, 'Ermenek');
INSERT INTO `counties` VALUES (498, 44, 'Kazımkarabekir');
INSERT INTO `counties` VALUES (499, 44, 'Sarıveliler');
INSERT INTO `counties` VALUES (500, 45, 'Akyaka');
INSERT INTO `counties` VALUES (501, 45, 'Arpaçay');
INSERT INTO `counties` VALUES (502, 45, 'Digor');
INSERT INTO `counties` VALUES (503, 45, 'Kağızman');
INSERT INTO `counties` VALUES (504, 45, 'Sarıkamış');
INSERT INTO `counties` VALUES (505, 45, 'Selim');
INSERT INTO `counties` VALUES (506, 45, 'Susuz');
INSERT INTO `counties` VALUES (507, 46, 'Abana');
INSERT INTO `counties` VALUES (508, 46, 'Ağlı');
INSERT INTO `counties` VALUES (509, 46, 'Araç');
INSERT INTO `counties` VALUES (510, 46, 'Azdavay');
INSERT INTO `counties` VALUES (511, 46, 'Cide');
INSERT INTO `counties` VALUES (512, 46, 'Çatalzeytin');
INSERT INTO `counties` VALUES (513, 46, 'Daday');
INSERT INTO `counties` VALUES (514, 46, 'Devrekani');
INSERT INTO `counties` VALUES (515, 46, 'Doğanyurt');
INSERT INTO `counties` VALUES (516, 46, 'Hanönü');
INSERT INTO `counties` VALUES (517, 46, 'İhsangazi');
INSERT INTO `counties` VALUES (518, 46, 'İnebolu');
INSERT INTO `counties` VALUES (519, 46, 'Küre');
INSERT INTO `counties` VALUES (520, 46, 'Pınarbaşı');
INSERT INTO `counties` VALUES (521, 46, 'Seydiler');
INSERT INTO `counties` VALUES (522, 46, 'Şenpazar');
INSERT INTO `counties` VALUES (523, 46, 'Taşköprü');
INSERT INTO `counties` VALUES (524, 46, 'Tosya');
INSERT INTO `counties` VALUES (525, 47, 'Akkışla');
INSERT INTO `counties` VALUES (526, 47, 'Bünyan');
INSERT INTO `counties` VALUES (527, 47, 'Develi');
INSERT INTO `counties` VALUES (528, 47, 'Felahiye');
INSERT INTO `counties` VALUES (529, 47, 'Hacılar');
INSERT INTO `counties` VALUES (530, 47, 'İncesu');
INSERT INTO `counties` VALUES (531, 47, 'Kocasinan');
INSERT INTO `counties` VALUES (532, 47, 'Melikgazi');
INSERT INTO `counties` VALUES (533, 47, 'Özvatan');
INSERT INTO `counties` VALUES (534, 47, 'Sarıoğlan');
INSERT INTO `counties` VALUES (535, 47, 'Sarız');
INSERT INTO `counties` VALUES (536, 47, 'Talas');
INSERT INTO `counties` VALUES (537, 47, 'Tomarza');
INSERT INTO `counties` VALUES (538, 47, 'Yahyalı');
INSERT INTO `counties` VALUES (539, 47, 'Yeşilhisar');
INSERT INTO `counties` VALUES (540, 48, 'Bahşili');
INSERT INTO `counties` VALUES (541, 48, 'Balışeyh');
INSERT INTO `counties` VALUES (542, 48, 'Çelebi');
INSERT INTO `counties` VALUES (543, 48, 'Delice');
INSERT INTO `counties` VALUES (544, 48, 'Karakeçili');
INSERT INTO `counties` VALUES (545, 48, 'Keskin');
INSERT INTO `counties` VALUES (546, 48, 'Sulakyurt');
INSERT INTO `counties` VALUES (547, 48, 'Yahşihan');
INSERT INTO `counties` VALUES (548, 49, 'Babaeski');
INSERT INTO `counties` VALUES (549, 49, 'Demirköy');
INSERT INTO `counties` VALUES (550, 49, 'Kofçaz');
INSERT INTO `counties` VALUES (551, 49, 'Lüleburgaz');
INSERT INTO `counties` VALUES (552, 49, 'Pehlivanköy');
INSERT INTO `counties` VALUES (553, 49, 'Pınarhisar');
INSERT INTO `counties` VALUES (554, 49, 'Vize');
INSERT INTO `counties` VALUES (555, 50, 'Akçakent');
INSERT INTO `counties` VALUES (556, 50, 'Akpınar');
INSERT INTO `counties` VALUES (557, 50, 'Boztepe');
INSERT INTO `counties` VALUES (558, 50, 'Çiçekdağı');
INSERT INTO `counties` VALUES (559, 50, 'Kaman');
INSERT INTO `counties` VALUES (560, 50, 'Mucur');
INSERT INTO `counties` VALUES (561, 51, 'Elbeyli');
INSERT INTO `counties` VALUES (562, 51, 'Musabeyli');
INSERT INTO `counties` VALUES (563, 51, 'Polateli');
INSERT INTO `counties` VALUES (564, 52, 'Başiskele');
INSERT INTO `counties` VALUES (565, 52, 'Çayırova');
INSERT INTO `counties` VALUES (566, 52, 'Darıca');
INSERT INTO `counties` VALUES (567, 52, 'Derince');
INSERT INTO `counties` VALUES (568, 52, 'Dilovası');
INSERT INTO `counties` VALUES (569, 52, 'Gebze');
INSERT INTO `counties` VALUES (570, 52, 'Gölcük');
INSERT INTO `counties` VALUES (571, 52, 'İzmit');
INSERT INTO `counties` VALUES (572, 52, 'Kandıra');
INSERT INTO `counties` VALUES (573, 52, 'Karamürsel');
INSERT INTO `counties` VALUES (574, 52, 'Kartepe');
INSERT INTO `counties` VALUES (575, 52, 'Körfez');
INSERT INTO `counties` VALUES (576, 53, 'Ahırlı');
INSERT INTO `counties` VALUES (577, 53, 'Akören');
INSERT INTO `counties` VALUES (578, 53, 'Akşehir');
INSERT INTO `counties` VALUES (579, 53, 'Altınekin');
INSERT INTO `counties` VALUES (580, 53, 'Beyşehir');
INSERT INTO `counties` VALUES (581, 53, 'Bozkır');
INSERT INTO `counties` VALUES (582, 53, 'Cihanbeyli');
INSERT INTO `counties` VALUES (583, 53, 'Çeltik');
INSERT INTO `counties` VALUES (584, 53, 'Çumra');
INSERT INTO `counties` VALUES (585, 53, 'Derbent');
INSERT INTO `counties` VALUES (586, 53, 'Derebucak');
INSERT INTO `counties` VALUES (587, 53, 'Doğanhisar');
INSERT INTO `counties` VALUES (588, 53, 'Emirgazi');
INSERT INTO `counties` VALUES (589, 53, 'Ereğli');
INSERT INTO `counties` VALUES (590, 53, 'Güneysınır');
INSERT INTO `counties` VALUES (591, 53, 'Hadim');
INSERT INTO `counties` VALUES (592, 53, 'Halkapınar');
INSERT INTO `counties` VALUES (593, 53, 'Hüyük');
INSERT INTO `counties` VALUES (594, 53, 'Ilgın');
INSERT INTO `counties` VALUES (595, 53, 'Kadınhanı');
INSERT INTO `counties` VALUES (596, 53, 'Karapınar');
INSERT INTO `counties` VALUES (597, 53, 'Karatay');
INSERT INTO `counties` VALUES (598, 53, 'Kulu');
INSERT INTO `counties` VALUES (599, 53, 'Meram');
INSERT INTO `counties` VALUES (600, 53, 'Sarayönü');
INSERT INTO `counties` VALUES (601, 53, 'Selçuklu');
INSERT INTO `counties` VALUES (602, 53, 'Seydişehir');
INSERT INTO `counties` VALUES (603, 53, 'Taşkent');
INSERT INTO `counties` VALUES (604, 53, 'Tuzlukçu');
INSERT INTO `counties` VALUES (605, 53, 'Yalıhüyük');
INSERT INTO `counties` VALUES (606, 53, 'Yunak');
INSERT INTO `counties` VALUES (607, 54, 'Altıntaş');
INSERT INTO `counties` VALUES (608, 54, 'Aslanapa');
INSERT INTO `counties` VALUES (609, 54, 'Çavdarhisar');
INSERT INTO `counties` VALUES (610, 54, 'Domaniç');
INSERT INTO `counties` VALUES (611, 54, 'Dumlupınar');
INSERT INTO `counties` VALUES (612, 54, 'Emet');
INSERT INTO `counties` VALUES (613, 54, 'Gediz');
INSERT INTO `counties` VALUES (614, 54, 'Hisarcık');
INSERT INTO `counties` VALUES (615, 54, 'Pazarlar');
INSERT INTO `counties` VALUES (616, 54, 'Simav');
INSERT INTO `counties` VALUES (617, 54, 'Şaphane');
INSERT INTO `counties` VALUES (618, 54, 'Tavşanlı');
INSERT INTO `counties` VALUES (619, 55, 'Akçadağ');
INSERT INTO `counties` VALUES (620, 55, 'Arapgir');
INSERT INTO `counties` VALUES (621, 55, 'Arguvan');
INSERT INTO `counties` VALUES (622, 55, 'Battalgazi');
INSERT INTO `counties` VALUES (623, 55, 'Darende');
INSERT INTO `counties` VALUES (624, 55, 'Doğanşehir');
INSERT INTO `counties` VALUES (625, 55, 'Doğanyol');
INSERT INTO `counties` VALUES (626, 55, 'Hekimhan');
INSERT INTO `counties` VALUES (627, 55, 'Kuluncak');
INSERT INTO `counties` VALUES (628, 55, 'Pütürge');
INSERT INTO `counties` VALUES (629, 55, 'Yazıhan');
INSERT INTO `counties` VALUES (630, 55, 'Yeşilyurt');
INSERT INTO `counties` VALUES (631, 56, 'Ahmetli');
INSERT INTO `counties` VALUES (632, 56, 'Akhisar');
INSERT INTO `counties` VALUES (633, 56, 'Alaşehir');
INSERT INTO `counties` VALUES (634, 56, 'Demirci');
INSERT INTO `counties` VALUES (635, 56, 'Gölmarmara');
INSERT INTO `counties` VALUES (636, 56, 'Gördes');
INSERT INTO `counties` VALUES (637, 56, 'Kırkağaç');
INSERT INTO `counties` VALUES (638, 56, 'Köprübaşı');
INSERT INTO `counties` VALUES (639, 56, 'Kula');
INSERT INTO `counties` VALUES (640, 56, 'Salihli');
INSERT INTO `counties` VALUES (641, 56, 'Sarıgöl');
INSERT INTO `counties` VALUES (642, 56, 'Saruhanlı');
INSERT INTO `counties` VALUES (643, 56, 'Selendi');
INSERT INTO `counties` VALUES (644, 56, 'Soma');
INSERT INTO `counties` VALUES (645, 56, 'Şehzadeler');
INSERT INTO `counties` VALUES (646, 56, 'Turgutlu');
INSERT INTO `counties` VALUES (647, 56, 'Yunusemre');
INSERT INTO `counties` VALUES (648, 57, 'Artuklu');
INSERT INTO `counties` VALUES (649, 57, 'Dargeçit');
INSERT INTO `counties` VALUES (650, 57, 'Derik');
INSERT INTO `counties` VALUES (651, 57, 'Kızıltepe');
INSERT INTO `counties` VALUES (652, 57, 'Mazıdağı');
INSERT INTO `counties` VALUES (653, 57, 'Midyat');
INSERT INTO `counties` VALUES (654, 57, 'Nusaybin');
INSERT INTO `counties` VALUES (655, 57, 'Ömerli');
INSERT INTO `counties` VALUES (656, 57, 'Savur');
INSERT INTO `counties` VALUES (657, 57, 'Yeşilli');
INSERT INTO `counties` VALUES (658, 58, 'Akdeniz');
INSERT INTO `counties` VALUES (659, 58, 'Anamur');
INSERT INTO `counties` VALUES (660, 58, 'Aydıncık');
INSERT INTO `counties` VALUES (661, 58, 'Bozyazı');
INSERT INTO `counties` VALUES (662, 58, 'Çamlıyayla');
INSERT INTO `counties` VALUES (663, 58, 'Erdemli');
INSERT INTO `counties` VALUES (664, 58, 'Gülnar');
INSERT INTO `counties` VALUES (665, 58, 'Mezitli');
INSERT INTO `counties` VALUES (666, 58, 'Mut');
INSERT INTO `counties` VALUES (667, 58, 'Silifke');
INSERT INTO `counties` VALUES (668, 58, 'Tarsus');
INSERT INTO `counties` VALUES (669, 58, 'Toroslar');
INSERT INTO `counties` VALUES (670, 59, 'Bodrum');
INSERT INTO `counties` VALUES (671, 59, 'Dalaman');
INSERT INTO `counties` VALUES (672, 59, 'Datça');
INSERT INTO `counties` VALUES (673, 59, 'Fethiye');
INSERT INTO `counties` VALUES (674, 59, 'Kavaklıdere');
INSERT INTO `counties` VALUES (675, 59, 'Köyceğiz');
INSERT INTO `counties` VALUES (676, 59, 'Marmaris');
INSERT INTO `counties` VALUES (677, 59, 'Menteşe');
INSERT INTO `counties` VALUES (678, 59, 'Milas');
INSERT INTO `counties` VALUES (679, 59, 'Ortaca');
INSERT INTO `counties` VALUES (680, 59, 'Seydikemer');
INSERT INTO `counties` VALUES (681, 59, 'Ula');
INSERT INTO `counties` VALUES (682, 59, 'Yatağan');
INSERT INTO `counties` VALUES (683, 60, 'Bulanık');
INSERT INTO `counties` VALUES (684, 60, 'Hasköy');
INSERT INTO `counties` VALUES (685, 60, 'Korkut');
INSERT INTO `counties` VALUES (686, 60, 'Malazgirt');
INSERT INTO `counties` VALUES (687, 60, 'Varto');
INSERT INTO `counties` VALUES (688, 61, 'Acıgöl');
INSERT INTO `counties` VALUES (689, 61, 'Avanos');
INSERT INTO `counties` VALUES (690, 61, 'Derinkuyu');
INSERT INTO `counties` VALUES (691, 61, 'Gülşehir');
INSERT INTO `counties` VALUES (692, 61, 'Hacıbektaş');
INSERT INTO `counties` VALUES (693, 61, 'Kozaklı');
INSERT INTO `counties` VALUES (694, 61, 'Ürgüp');
INSERT INTO `counties` VALUES (695, 62, 'Altunhisar');
INSERT INTO `counties` VALUES (696, 62, 'Bor');
INSERT INTO `counties` VALUES (697, 62, 'Çamardı');
INSERT INTO `counties` VALUES (698, 62, 'Çiftlik');
INSERT INTO `counties` VALUES (699, 62, 'Ulukışla');
INSERT INTO `counties` VALUES (700, 63, 'Akkuş');
INSERT INTO `counties` VALUES (701, 63, 'Altınordu');
INSERT INTO `counties` VALUES (702, 63, 'Aybastı');
INSERT INTO `counties` VALUES (703, 63, 'Çamaş');
INSERT INTO `counties` VALUES (704, 63, 'Çatalpınar');
INSERT INTO `counties` VALUES (705, 63, 'Çaybaşı');
INSERT INTO `counties` VALUES (706, 63, 'Fatsa');
INSERT INTO `counties` VALUES (707, 63, 'Gölköy');
INSERT INTO `counties` VALUES (708, 63, 'Gülyalı');
INSERT INTO `counties` VALUES (709, 63, 'Gürgentepe');
INSERT INTO `counties` VALUES (710, 63, 'İkizce');
INSERT INTO `counties` VALUES (711, 63, 'Kabadüz');
INSERT INTO `counties` VALUES (712, 63, 'Kabataş');
INSERT INTO `counties` VALUES (713, 63, 'Korgan');
INSERT INTO `counties` VALUES (714, 63, 'Kumru');
INSERT INTO `counties` VALUES (715, 63, 'Mesudiye');
INSERT INTO `counties` VALUES (716, 63, 'Perşembe');
INSERT INTO `counties` VALUES (717, 63, 'Ulubey');
INSERT INTO `counties` VALUES (718, 63, 'Ünye');
INSERT INTO `counties` VALUES (719, 64, 'Bahçe');
INSERT INTO `counties` VALUES (720, 64, 'Düziçi');
INSERT INTO `counties` VALUES (721, 64, 'Hasanbeyli');
INSERT INTO `counties` VALUES (722, 64, 'Kadirli');
INSERT INTO `counties` VALUES (723, 64, 'Sumbas');
INSERT INTO `counties` VALUES (724, 64, 'Toprakkale');
INSERT INTO `counties` VALUES (725, 65, 'Ardeşen');
INSERT INTO `counties` VALUES (726, 65, 'Çamlıhemşin');
INSERT INTO `counties` VALUES (727, 65, 'Çayeli');
INSERT INTO `counties` VALUES (728, 65, 'Derepazarı');
INSERT INTO `counties` VALUES (729, 65, 'Fındıklı');
INSERT INTO `counties` VALUES (730, 65, 'Güneysu');
INSERT INTO `counties` VALUES (731, 65, 'Hemşin');
INSERT INTO `counties` VALUES (732, 65, 'İkizdere');
INSERT INTO `counties` VALUES (733, 65, 'İyidere');
INSERT INTO `counties` VALUES (734, 65, 'Kalkandere');
INSERT INTO `counties` VALUES (735, 65, 'Pazar');
INSERT INTO `counties` VALUES (736, 66, 'Adapazarı');
INSERT INTO `counties` VALUES (737, 66, 'Akyazı');
INSERT INTO `counties` VALUES (738, 66, 'Arifiye');
INSERT INTO `counties` VALUES (739, 66, 'Erenler');
INSERT INTO `counties` VALUES (740, 66, 'Ferizli');
INSERT INTO `counties` VALUES (741, 66, 'Geyve');
INSERT INTO `counties` VALUES (742, 66, 'Hendek');
INSERT INTO `counties` VALUES (743, 66, 'Karapürçek');
INSERT INTO `counties` VALUES (744, 66, 'Karasu');
INSERT INTO `counties` VALUES (745, 66, 'Kaynarca');
INSERT INTO `counties` VALUES (746, 66, 'Kocaali');
INSERT INTO `counties` VALUES (747, 66, 'Pamukova');
INSERT INTO `counties` VALUES (748, 66, 'Sapanca');
INSERT INTO `counties` VALUES (749, 66, 'Serdivan');
INSERT INTO `counties` VALUES (750, 66, 'Söğütlü');
INSERT INTO `counties` VALUES (751, 66, 'Taraklı');
INSERT INTO `counties` VALUES (752, 67, '42143');
INSERT INTO `counties` VALUES (753, 67, 'Alaçam');
INSERT INTO `counties` VALUES (754, 67, 'Asarcık');
INSERT INTO `counties` VALUES (755, 67, 'Atakum');
INSERT INTO `counties` VALUES (756, 67, 'Bafra');
INSERT INTO `counties` VALUES (757, 67, 'Canik');
INSERT INTO `counties` VALUES (758, 67, 'Çarşamba');
INSERT INTO `counties` VALUES (759, 67, 'Havza');
INSERT INTO `counties` VALUES (760, 67, 'İlkadım');
INSERT INTO `counties` VALUES (761, 67, 'Kavak');
INSERT INTO `counties` VALUES (762, 67, 'Ladik');
INSERT INTO `counties` VALUES (763, 67, 'Salıpazarı');
INSERT INTO `counties` VALUES (764, 67, 'Tekkeköy');
INSERT INTO `counties` VALUES (765, 67, 'Terme');
INSERT INTO `counties` VALUES (766, 67, 'Vezirköprü');
INSERT INTO `counties` VALUES (767, 67, 'Yakakent');
INSERT INTO `counties` VALUES (768, 68, 'Baykan');
INSERT INTO `counties` VALUES (769, 68, 'Eruh');
INSERT INTO `counties` VALUES (770, 68, 'Kurtalan');
INSERT INTO `counties` VALUES (771, 68, 'Pervari');
INSERT INTO `counties` VALUES (772, 68, 'Şirvan');
INSERT INTO `counties` VALUES (773, 68, 'Tillo');
INSERT INTO `counties` VALUES (774, 69, 'Ayancık');
INSERT INTO `counties` VALUES (775, 69, 'Boyabat');
INSERT INTO `counties` VALUES (776, 69, 'Dikmen');
INSERT INTO `counties` VALUES (777, 69, 'Durağan');
INSERT INTO `counties` VALUES (778, 69, 'Erfelek');
INSERT INTO `counties` VALUES (779, 69, 'Gerze');
INSERT INTO `counties` VALUES (780, 69, 'Saraydüzü');
INSERT INTO `counties` VALUES (781, 69, 'Türkeli');
INSERT INTO `counties` VALUES (782, 70, 'Akıncılar');
INSERT INTO `counties` VALUES (783, 70, 'Divriği');
INSERT INTO `counties` VALUES (784, 70, 'Doğanşar');
INSERT INTO `counties` VALUES (785, 70, 'Gemerek');
INSERT INTO `counties` VALUES (786, 70, 'Gölova');
INSERT INTO `counties` VALUES (787, 70, 'Gürün');
INSERT INTO `counties` VALUES (788, 70, 'Hafik');
INSERT INTO `counties` VALUES (789, 70, 'İmranlı');
INSERT INTO `counties` VALUES (790, 70, 'Kangal');
INSERT INTO `counties` VALUES (791, 70, 'Koyulhisar');
INSERT INTO `counties` VALUES (792, 70, 'Suşehri');
INSERT INTO `counties` VALUES (793, 70, 'Şarkışla');
INSERT INTO `counties` VALUES (794, 70, 'Ulaş');
INSERT INTO `counties` VALUES (795, 70, 'Yıldızeli');
INSERT INTO `counties` VALUES (796, 70, 'Zara');
INSERT INTO `counties` VALUES (797, 71, 'Akçakale');
INSERT INTO `counties` VALUES (798, 71, 'Birecik');
INSERT INTO `counties` VALUES (799, 71, 'Bozova');
INSERT INTO `counties` VALUES (800, 71, 'Ceylanpınar');
INSERT INTO `counties` VALUES (801, 71, 'Eyyübiye');
INSERT INTO `counties` VALUES (802, 71, 'Halfeti');
INSERT INTO `counties` VALUES (803, 71, 'Haliliye');
INSERT INTO `counties` VALUES (804, 71, 'Harran');
INSERT INTO `counties` VALUES (805, 71, 'Hilvan');
INSERT INTO `counties` VALUES (806, 71, 'Karaköprü');
INSERT INTO `counties` VALUES (807, 71, 'Siverek');
INSERT INTO `counties` VALUES (808, 71, 'Suruç');
INSERT INTO `counties` VALUES (809, 71, 'Viranşehir');
INSERT INTO `counties` VALUES (810, 72, 'Beytüşşebap');
INSERT INTO `counties` VALUES (811, 72, 'Cizre');
INSERT INTO `counties` VALUES (812, 72, 'Güçlükonak');
INSERT INTO `counties` VALUES (813, 72, 'İdil');
INSERT INTO `counties` VALUES (814, 72, 'Silopi');
INSERT INTO `counties` VALUES (815, 72, 'Uludere');
INSERT INTO `counties` VALUES (816, 73, 'Çerkezköy');
INSERT INTO `counties` VALUES (817, 73, 'Çorlu');
INSERT INTO `counties` VALUES (818, 73, 'Ergene');
INSERT INTO `counties` VALUES (819, 73, 'Hayrabolu');
INSERT INTO `counties` VALUES (820, 73, 'Kapaklı');
INSERT INTO `counties` VALUES (821, 73, 'Malkara');
INSERT INTO `counties` VALUES (822, 73, 'Marmaraereğlisi');
INSERT INTO `counties` VALUES (823, 73, 'Muratlı');
INSERT INTO `counties` VALUES (824, 73, 'Saray');
INSERT INTO `counties` VALUES (825, 73, 'Süleymanpaşa');
INSERT INTO `counties` VALUES (826, 73, 'Şarköy');
INSERT INTO `counties` VALUES (827, 74, 'Almus');
INSERT INTO `counties` VALUES (828, 74, 'Artova');
INSERT INTO `counties` VALUES (829, 74, 'Başçiftlik');
INSERT INTO `counties` VALUES (830, 74, 'Erbaa');
INSERT INTO `counties` VALUES (831, 74, 'Niksar');
INSERT INTO `counties` VALUES (832, 74, 'Reşadiye');
INSERT INTO `counties` VALUES (833, 74, 'Sulusaray');
INSERT INTO `counties` VALUES (834, 74, 'Turhal');
INSERT INTO `counties` VALUES (835, 74, 'Zile');
INSERT INTO `counties` VALUES (836, 75, 'Akçaabat');
INSERT INTO `counties` VALUES (837, 75, 'Araklı');
INSERT INTO `counties` VALUES (838, 75, 'Arsin');
INSERT INTO `counties` VALUES (839, 75, 'Beşikdüzü');
INSERT INTO `counties` VALUES (840, 75, 'Çarşıbaşı');
INSERT INTO `counties` VALUES (841, 75, 'Çaykara');
INSERT INTO `counties` VALUES (842, 75, 'Dernekpazarı');
INSERT INTO `counties` VALUES (843, 75, 'Düzköy');
INSERT INTO `counties` VALUES (844, 75, 'Hayrat');
INSERT INTO `counties` VALUES (845, 75, 'Maçka');
INSERT INTO `counties` VALUES (846, 75, 'Of');
INSERT INTO `counties` VALUES (847, 75, 'Ortahisar');
INSERT INTO `counties` VALUES (848, 75, 'Sürmene');
INSERT INTO `counties` VALUES (849, 75, 'Şalpazarı');
INSERT INTO `counties` VALUES (850, 75, 'Tonya');
INSERT INTO `counties` VALUES (851, 75, 'Vakfıkebir');
INSERT INTO `counties` VALUES (852, 75, 'Yomra');
INSERT INTO `counties` VALUES (853, 76, 'Çemişgezek');
INSERT INTO `counties` VALUES (854, 76, 'Hozat');
INSERT INTO `counties` VALUES (855, 76, 'Mazgirt');
INSERT INTO `counties` VALUES (856, 76, 'Nazımiye');
INSERT INTO `counties` VALUES (857, 76, 'Pertek');
INSERT INTO `counties` VALUES (858, 76, 'Pülümür');
INSERT INTO `counties` VALUES (859, 77, 'Banaz');
INSERT INTO `counties` VALUES (860, 77, 'Eşme');
INSERT INTO `counties` VALUES (861, 77, 'Karahallı');
INSERT INTO `counties` VALUES (862, 77, 'Sivaslı');
INSERT INTO `counties` VALUES (863, 78, 'Bahçesaray');
INSERT INTO `counties` VALUES (864, 78, 'Başkale');
INSERT INTO `counties` VALUES (865, 78, 'Çaldıran');
INSERT INTO `counties` VALUES (866, 78, 'Çatak');
INSERT INTO `counties` VALUES (867, 78, 'Erciş');
INSERT INTO `counties` VALUES (868, 78, 'Gevaş');
INSERT INTO `counties` VALUES (869, 78, 'Gürpınar');
INSERT INTO `counties` VALUES (870, 78, 'İpekyolu');
INSERT INTO `counties` VALUES (871, 78, 'Muradiye');
INSERT INTO `counties` VALUES (872, 78, 'Özalp');
INSERT INTO `counties` VALUES (873, 78, 'Tuşba');
INSERT INTO `counties` VALUES (874, 79, 'Altınova');
INSERT INTO `counties` VALUES (875, 79, 'Armutlu');
INSERT INTO `counties` VALUES (876, 79, 'Çınarcık');
INSERT INTO `counties` VALUES (877, 79, 'Çiftlikköy');
INSERT INTO `counties` VALUES (878, 79, 'Termal');
INSERT INTO `counties` VALUES (879, 80, 'Akdağmadeni');
INSERT INTO `counties` VALUES (880, 80, 'Boğazlıyan');
INSERT INTO `counties` VALUES (881, 80, 'Çandır');
INSERT INTO `counties` VALUES (882, 80, 'Çayıralan');
INSERT INTO `counties` VALUES (883, 80, 'Çekerek');
INSERT INTO `counties` VALUES (884, 80, 'Kadışehri');
INSERT INTO `counties` VALUES (885, 80, 'Saraykent');
INSERT INTO `counties` VALUES (886, 80, 'Sarıkaya');
INSERT INTO `counties` VALUES (887, 80, 'Sorgun');
INSERT INTO `counties` VALUES (888, 80, 'Şefaatli');
INSERT INTO `counties` VALUES (889, 80, 'Yenifakılı');
INSERT INTO `counties` VALUES (890, 80, 'Yerköy');
INSERT INTO `counties` VALUES (891, 81, 'Alaplı');
INSERT INTO `counties` VALUES (892, 81, 'Çaycuma');
INSERT INTO `counties` VALUES (893, 81, 'Devrek');
INSERT INTO `counties` VALUES (894, 81, 'Gökçebey');
INSERT INTO `counties` VALUES (895, 81, 'Kilimli');
INSERT INTO `counties` VALUES (896, 81, 'Kozlu');

-- ----------------------------
-- Table structure for exam_questions
-- ----------------------------
DROP TABLE IF EXISTS `exam_questions`;
CREATE TABLE `exam_questions`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `exam_id` bigint(20) UNSIGNED NOT NULL,
  `question_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exam_questions_exam_id_foreign`(`exam_id`) USING BTREE,
  CONSTRAINT `exam_questions_exam_id_foreign` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 62 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of exam_questions
-- ----------------------------
INSERT INTO `exam_questions` VALUES (1, 1, 2, NULL, NULL);
INSERT INTO `exam_questions` VALUES (2, 1, 19, NULL, NULL);
INSERT INTO `exam_questions` VALUES (3, 1, 5, NULL, NULL);
INSERT INTO `exam_questions` VALUES (4, 1, 15, NULL, NULL);
INSERT INTO `exam_questions` VALUES (5, 1, 17, NULL, NULL);
INSERT INTO `exam_questions` VALUES (6, 1, 6, NULL, NULL);
INSERT INTO `exam_questions` VALUES (7, 1, 10, NULL, NULL);
INSERT INTO `exam_questions` VALUES (8, 1, 3, NULL, NULL);
INSERT INTO `exam_questions` VALUES (9, 1, 4, NULL, NULL);
INSERT INTO `exam_questions` VALUES (10, 1, 7, NULL, NULL);
INSERT INTO `exam_questions` VALUES (11, 1, 20, NULL, NULL);
INSERT INTO `exam_questions` VALUES (12, 1, 18, NULL, NULL);
INSERT INTO `exam_questions` VALUES (13, 1, 9, NULL, NULL);
INSERT INTO `exam_questions` VALUES (14, 1, 1, NULL, NULL);
INSERT INTO `exam_questions` VALUES (15, 1, 11, NULL, NULL);
INSERT INTO `exam_questions` VALUES (16, 1, 16, NULL, NULL);
INSERT INTO `exam_questions` VALUES (17, 1, 13, NULL, NULL);
INSERT INTO `exam_questions` VALUES (18, 2, 20, NULL, NULL);
INSERT INTO `exam_questions` VALUES (19, 2, 2, NULL, NULL);
INSERT INTO `exam_questions` VALUES (20, 2, 16, NULL, NULL);
INSERT INTO `exam_questions` VALUES (21, 2, 1, NULL, NULL);
INSERT INTO `exam_questions` VALUES (22, 2, 14, NULL, NULL);
INSERT INTO `exam_questions` VALUES (23, 2, 7, NULL, NULL);
INSERT INTO `exam_questions` VALUES (24, 2, 19, NULL, NULL);
INSERT INTO `exam_questions` VALUES (25, 2, 8, NULL, NULL);
INSERT INTO `exam_questions` VALUES (26, 2, 9, NULL, NULL);
INSERT INTO `exam_questions` VALUES (27, 2, 13, NULL, NULL);
INSERT INTO `exam_questions` VALUES (28, 2, 17, NULL, NULL);
INSERT INTO `exam_questions` VALUES (29, 2, 10, NULL, NULL);
INSERT INTO `exam_questions` VALUES (30, 2, 6, NULL, NULL);
INSERT INTO `exam_questions` VALUES (31, 2, 3, NULL, NULL);
INSERT INTO `exam_questions` VALUES (32, 2, 18, NULL, NULL);
INSERT INTO `exam_questions` VALUES (33, 3, 4, NULL, NULL);
INSERT INTO `exam_questions` VALUES (34, 3, 19, NULL, NULL);
INSERT INTO `exam_questions` VALUES (35, 3, 15, NULL, NULL);
INSERT INTO `exam_questions` VALUES (36, 3, 6, NULL, NULL);
INSERT INTO `exam_questions` VALUES (37, 3, 3, NULL, NULL);
INSERT INTO `exam_questions` VALUES (38, 3, 10, NULL, NULL);
INSERT INTO `exam_questions` VALUES (39, 3, 2, NULL, NULL);
INSERT INTO `exam_questions` VALUES (40, 4, 15, NULL, NULL);
INSERT INTO `exam_questions` VALUES (41, 4, 12, NULL, NULL);
INSERT INTO `exam_questions` VALUES (42, 4, 19, NULL, NULL);
INSERT INTO `exam_questions` VALUES (43, 4, 20, NULL, NULL);
INSERT INTO `exam_questions` VALUES (44, 5, 7, NULL, NULL);
INSERT INTO `exam_questions` VALUES (45, 5, 10, NULL, NULL);
INSERT INTO `exam_questions` VALUES (46, 5, 19, NULL, NULL);
INSERT INTO `exam_questions` VALUES (47, 5, 1, NULL, NULL);
INSERT INTO `exam_questions` VALUES (48, 5, 8, NULL, NULL);
INSERT INTO `exam_questions` VALUES (49, 5, 17, NULL, NULL);
INSERT INTO `exam_questions` VALUES (50, 5, 12, NULL, NULL);
INSERT INTO `exam_questions` VALUES (51, 5, 11, NULL, NULL);
INSERT INTO `exam_questions` VALUES (52, 5, 15, NULL, NULL);
INSERT INTO `exam_questions` VALUES (53, 5, 5, NULL, NULL);
INSERT INTO `exam_questions` VALUES (54, 5, 13, NULL, NULL);
INSERT INTO `exam_questions` VALUES (55, 5, 20, NULL, NULL);
INSERT INTO `exam_questions` VALUES (56, 5, 18, NULL, NULL);
INSERT INTO `exam_questions` VALUES (57, 5, 9, NULL, NULL);
INSERT INTO `exam_questions` VALUES (58, 5, 14, NULL, NULL);
INSERT INTO `exam_questions` VALUES (59, 5, 16, NULL, NULL);
INSERT INTO `exam_questions` VALUES (60, 5, 2, NULL, NULL);
INSERT INTO `exam_questions` VALUES (61, 5, 6, NULL, NULL);

-- ----------------------------
-- Table structure for exam_reports
-- ----------------------------
DROP TABLE IF EXISTS `exam_reports`;
CREATE TABLE `exam_reports`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `exam_id` bigint(20) UNSIGNED NOT NULL,
  `start_time` datetime(0) NOT NULL,
  `end_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for exams
-- ----------------------------
DROP TABLE IF EXISTS `exams`;
CREATE TABLE `exams`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_manual` tinyint(1) NOT NULL,
  `start_time` datetime(0) NULL DEFAULT NULL,
  `end_time` datetime(0) NULL DEFAULT NULL,
  `socket_start` tinyint(1) NOT NULL DEFAULT 0,
  `type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `public` tinyint(1) NOT NULL DEFAULT 0,
  `question_shuffle` tinyint(1) NOT NULL DEFAULT 0,
  `answer_shuffle` tinyint(1) NOT NULL DEFAULT 0,
  `code` varchar(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `theme` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of exams
-- ----------------------------
INSERT INTO `exams` VALUES (1, 1, 'sed', 1, NULL, NULL, 0, 'senkron', 1, 0, 0, 'jua37rz', 'default', '2019-08-09 11:10:21', '2019-08-09 11:10:21', NULL);
INSERT INTO `exams` VALUES (2, 1, 'corrupti', 1, NULL, NULL, 0, 'asenkron', 1, 0, 0, 'ne2nc9i', 'default', '2019-08-09 11:10:21', '2019-08-09 11:10:21', NULL);
INSERT INTO `exams` VALUES (3, 1, 'autem', 1, NULL, NULL, 0, 'senkron', 1, 0, 0, 'xyislje', 'default', '2019-08-09 11:10:21', '2019-08-09 11:10:21', NULL);
INSERT INTO `exams` VALUES (4, 1, 'consequuntur', 1, NULL, NULL, 0, 'senkron', 1, 0, 0, '8rl8gbw', 'default', '2019-08-09 11:10:21', '2019-08-09 11:10:21', NULL);
INSERT INTO `exams` VALUES (5, 1, 'sed', 1, NULL, NULL, 0, 'asenkron', 1, 0, 0, 'tixsfrr', 'default', '2019-08-09 11:10:21', '2019-08-09 11:10:21', NULL);

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_08_06_074124_create_roles_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_08_06_074330_create_categories_table', 1);
INSERT INTO `migrations` VALUES (5, '2019_08_06_074401_create_questions_table', 1);
INSERT INTO `migrations` VALUES (6, '2019_08_06_074409_create_answers_table', 1);
INSERT INTO `migrations` VALUES (7, '2019_08_06_074418_create_exams_table', 1);
INSERT INTO `migrations` VALUES (8, '2019_08_06_074431_create_exam_questions_table', 1);
INSERT INTO `migrations` VALUES (9, '2019_08_06_074442_create_exam_reports_table', 1);
INSERT INTO `migrations` VALUES (10, '2019_08_06_074453_create_question_reports_table', 1);
INSERT INTO `migrations` VALUES (11, '2019_08_06_074648_create_cities_table', 1);
INSERT INTO `migrations` VALUES (12, '2019_08_06_075041_create_counties_table', 1);
INSERT INTO `migrations` VALUES (13, '2019_08_06_075821_create_users_foreign_keys_table', 1);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for question_reports
-- ----------------------------
DROP TABLE IF EXISTS `question_reports`;
CREATE TABLE `question_reports`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `exam_id` bigint(20) UNSIGNED NOT NULL,
  `question_id` bigint(20) UNSIGNED NOT NULL,
  `answer_id` bigint(20) UNSIGNED NOT NULL,
  `answer_string` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `start_time` datetime(0) NOT NULL,
  `end_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for questions
-- ----------------------------
DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `time` int(10) UNSIGNED NOT NULL,
  `type` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of questions
-- ----------------------------
INSERT INTO `questions` VALUES (1, 1, 0, 'For some minutes it seemed quite natural); but when the tide rises and sharks are around, His voice has a timid voice at her for a great crowd assembled about them--all sorts of little Alice and all.', '', 16, 'bosluk', '2019-08-09 11:10:20', '2019-08-09 11:10:20', NULL);
INSERT INTO `questions` VALUES (2, 1, 0, 'And the Gryphon in an agony of terror. \'Oh, there goes his PRECIOUS nose\'; as an explanation; \'I\'ve none of them can explain it,\' said Alice. \'Exactly so,\' said Alice. \'Why, you don\'t like it, yer.', '', 47, 'coktan', '2019-08-09 11:10:20', '2019-08-09 11:10:20', NULL);
INSERT INTO `questions` VALUES (3, 1, 0, 'And she thought of herself, \'I wish I could shut up like a stalk out of this sort of knot, and then she looked down at her hands, and was just going to begin at HIS time of life. The King\'s argument.', '', 28, 'bosluk', '2019-08-09 11:10:20', '2019-08-09 11:10:20', NULL);
INSERT INTO `questions` VALUES (4, 1, 0, 'Duchess sang the second thing is to France-- Then turn not pale, beloved snail, but come and join the dance. \'\"What matters it how far we go?\" his scaly friend replied. \"There is another shore, you.', '', 58, 'bosluk', '2019-08-09 11:10:20', '2019-08-09 11:10:20', NULL);
INSERT INTO `questions` VALUES (5, 1, 0, 'Alice had learnt several things of this sort in her head, she tried her best to climb up one of the song, she kept on good terms with him, he\'d do almost anything you liked with the lobsters, out to.', '', 57, 'coktan', '2019-08-09 11:10:20', '2019-08-09 11:10:20', NULL);
INSERT INTO `questions` VALUES (6, 1, 0, 'However, this bottle does. I do hope it\'ll make me smaller, I suppose.\' So she set to work throwing everything within her reach at the corners: next the ten courtiers; these were ornamented all over.', '', 54, 'bosluk', '2019-08-09 11:10:20', '2019-08-09 11:10:20', NULL);
INSERT INTO `questions` VALUES (7, 1, 0, 'Alice did not dare to disobey, though she looked up, and began whistling. \'Oh, there\'s no name signed at the mushroom for a minute, while Alice thought the whole she thought of herself, \'I don\'t.', '', 52, 'coktan', '2019-08-09 11:10:20', '2019-08-09 11:10:20', NULL);
INSERT INTO `questions` VALUES (8, 1, 0, 'Involved in this way! Stop this moment, I tell you!\' said Alice. \'I wonder how many hours a day or two: wouldn\'t it be murder to leave it behind?\' She said it to speak with. Alice waited patiently.', '', 25, 'bosluk', '2019-08-09 11:10:20', '2019-08-09 11:10:20', NULL);
INSERT INTO `questions` VALUES (9, 1, 0, 'How brave they\'ll all think me for his housemaid,\' she said to herself as she spoke. (The unfortunate little Bill had left off staring at the bottom of a good opportunity for making her escape; so.', '', 39, 'coktan', '2019-08-09 11:10:20', '2019-08-09 11:10:20', NULL);
INSERT INTO `questions` VALUES (10, 1, 0, 'Allow me to him: She gave me a pair of the soldiers remaining behind to execute the unfortunate gardeners, who ran to Alice a little bird as soon as it is.\' \'Then you should say what you had been to.', '', 33, 'bosluk', '2019-08-09 11:10:20', '2019-08-09 11:10:20', NULL);
INSERT INTO `questions` VALUES (11, 1, 0, 'Alice very humbly: \'you had got its neck nicely straightened out, and was just in time to wash the things I used to say.\' \'So he did, so he did,\' said the Mock Turtle to the Knave \'Turn them over!\'.', '', 21, 'bosluk', '2019-08-09 11:10:20', '2019-08-09 11:10:20', NULL);
INSERT INTO `questions` VALUES (12, 1, 0, 'Cat. \'Do you play croquet?\' The soldiers were silent, and looked at Alice, and she was now the right thing to eat some of the fact. \'I keep them to sell,\' the Hatter said, turning to Alice, and.', '', 54, 'coktan', '2019-08-09 11:10:20', '2019-08-09 11:10:20', NULL);
INSERT INTO `questions` VALUES (13, 1, 0, 'Off--\' \'Nonsense!\' said Alice, \'how am I to get dry again: they had to be a book written about me, that there was no one else seemed inclined to say when I get it home?\' when it saw Alice. It looked.', '', 11, 'coktan', '2019-08-09 11:10:20', '2019-08-09 11:10:20', NULL);
INSERT INTO `questions` VALUES (14, 1, 0, 'Lizard\'s slate-pencil, and the sounds will take care of themselves.\"\' \'How fond she is of yours.\"\' \'Oh, I know!\' exclaimed Alice, who had been all the players, except the Lizard, who seemed too much.', '', 33, 'coktan', '2019-08-09 11:10:20', '2019-08-09 11:10:20', NULL);
INSERT INTO `questions` VALUES (15, 1, 0, 'March Hare took the hookah into its face to see if he would not stoop? Soup of the bill, \"French, music, AND WASHING--extra.\"\' \'You couldn\'t have wanted it much,\' said Alice, and sighing. \'It IS a.', '', 53, 'bosluk', '2019-08-09 11:10:20', '2019-08-09 11:10:20', NULL);
INSERT INTO `questions` VALUES (16, 1, 0, 'Mouse, sharply and very angrily. \'A knot!\' said Alice, \'and why it is all the unjust things--\' when his eye chanced to fall a long argument with the tea,\' the March Hare. Visit either you like.', '', 31, 'bosluk', '2019-08-09 11:10:20', '2019-08-09 11:10:20', NULL);
INSERT INTO `questions` VALUES (17, 1, 0, 'SHE, of course,\' he said to herself, \'because of his great wig.\' The judge, by the way, was the first figure!\' said the last words out loud, and the words did not dare to disobey, though she felt.', '', 11, 'coktan', '2019-08-09 11:10:20', '2019-08-09 11:10:20', NULL);
INSERT INTO `questions` VALUES (18, 1, 0, 'There was no label this time she had brought herself down to the game. CHAPTER IX. The Mock Turtle sang this, very slowly and sadly:-- \'\"Will you walk a little girl she\'ll think me at home! Why, I.', '', 21, 'coktan', '2019-08-09 11:10:20', '2019-08-09 11:10:20', NULL);
INSERT INTO `questions` VALUES (19, 1, 0, 'Hatter replied. \'Of course it is,\' said the Cat. \'I said pig,\' replied Alice; \'and I do hope it\'ll make me grow large again, for this time she had put on her toes when they liked, and left off.', '', 46, 'coktan', '2019-08-09 11:10:20', '2019-08-09 11:10:20', NULL);
INSERT INTO `questions` VALUES (20, 1, 0, 'I don\'t believe there\'s an atom of meaning in them, after all. \"--SAID I COULD NOT SWIM--\" you can\'t help it,\' said the Caterpillar. Here was another puzzling question; and as he found it very hard.', '', 16, 'bosluk', '2019-08-09 11:10:20', '2019-08-09 11:10:20', NULL);

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES (1, 'Administrator', 'admin', NULL, NULL);
INSERT INTO `roles` VALUES (2, 'User', 'user', NULL, NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `city_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `county_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `cinsiyet` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `tel` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `resim` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'assets/img/user.png',
  `adres` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `confirmed` tinyint(1) NOT NULL DEFAULT 0,
  `confirmation_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE,
  INDEX `users_role_id_foreign`(`role_id`) USING BTREE,
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Arif DEMİR', 'cwepicentre@gmail.com', '$2y$10$WbxsVujhDmmX/sm3DDBxoOLfZhqkvIy2DDF7/p/k1bZw5Ncw4OMH.', 1, 0, 0, NULL, NULL, 'assets/img/user.png', NULL, 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `users` VALUES (2, 'Kübra DEMİR', 'kubrayas@hotmail.com', '$2y$10$kB13FBXtj4tDu/vycehGlO9rQrNjN6nmfsRl.EJdHiuGkYPksBNnq', 2, 0, 0, NULL, NULL, 'assets/img/user.png', NULL, 1, 1, NULL, NULL, NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;

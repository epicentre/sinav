#!/bin/sh
sed -i 's,$NGINX_WEB_ROOT,'"$NGINX_WEB_ROOT"',' /etc/nginx/conf.d/default.conf && \
echo "Starting NGINX" && \
nginx && \
echo "NGINX ready" && \
echo "Starting PHP7.2-FPM" && \
php-fpm7 -F -R

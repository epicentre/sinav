<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\Exam::class, function (Faker $faker) {
    $types = ['senkron', 'asenkron'];
    return [
        'user_id' => 1,
        'name' => $faker->word(),
        'time_manual' => true,
        'socket_start' => false,
        'type' => $types[array_rand($types)],
        'public' => true,
        'question_shuffle' => false,
        'answer_shuffle' => false,
        'code' => strtolower(str_random(7)),
        'theme' => 'default'
    ];
});

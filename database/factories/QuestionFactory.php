<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(App\Models\Question::class, function (Faker $faker) {
    $types = ['coktan', 'bosluk'];
    return [
        'user_id' => 1,
        'category_id' => 0,
        'content' => $faker->realText(200, 2),
        'description' => '',
        'time' => rand(10, 60),
        'type' => $types[array_rand($types)]
    ];
});

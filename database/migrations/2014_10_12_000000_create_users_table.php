<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->unsignedBigInteger('role_id')->unsigned();
            $table->unsignedBigInteger('city_id')->unsigned()->default(0);
            $table->unsignedBigInteger('county_id')->unsigned()->default(0);
            $table->string('cinsiyet', 50)->nullable();
            $table->string('tel', 50)->nullable();
            $table->string('resim')->default("assets/img/user.png");
            $table->text('adres')->nullable();
            $table->boolean('active')->default(false);
            $table->boolean('confirmed')->default(false);
            $table->string('confirmation_code')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

<?php

use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(App\Models\Question::class, 20)->create()->each(function ($question) {
            $type = $question->type;
            switch ($type) {
                case 'coktan':
                    $answer_count = rand(3, 5);
                    $answers = factory(App\Models\Answer::class, $answer_count)->make()->each(function($answer) use ($question) {
                        $answer->question_id = $question->id;
                        $answer->dogru = (bool) rand(0, 1);

                        $question->answers()->save($answer);
                    });
                    break;
                case 'bosluk':
                    $answer = factory(App\Models\Answer::class)->make();
                    $answer->question_id = $question->id;
                    $answer->dogru = true;

                    $question->answers()->save($answer);
                    break;
            }
        });

    }
}

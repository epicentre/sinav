<?php

use Illuminate\Database\Seeder;
use App\Models\ExamQuestion;
use App\Models\Question;

class ExamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $question_ids = Question::where('user_id', 1)->pluck('id')->toArray();

        factory(App\Models\Exam::class, 5)->create()->each(function ($exam) use ($question_ids) {
            $exam_question_count = rand(2, 20);

            $exam_questions = [];
            shuffle($question_ids);
            for ($i = 1; $i <= $exam_question_count; $i++) {
                $exam_questions[] = [
                    'exam_id' => $exam->id,
                    'question_id' => $question_ids[$i - 1]
                ];
            }

            ExamQuestion::insert($exam_questions);
        });
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('users')->truncate();

        $users = [
            [
                'name' => 'Arif DEMİR',
                'email' => 'cwepicentre@gmail.com',
                'password' => bcrypt('123456'),
                'role_id' => 1,
                'active' => true,
                'confirmed' => true
            ],
            [
                'name' => 'Kübra DEMİR',
                'email' => 'kubrayas@hotmail.com',
                'password' => bcrypt('123456'),
                'role_id' => 2,
                'active' => true,
                'confirmed' => true
            ]
        ];

        User::insert($users);
    }
}

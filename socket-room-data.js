class RoomData {

    constructor(debugMode = false) {
        this.debugMode = debugMode;
        this.rooms = {};
    }

    roomExists(roomName) {
        return this.rooms.hasOwnProperty(roomName);
    }

    createRoomData(socket, room) {
        if(this.debugMode) console.log(socket.id+": Creating Room: "+room);
        this.rooms[room] = {owner:socket.id, users:[], variables: {}};
    }

    prepareRoomData(socket, room) {
        if(this.debugMode) console.log(socket.id+": Preparing room: "+room);
        if(!this.roomExists(room)) {
            this.createRoomData(socket, room);
        } else {
            this.rooms[room].users.push(socket.id);
        }
    }

    clearRoom(roomName) {
        delete this.rooms[roomName];
    }

    leaveRoom(socket) {
        let roomName = this.getRoomName(socket);
        if(roomName === false) {
            // throw new Error("socket id:" + socket.id + " is not in a room!");
            console.log("socket id:" + socket.id + " is not in a room!");
            return false;
        }
        if(this.debugMode) console.log(socket.id+": Leaving room: "+roomName);
        let i = this.rooms[roomName].users.indexOf(socket.id);
        if(i !== -1) this.rooms[roomName].users.splice(i, 1);
        if(this.rooms[roomName].users.length === 0) {
            this.clearRoom(roomName);
        }
    }

    set(socket, variable, content) {
        if(this.debugMode) console.log(socket.id+": Creating variable: "+variable+" with content: "+content);
        let roomName = this.getRoomName(socket);
        if(!this.roomExists(roomName)){
            console.error("You have tried setting a room variable but this socket is not in any room!");
            return false;
        }
        this.rooms[roomName].variables[variable] = content;
    }

    get(socket, variable) {
        if(this.debugMode) console.log(socket.id+": Getting variable: "+variable);
        let roomName = this.getRoomName(socket);
        if(!this.roomExists(roomName)){
            console.error("You have tried getting a room variable but this socket is not in any room!");
            return undefined;
        }

        return this.rooms[roomName].variables[variable];
    }

    getRoomName(socket) {
        for (let roomName in this.rooms) {
            let room = this.rooms[roomName];
            if (room.owner === socket.id || room.users.includes(socket.id)) {
                return roomName;
            }
        }
        return false;
    }

    getOwnerId(socket) {
        let roomName = this.getRoomName(socket);
        if (this.roomExists(roomName)) {
            return this.rooms[roomName].owner;
        }

        return false;
    }

    getUsers(socket) {
        let roomName = this.getRoomName(socket);
        if (this.roomExists(roomName)) {
            return this.rooms[roomName].users;
        }

        return false;
    }
}

module.exports = RoomData;

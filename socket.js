let app = require('express')();
let http = require('http').Server(app);
let io = require('socket.io')(http);
let RoomData = require('./socket-room-data.js');
let sinavSocketData = new RoomData(true);

io.on('connection', function(socket){

    socket.on('baglan', function(data) {

        let userDetail = {
            'userId'    : data.user.id,
            'name'      : data.user.name,
            'email'     : data.user.email,
            'socketId'  : socket.id
        };

        socket.join(data.room);
        sinavSocketData.prepareRoomData(socket, data.room);

        if (sinavSocketData.get(socket, 'started') === undefined) {
            let userDetailsTmp = sinavSocketData.get(socket, 'userDetails');

            if (sinavSocketData.getOwnerId(socket) != socket.id) {
                if (typeof userDetailsTmp !== 'object') {
                    userDetailsTmp = {};
                }
                userDetailsTmp[socket.id] = userDetail;
                sinavSocketData.set(socket, 'userDetails', userDetailsTmp);
            }

            if (typeof userDetailsTmp === 'object' && Object.keys(userDetailsTmp).length > 0) {
                io.to(sinavSocketData.getOwnerId(socket)).emit('kullaniciListele', userDetailsTmp);
            }

            console.log(data.user.name + ' odaya bağlandı, socketId:' + socket.id);
        } else {
            io.to(socket.id).emit('ban');
            console.log("Sınav esnasındayken odaya girme isteği engellendi");
        }
    });

    socket.on('disconnect', function() {

        let userDetailsTmp = sinavSocketData.get(socket, 'userDetails');
        if(typeof userDetailsTmp === 'object' && userDetailsTmp.hasOwnProperty(socket.id))
            delete userDetailsTmp[socket.id];
        sinavSocketData.set(socket, 'userDetails', userDetailsTmp);

        if (sinavSocketData.getOwnerId(socket) == socket.id) {
            io.to(sinavSocketData.getRoomName(socket)).emit('ban');
        } else {
            io.to(sinavSocketData.getOwnerId(socket)).emit('kullaniciListele', userDetailsTmp);
        }

        sinavSocketData.leaveRoom(socket);

        console.info('Odadan ayrıldı (' + socket.id + ').');
    });

    socket.on('banla', function(socketId){
        console.log('kullanıcı banlanacak: ' + socketId);
        io.to(socketId).emit('ban');
    });

    socket.on('baslat', function(data){
        var soru_sayisi = Object.keys(data.sorular).length;
        sinavSocketData.set(socket, 'soru_sayisi', soru_sayisi);
        sinavSocketData.set(socket, 'kacinci_soru', 1);

        var sorular = data.sorular;
        var kategoriler = data.kategoriler;
        var soru = data.sorular[0];
        var kategori = data.kategoriler[0];
        sinavSocketData.set(socket, 'sorular', sorular);
        sinavSocketData.set(socket, 'kategoriler', kategoriler);

        sinavSocketData.set(socket, 'started', true);

        io.to(sinavSocketData.getRoomName(socket)).emit('soruGetir', {'soru' : soru, 'soru_sayisi' : soru_sayisi, 'kacinci_soru' : 1, 'start_time' : Date.now(), 'kategori' : kategori });
    });

    socket.on('ileri', function(){
        sinavSocketData.set(socket, 'cevaplar', []);
        var kacinci_soru = sinavSocketData.get(socket, 'kacinci_soru') + 1;
        if(kacinci_soru <= sinavSocketData.get(socket, 'soru_sayisi')){
            sinavSocketData.set(socket, 'kacinci_soru', kacinci_soru);

            var sorular = sinavSocketData.get(socket, 'sorular');
            var kategoriler = sinavSocketData.get(socket, 'kategoriler');
            var soru = sorular[kacinci_soru - 1];
            var kategori = kategoriler[kacinci_soru - 1];

            io.to(sinavSocketData.getRoomName(socket)).emit('soruGetir', {'soru' : soru, 'soru_sayisi' : sinavSocketData.get(socket, 'soru_sayisi'), 'kacinci_soru' : kacinci_soru, 'start_time' : Date.now(), 'kategori' : kategori});
        }
        else{
            io.to(sinavSocketData.getRoomName(socket)).emit('sinavBitir');
        }

    });

    socket.on('cevapRaporGir', function(data){
        var cevaplar = sinavSocketData.get(socket, 'cevaplar') === undefined ? [] : sinavSocketData.get(socket, 'cevaplar');

        cevaplar.push({"cevapId" : data.cevapId, "answer_string" : data.answer_string});

        sinavSocketData.set(socket, 'cevaplar', cevaplar);

        io.to(sinavSocketData.getOwnerId(socket)).emit('cevapGeldi', {"sayi" : Object.keys(cevaplar).length });

    });

    socket.on('cevapRapor', function(){
        var soru = sinavSocketData.get(socket, 'sorular')[sinavSocketData.get(socket, 'kacinci_soru') - 1];
        var secenekler = soru.answers;

        var verilen_cevaplar = sinavSocketData.get(socket, 'cevaplar');
        var cevap_veri = {};
        if(soru.type == "coktan"){
            for(var k in verilen_cevaplar){
                if(cevap_veri.hasOwnProperty(verilen_cevaplar[k].cevapId))
                    cevap_veri[verilen_cevaplar[k].cevapId]+=1;
                else
                    cevap_veri[verilen_cevaplar[k].cevapId]=1;
            }
        } else {
            for (var k in verilen_cevaplar) {
                if(cevap_veri.hasOwnProperty(verilen_cevaplar[k].answer_string))
                    cevap_veri[verilen_cevaplar[k].answer_string]+=1;
                else
                    cevap_veri[verilen_cevaplar[k].answer_string]=1;
            }
        }

        io.to(sinavSocketData.getRoomName(socket)).emit('raporGetir', { "secenekler" : secenekler, "cevap_veri" : cevap_veri, "cevaplayan_sayisi" : verilen_cevaplar.length });
    });
});

http.listen(3000, function(){
    console.log('Listening on Port 3000');
});
